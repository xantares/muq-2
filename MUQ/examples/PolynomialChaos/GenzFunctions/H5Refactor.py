import sys
import h5py

if __name__ == "__main__":
    
    if len(sys.argv) != 2:
        print "Error! Usage: python H5Content.py <filename>"
        exit()
    
    f = h5py.File( sys.argv[1], 'r+' )

    for gtype in f.keys():
        print " "
        print "Type: " + gtype
        gtype_grp = f[gtype]
        for genzName in gtype_grp.keys():
            print "\tFunction: " + genzName
            dim_grp = gtype_grp[genzName]
            for dim in dim_grp.keys():
                print "\t\tDim: " + dim
                tscale_grp = dim_grp[dim]
                for tscale in tscale_grp.keys():
                    print "\t\t\tTime scale: " + tscale
                    itscale = int(float(tscale[7:]))
                    print "\t\t\tTime scale refactor: " + str(itscale)
                    tscale_grp.move( tscale, 'tscale=' + str(itscale) )
                    # tscale_grp[ 'tscale=' + str(itscale) ] = tscale_grp[ tscale ]

    f.close()
