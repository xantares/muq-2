/** \example modelling-example-1.cpp
 *  <h1>Introduction</h1>
 *  <p>Welcome to the first tutorial in MUQ.  This example is the first in a series of examples devoted to describing
 * deterministic and stochastic system in MUQ.
 *  In order to tackle any problem in MUQ, the problem needs to be described in a common format that all MUQ tools can
 * understand.  Within MUQ, the modelling module provides
 *  three fundamental objects to achieve this: (1) models, (2) random variables, and (3) densities.  The goal of
 * tutorial is to introduce MUQ's method for definining models.
 *  Later tutorials will demonstrate ways to define more advanced models, combine models, and ultimately combine them
 * with random variables and densities to tackle uncertainty quantification problems.</p>
 *
 *  <h3> About this example </h3>
 *  <p>A recurring problem in the next few examples will be the implementation of a relatively simple nonlinear
 * predator-prey model.  This model approximate the population growth of a two species system.
 *  The predator species consumes the prey species to survive while the prey species consumes other resources (such as
 * vegetation).  Let \f$P\f$ be the population of the prey and \f$Q\f$ be the predator population.
 *  Our predator-prey model consists of the following system of ODEs:
 *  \f{eqnarray*}{
 *  \frac{dP}{dt} & = & rP\left(1-\frac{P}{K}\right) - s\frac{PQ}{a+P}\\
 *  \frac{dQ}{dt} & = & u\frac{PQ}{a+P} - vQ
 *  \f}
 *  Intuitively, the first terms on each right hand side represent growth terms while the second terms represent death
 * terms. In the first equation, \f$r\f$ is the prey growth rate,
 *  \f$K\f$ is the carrying capacity of the environment for the prey, \f$s\f$ is a predation rate, and \f$a\f$ is
 * related to the amount of prey the predators can consume.  Analogously,
 *  in the second equation, \f$u\f$ is the growth rate of the predator, and $v$ is the death rate.
 *  </p>
 *
 *  <p>
 *  In this example, we will build a model to evaluate the right hand side of this ODE for fixed values of the model
 * parameters.  Our right hand side model will take \$P\$ and \$Q\$ as inputs and return \f$\frac{dP}{dt}\f$ and
 * \f$\frac{dQ}{dt}\f$.
 *  Model definitions in MUQ take zero or more vector valued inputs and return one vector valued output.  Thus, the
 * model output will be the vector \f$[\frac{dP}{dt}, \frac{dQ}{dt}]^T\f$.  For simplicity, the model in this example
 * will also lump
 *  \f$P\f$ and \f$Q\f$ into a vector valued input.  Thus, the MUQ model will define a function \f$ f:R^2\rightarrow
 * R^2\f$.  Note that MUQ uses the Eigen::VectorXd type for real-valued vectors.
 *  </p>
 *
 *  <h3>Line by line explanation</h3>
 *  <p>See example-1.cpp for finely documented code.</p>
 *
 *  <h3> Compiling this example and linking to MUQ </h3>
 *
 */
/* MUQ uses Eigen for defining all vectors and matrices.
 *  This line includes the core components of Eigen that will be needed for defining our model in MUQ.
 */
#include <Eigen/Core>

/* At the end of this program, we will report a model evaluation so we will need to access std::cout
 */
#include <iostream>

/* The tools in MUQ interact with models through the ModPiece class.
 * So to define our model, we will define a child of ModPiece.
 * The base ModPiece class has many functions that allow model implementations
 * to use multiple inputs and provide derivative information.  However,
 * in this example, our model has only one input and we do not require derivatives.
 * Fortunately, MUQ provides single input templates that make it easy to define a model.
 * The ModPieceTemplates.h header file contains all of these templates.
 */
#include <MUQ/Modelling/ModPieceTemplates.h>


/* use the muq::Modelling namespace
 */
using namespace muq::Modelling;

/* use the std namespace for cout and endl
 */
using namespace std;


/* This class describes the right hand side of our predator prey model.  All MUQ models need to be defined as
 * a child class of the ModPiece class.  For Models that do not need the full flexibility of the ModPiece
 * base class, several one-input template classes are provided.  In this tutorial, our model definition
 * is going to inherit from the muq::Modelling::OneInputNoDerivModPiece class.  This class provides a single
 * vector-valued input template that does not require the user to provide derivative information (more on
 * computing gradients, Jacobians, and Hessians can be found in later examples).
 */
class PredPreyModel : public OneInputNoDerivModPiece {
public:

  /* The constructor for our Model takes the 6 fixed parameters in the predator prey model and initializes
   * the OneInputNoDerivModPiece parent class.  The OneInputNoDerivModPiece constructor requires the input
   * and output dimensions of the model.  In our case, the input and output are both two dimensional vectors.
   * Note that the input and output dimensions of all models in MUQ are constant.  This means that the input
   * and output dimensions must be set when the constructor is called, and also that the input and output
   * dimensions of an existing Model cannot be changed.
   */
  PredPreyModel(double r, double k, double s, double a, double u, double v) : OneInputNoDerivModPiece(2, 2), preyGrowth(
                                                                                r),
                                                                              preyCapacity(k), predationRate(s),
                                                                              predationHunger(
                                                                                a), predatorLoss(u),
                                                                              predatorGrowth(v)
  {}

  /* We define a default virtual destructor for the model to eliminate issues when the destructor is called from
   * a pointer to a parent of this class (a common occurence in MUQ).
   */
  virtual ~PredPreyModel() = default;

private:

  /* The EvaluateImpl class is where all the magic happens.  This is where the model computation is performed.  Note
   * that this function is defined as a private function.  To provide some extra functionality such as caching, MUQ
   * separates the model implementation (this function) from the evaluation interface (the Evaluate function
   * demonstrated in the main function below).
   */
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override
  {
    // for clarity, grab the prey and predator populations from the input vector
    double preyPop = input(0);
    double predPop = input(1);

    // create a vector to hold the model output, this will hold the time derivatives of population
    Eigen::VectorXd output(2);

    // the first output of this function is the derivative of the PREY population size with resepct to time
    output(0) = preyPop *
                (preyGrowth * (1 - preyPop / preyCapacity) - predationRate * predPop / (predationHunger + preyPop));

    // the second output of this function is the derivative of the PREDATOR population size with resepct to time
    output(1) = predPop * (predatorGrowth * preyPop / (predationHunger + preyPop) - predatorLoss);

    // return the output
    return output;
  }

  // store the constant model parameters
  double preyGrowth, preyCapacity, predationRate, predationHunger, predatorLoss, predatorGrowth;
};


/* Now that we have define the forward model in the PredPreyModel class, let's play around with the model a bit.
 * The main function creates an instance of PredPreyModel and demonstrates how to evaluate the model.
 */
int main()
{
  // Before constructing the model, we will set the fixed model values.
  double preyGrowth, preyCapacity, predationRate, predationHunger, predatorLoss, predatorGrowth;

  preyGrowth      = 0.8;
  preyCapacity    = 100;
  predationRate   = 4.0;
  predationHunger = 15;
  predatorLoss    = 0.6;
  predatorGrowth  = 0.8;

  /* create an instance of the ModPiece defining the predator prey model.  Notice the use of the auto keyword, which
   * automatically captures
   * the shared_ptr type returned from the make_shared function.  MUQ makes extensive use of shared pointers and it is a
   *good (sometimes critical)
   * habit to use smart pointers to Models instead of the object itself.
   */
  auto predatorPreyModel = make_shared<PredPreyModel>(preyGrowth,
                                                      preyCapacity,
                                                      predationRate,
                                                      predationHunger,
                                                      predatorLoss,
                                                      predatorGrowth);

  // create an input vector holding both the predator and prey populations
  Eigen::VectorXd populations(2);
  populations(0) = 50; // prey population
  populations(1) = 5;  // predator population

  /* Evaluate the model with this the values in the populations vector.  Remember that the model is stored in a smart
   * pointer and
   * thus requires us to use "->" instead of "." to call member functions.  Also, notice that we call the Evaluate
   *function and
   * not the EvaluateImpl function defined in the PredPreyModel class.  The Evaluate function checks the input sizes and
   *provides
   * one step caching.  This means that calling the model two times in a row with the same input will only result in one
   *call to EvaluateImpl.
   * For some algorithms and expensive Models, this can result in dramatic time savings.
   */
  Eigen::VectorXd growthRates = predatorPreyModel->Evaluate(populations);

  // print the growth rates to std out
  cout << "The prey growth rate evaluated at     (" << populations(0) << "," << populations(1) << ") is " <<
    growthRates(0) << endl;
  cout << "The predator growth rate evaluated at (" << populations(0) << "," << populations(1) << ") is " <<
    growthRates(1) << endl;

  /* We can also ask the model for it's input and output sizes.  Every Model in MUQ has a constant vector, "inputSizes",
   * that contains the length of each vector valued input.  Since our Model only has one input, inputSizes only has one
   *entry.  The output dimension
   * of the model is stored in the "outputSize" variable.  Since MUQ models can only have one output, "outputSize" is
   *simply an integer.
   */
  cout << "The input size is  " << predatorPreyModel->inputSizes(0) << endl;
  cout << "The output size is " << predatorPreyModel->outputSize << endl;
}

