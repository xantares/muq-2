
#include "MUQ/Utilities/mesh/Mesh.h"

#include <iostream>
#include <functional>
#include <algorithm>
#include <cstdlib>
#include <numeric>

#include <boost/math/constants/constants.hpp>

using namespace muq::Utilities;
using namespace std;

using std::atan2;

FindElementFailure ElementSearchFail;

/** Return a matrix holding all of the element diffPositions. */
template<unsigned int dim>
Eigen::Matrix<double, dim, Eigen::Dynamic> Mesh<dim>::GetAllElePos() const
{
  Eigen::Matrix<double, dim, Eigen::Dynamic> Output(dim, NumEles());

  // loop over all the elements
  for (unsigned int i = 0; i < NumEles(); ++i) {
    Output.col(i) = GetElePos(i);
  }

  return Output;
}

namespace muq {
namespace Utilities {
template<>
unsigned int Mesh<1>::GetEle(const Eigen::Matrix<double, 1, 1>& pos)
{
  // loop over all the elements, checking if this diffPosition is inside
  for (unsigned int i = 0; i < NumEles(); ++i) {
    // get the nodes of this element
    Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic> nodes = GetNodes(i);

    unsigned int Nnodes = nodes.size();

    // get the difference in diffPositions of each node
    Eigen::Matrix<double, 1, Eigen::Dynamic> diffPos(1, Nnodes);
    for (unsigned int n = 0; n < Nnodes; ++n) {
      diffPos.col(n) = GetNodePos(nodes(n, 0)) - pos;
    }
    if ((diffPos(0, 0) <= 0) && (diffPos(0, 1) >= 0)) {
      return i;
    }
  }

  // if no element was found, throw an exception
  throw(ElementSearchFail);
}

template<>
unsigned int Mesh<2>::GetEle(const Eigen::Matrix<double, 2, 1>& pos)
{
  // loop over all the elements, checking if this diffPosition is inside
  for (unsigned int i = 0; i < NumEles(); ++i) {
    // get the nodes of this element
    Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic> nodes = GetNodes(i);

    unsigned int Nnodes = nodes.size();

    // get the difference in diffPositions of each node
    Eigen::Matrix<double, 2, Eigen::Dynamic> diffPos(2, Nnodes);
    for (unsigned int n = 0; n < Nnodes; ++n) {
      // std::cout << GetNodediffPos(nodes(n,0)).transdiffPose() << std::endl;
      diffPos.col(n) = GetNodePos(nodes(n, 0)) - pos;
    }

    //std::cout << std::endl;
    // first check to see if point is in bounding box
    Eigen::Matrix<double, 2, 1> lb = diffPos.rowwise().minCoeff();
    Eigen::Matrix<double, 2, 1> ub = diffPos.rowwise().maxCoeff();
    if ((lb.maxCoeff() < 0) && (ub.minCoeff() > 0)) {
      // create a vector to hold the angles
      Eigen::VectorXd angles = diffPos.row(1).binaryExpr(diffPos.row(0), std::ptr_fun<double, double, double>(atan2));

      // sort the angles
      std::sort(angles.data(), angles.data() + Nnodes);

      // compute the differences in angles
      Eigen::VectorXd anglediff = Eigen::VectorXd::Zero(Nnodes);
      anglediff.head(Nnodes - 1) = angles.tail(Nnodes - 1) - angles.head(Nnodes - 1);
      anglediff[Nnodes - 1]      = 2 * boost::math::constants::pi<double>() + (angles(0) - angles(Nnodes - 1));

      if (anglediff.maxCoeff() <= boost::math::constants::pi<double>()) {
        return i;
      }
    }
  }


  // if no element was found, throw an exception
  throw(ElementSearchFail);
}

template<>
unsigned int Mesh<3>::GetEle(const Eigen::Matrix<double, 3, 1>& pos)
{
  // loop over all the elements, checking if this position is inside
  for (unsigned int i = 0; i < NumEles(); ++i) {
    // get the nodes of this element
    Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic> nodes = GetNodes(i);

    unsigned int Nnodes = nodes.size();

    // get the difference in positions of each node
    Eigen::Matrix<double, 3, Eigen::Dynamic> diffPos(3, Nnodes);
    for (unsigned int n = 0; n < Nnodes; ++n) {
      diffPos.col(n) = GetNodePos(nodes(n, 0)) - pos;
    }

    // first check to see if point is in bounding box
    Eigen::Matrix<double, 3, 1> lb = diffPos.rowwise().minCoeff();
    Eigen::Matrix<double, 3, 1> ub = diffPos.rowwise().maxCoeff();
    if ((lb.maxCoeff() < 0) && (ub.minCoeff() > 0)) {
      // figure out if this is a tetrahedra or a quadrilateral
      if (Nnodes == 4) { // simplicial
        Eigen::Matrix<double, 12, 12> A(12, 12);
        A << 1,
            diffPos(0, 0), diffPos(1, 0), diffPos(2, 0), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, diffPos(0, 0), diffPos(
          1,
          0),
            diffPos(2,
                    0), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    0),
            diffPos(1,
                    0),
            diffPos(2,
                    0), 1,
            diffPos(0, 1), diffPos(1, 1), diffPos(2, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, diffPos(0, 1), diffPos(
          1,
          1),
            diffPos(2,
                    1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    1),
            diffPos(1,
                    1),
            diffPos(2,
                    1), 1,
            diffPos(0, 2), diffPos(1, 2), diffPos(2, 2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, diffPos(0, 2), diffPos(
          1,
          2),
            diffPos(2,
                    2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    2),
            diffPos(1,
                    2),
            diffPos(2,
                    2), 1,
            diffPos(0, 3), diffPos(1, 3), diffPos(2, 3), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, diffPos(0, 3), diffPos(
          1,
          3), diffPos(2, 3), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, diffPos(0, 3), diffPos(1, 3), diffPos(2, 3);

        Eigen::Matrix<double, 12, 1> b(12, 1);
        b << 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1;

        Eigen::Matrix<double, 12, 1> x = A.lu().solve(b);

        if ((x(0) >= 0) && (x(4) >= 0) & (x(8) >= 0) && (x(0) + x(4) + x(8) <= 1)) {
          return i;
        }
      } else if (Nnodes == 8) { // quadrilateral
        // first, transform the quad to a cube
        Eigen::Matrix<double, 24, 24> A(24, 24);
        A << 1,
            diffPos(0,
                    0),
            diffPos(1,
                    0),
            diffPos(2, 0), diffPos(0, 0) * diffPos(1, 0), diffPos(0, 0) * diffPos(2, 0), diffPos(1, 0) * diffPos(2,
                                                                                                                 0),
            diffPos(0,
                    0) *
            diffPos(1,
                    0) *
            diffPos(2,
                    0), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    0),
            diffPos(1,
                    0),
            diffPos(2, 0), diffPos(0, 0) * diffPos(1, 0), diffPos(0, 0) * diffPos(2, 0), diffPos(1, 0) * diffPos(2,
                                                                                                                 0),
            diffPos(0,
                    0) *
            diffPos(1,
                    0) *
            diffPos(2,
                    0), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    0),
            diffPos(1,
                    0),
            diffPos(2, 0), diffPos(0, 0) * diffPos(1, 0), diffPos(0, 0) * diffPos(2, 0), diffPos(1, 0) * diffPos(2,
                                                                                                                 0),
            diffPos(0, 0) * diffPos(1, 0) * diffPos(2, 0), 1, diffPos(0, 1), diffPos(1, 1), diffPos(2, 1), diffPos(
          0,
          1) * diffPos(1, 1), diffPos(0, 1) * diffPos(2, 1), diffPos(1, 1) * diffPos(2, 1), diffPos(0, 1) * diffPos(1,
                                                                                                                    1) *
            diffPos(2,
                    1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    1),
            diffPos(1,
                    1),
            diffPos(2, 1), diffPos(0, 1) * diffPos(1, 1), diffPos(0, 1) * diffPos(2, 1), diffPos(1, 1) * diffPos(2,
                                                                                                                 1),
            diffPos(0,
                    1) *
            diffPos(1,
                    1) *
            diffPos(2,
                    1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    1),
            diffPos(1,
                    1),
            diffPos(2, 1), diffPos(0, 1) * diffPos(1, 1), diffPos(0, 1) * diffPos(2, 1), diffPos(1, 1) * diffPos(2,
                                                                                                                 1),
            diffPos(0, 1) * diffPos(1, 1) * diffPos(2, 1), 1, diffPos(0, 2), diffPos(1, 2), diffPos(2, 2), diffPos(
          0,
          2) * diffPos(1, 2), diffPos(0, 2) * diffPos(2, 2), diffPos(1, 2) * diffPos(2, 2), diffPos(0, 2) * diffPos(1,
                                                                                                                    2) *
            diffPos(2,
                    2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    2),
            diffPos(1,
                    2),
            diffPos(2, 2), diffPos(0, 2) * diffPos(1, 2), diffPos(0, 2) * diffPos(2, 2), diffPos(1, 2) * diffPos(2,
                                                                                                                 2),
            diffPos(0,
                    2) *
            diffPos(1,
                    2) *
            diffPos(2,
                    2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    2),
            diffPos(1,
                    2),
            diffPos(2, 2), diffPos(0, 2) * diffPos(1, 2), diffPos(0, 2) * diffPos(2, 2), diffPos(1, 2) * diffPos(2,
                                                                                                                 2),
            diffPos(0, 2) * diffPos(1, 2) * diffPos(2, 2), 1, diffPos(0, 3), diffPos(1, 3), diffPos(2, 3), diffPos(
          0,
          3) * diffPos(1, 3), diffPos(0, 3) * diffPos(2, 3), diffPos(1, 3) * diffPos(2, 3), diffPos(0, 3) * diffPos(1,
                                                                                                                    3) *
            diffPos(2,
                    3), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    3),
            diffPos(1,
                    3),
            diffPos(2, 3), diffPos(0, 3) * diffPos(1, 3), diffPos(0, 3) * diffPos(2, 3), diffPos(1, 3) * diffPos(2,
                                                                                                                 3),
            diffPos(0,
                    3) *
            diffPos(1,
                    3) *
            diffPos(2,
                    3), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    3),
            diffPos(1,
                    3),
            diffPos(2, 3), diffPos(0, 3) * diffPos(1, 3), diffPos(0, 3) * diffPos(2, 3), diffPos(1, 3) * diffPos(2,
                                                                                                                 3),
            diffPos(0, 3) * diffPos(1, 3) * diffPos(2, 3), 1, diffPos(0, 4), diffPos(1, 4), diffPos(2, 4), diffPos(
          0,
          4) * diffPos(1, 4), diffPos(0, 4) * diffPos(2, 4), diffPos(1, 4) * diffPos(2, 4), diffPos(0, 4) * diffPos(1,
                                                                                                                    4) *
            diffPos(2,
                    4), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    4),
            diffPos(1,
                    4),
            diffPos(2, 4), diffPos(0, 1) * diffPos(1, 4), diffPos(0, 4) * diffPos(2, 4), diffPos(1, 4) * diffPos(2,
                                                                                                                 4),
            diffPos(0,
                    4) *
            diffPos(1,
                    4) *
            diffPos(2,
                    4), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    4),
            diffPos(1,
                    4),
            diffPos(2, 4), diffPos(0, 4) * diffPos(1, 4), diffPos(0, 4) * diffPos(2, 4), diffPos(1, 4) * diffPos(2,
                                                                                                                 4),
            diffPos(0, 4) * diffPos(1, 4) * diffPos(2, 4), 1, diffPos(0, 5), diffPos(1, 5), diffPos(2, 5), diffPos(
          0,
          5) * diffPos(1, 5), diffPos(0, 5) * diffPos(2, 5), diffPos(1, 5) * diffPos(2, 5), diffPos(0, 5) * diffPos(1,
                                                                                                                    5) *
            diffPos(2,
                    5), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    5),
            diffPos(1,
                    5),
            diffPos(2, 5), diffPos(0, 5) * diffPos(1, 5), diffPos(0, 5) * diffPos(2, 5), diffPos(1, 5) * diffPos(2,
                                                                                                                 5),
            diffPos(0,
                    5) *
            diffPos(1,
                    5) *
            diffPos(2,
                    5), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    5),
            diffPos(1,
                    5),
            diffPos(2, 5), diffPos(0, 5) * diffPos(1, 5), diffPos(0, 5) * diffPos(2, 5), diffPos(1, 5) * diffPos(2,
                                                                                                                 5),
            diffPos(0, 5) * diffPos(1, 5) * diffPos(2, 5), 1, diffPos(0, 6), diffPos(1, 6), diffPos(2, 6), diffPos(
          0,
          6) * diffPos(1, 6), diffPos(0, 6) * diffPos(2, 6), diffPos(1, 6) * diffPos(2, 6), diffPos(0, 6) * diffPos(1,
                                                                                                                    6) *
            diffPos(2,
                    6), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    6),
            diffPos(1,
                    6),
            diffPos(2, 6), diffPos(0, 6) * diffPos(1, 6), diffPos(0, 6) * diffPos(2, 6), diffPos(1, 6) * diffPos(2,
                                                                                                                 6),
            diffPos(0,
                    6) *
            diffPos(1,
                    6) *
            diffPos(2,
                    6), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    6),
            diffPos(1,
                    6),
            diffPos(2, 6), diffPos(0, 6) * diffPos(1, 6), diffPos(0, 6) * diffPos(2, 6), diffPos(1, 6) * diffPos(2,
                                                                                                                 6),
            diffPos(0, 6) * diffPos(1, 6) * diffPos(2, 6), 1, diffPos(0, 7), diffPos(1, 7), diffPos(2, 7), diffPos(
          0,
          7) * diffPos(1, 7), diffPos(0, 7) * diffPos(2, 7), diffPos(1, 7) * diffPos(2, 7), diffPos(0, 7) * diffPos(1,
                                                                                                                    7) *
            diffPos(2,
                    7), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    7),
            diffPos(1,
                    7),
            diffPos(2, 7), diffPos(0, 7) * diffPos(1, 7), diffPos(0, 7) * diffPos(2, 7), diffPos(1, 7) * diffPos(2,
                                                                                                                 7),
            diffPos(0,
                    7) *
            diffPos(1,
                    7) *
            diffPos(2,
                    7), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            diffPos(0,
                    7),
            diffPos(1,
                    7),
            diffPos(2, 7), diffPos(0, 7) * diffPos(1, 7), diffPos(0, 7) * diffPos(2, 7), diffPos(1, 7) * diffPos(2,
                                                                                                                 7),
            diffPos(0, 7) * diffPos(1, 7) * diffPos(2, 7);

        Eigen::Matrix<double, 24, 1> b(24, 1);
        b << 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1;

        Eigen::Matrix<double, 24, 1> x = A.lu().solve(b);

        // now, check to make sure the transformed zero vector would be inside the cube
        double u = x(0);
        double v = x(8);
        double w = x(16);
        if ((u >= 0) && (u <= 1) && (v >= 0) && (v <= 1) && (w >= 0) && (w <= 1)) {
          return i;
        }
      } else {
        std::cerr << "ERROR: unsupoorted element type found in Mesh<3>::GetEle();\n";
        throw(ElementSearchFail);
      }
    }
  }


  // if no element was found, throw an exception
  throw(ElementSearchFail);
}
}
}

namespace muq {
namespace Utilities {
template class Mesh<1>;
template class Mesh<2>;
template class Mesh<3>;
}
}


