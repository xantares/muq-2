
#include "MUQ/Utilities/mesh/UnstructuredSimplicialMesh.h"

// std library includes
#include <iostream>
#include <fstream>
#include <boost/math/special_functions/factorials.hpp>
#include <Eigen/LU>

using namespace muq::Utilities;
using namespace std;


/** Read the mesh in Triangle format from an element file and a node file. */
template<unsigned int dim>
UnstructuredSimplicialMesh<dim>::UnstructuredSimplicialMesh(const std::string& EleFile, const std::string& NodeFile)
{
  unsigned int NumNodes        = 0; // number of nodes
  unsigned int NumEles         = 0; // number of elements
  unsigned int FileDim         = 0; // the mesh dimension read from the file
  unsigned int FileSimplexSize = 0; // the number of nodes per element according to the element file

  char streambuffer[256];

  // Open the node file
  ifstream nodes(NodeFile.c_str());

  if (!nodes.is_open()) {
    std::cerr <<
    "ERROR: Could not open node file in muq::Utilities::UnstructuredSimplicialMesh(const std::string &EleFile, const std::string &NodeFile).  Make sure the filename is correct.\n";
    throw("Could not open node file");
  }

  // read the number of nodes
  int Natt = 0;
  nodes >> NumNodes >> FileDim >> Natt;

  // make sure the File has the same dimensions as this mesh
      assert(FileDim == dim);

  Mesh<dim>::NodeValue.resize(NumNodes, Natt);
  Mesh<dim>::NodeZone.resize(NumNodes);

  // allocate space for the nodes
  NodePos.resize(NumNodes, -1.0 * Eigen::Matrix<double, dim, 1>::Ones(dim, 1));

  // read in all of the nodes
  for (unsigned i = 0; i < NumNodes; ++i) {
    // read the rest of the line
    nodes.getline(streambuffer, 256);

    // read in the vertex number
    unsigned int ind;
    nodes >> ind;

    // read in the node position
    for (unsigned int d = 0; d < dim; ++d) {
      nodes >> NodePos[ind - 1][d];
    }


    // read in the node zone
    if (Natt > 0) {
      nodes >> Mesh<dim>::NodeZone[ind - 1];

      // read in the attributes
      for (int k = 0; k < Natt; ++k) {
        nodes >> Mesh<dim>::NodeValue(ind - 1, k);
      }
    }
  }

  // close the node file
  nodes.close();


  // Open the element file
  ifstream elestream(EleFile.c_str());

  if (!elestream.is_open()) {
    std::cerr <<
    "ERROR: Could not open element file in muq::Utilities::UnstructuredSimplicialMesh(const std::string &EleFile, const std::string &NodeFile).  Make sure the filename is correct.\n";
    throw("Could not open element file");
  }


  // read in the number of elements and the number of nodes per triangle
  elestream >> NumEles >> FileSimplexSize;

  // make sure the element are the right shape
      assert(FileSimplexSize == dim + 1);

  // allocate space for the nodes
  Eles.resize(NumEles, -1.0 * Eigen::Matrix<double, dim + 1, 1>::Ones(dim + 1, 1));

  // read in all of the elements
  for (unsigned int i = 0; i < NumEles; ++i) {
    // get the rest of the line
    elestream.getline(streambuffer, 256);

    // get the element number
    unsigned int ind;
    elestream >> ind;

    // read in the element vertices -- making sure they are legitimate
    for (unsigned int d = 0; d < dim + 1; ++d) {
      unsigned int tempVertex;
      elestream >> tempVertex;

      // make sure the input node is legitimate
      assert(tempVertex - 1 < NumNodes);

      Eles[ind - 1][d] = tempVertex - 1;
    }
  }

  // close the element file
  elestream.close();
}

namespace muq {
namespace Utilities {
/** Construct the mesh on a rectangle -- mostly used for debugging. This constructor builds the quad mesh and then
 *  divides each quadrilateral element into two simplicial elements */
template<> UnstructuredSimplicialMesh<1>::UnstructuredSimplicialMesh(const Eigen::Matrix<double, 1, 1>& lbIn,
                                                                     const Eigen::Matrix<double, 1, 1>& ubIn,
                                                                     const Eigen::Matrix<unsigned int, 1, 1>& NIn)
{
  // allocate space for the nodes and elements
  unsigned int NumNodes = (NIn + Eigen::Matrix<unsigned int, 1, 1>::Ones(1, 1)).prod();
  unsigned int NumEles  = NIn.prod();

  Eles.resize(NumEles);     //,-1.0*Eigen::Matrix<double,2,1>::Ones(2,1));
  NodePos.resize(NumNodes); //, -1.0*Eigen::Matrix<double,1,1>::Ones(1,1));

  // get the quadrilateral mesh sizes
  double dx = (ubIn[0] - lbIn[0]) / NIn[0];

  // fill in the node positions
  for (unsigned int i = 0; i < NumNodes; ++i) {
    NodePos[i][0] = dx * i;
  }

  // fill in the elements
  for (unsigned int i = 0; i < NumEles; ++i) {
    Eigen::Matrix<double, 2, 1> CurrEle(2);
    CurrEle[0] = i;
    CurrEle[1] = i + 1;
    Eles[i]    = CurrEle;
  }
}

template<> UnstructuredSimplicialMesh<2>::UnstructuredSimplicialMesh(const Eigen::Matrix<double, 2, 1>& lbIn,
                                                                     const Eigen::Matrix<double, 2, 1>& ubIn,
                                                                     const Eigen::Matrix<unsigned int, 2, 1>& NIn)
{
  // allocate space for the nodes and elements
  unsigned int NumNodes = (NIn + Eigen::Matrix<unsigned int, 2, 1>::Ones(2, 1)).prod();
  unsigned int NumEles  = 2 * NIn.prod();


  Eles.resize(NumEles);
  NodePos.resize(NumNodes);

  // get the quadrilateral mesh sizes
  double dx = (ubIn[0] - lbIn[0]) / NIn[0];
  double dy = (ubIn[1] - lbIn[1]) / NIn[1];

  // fill in the node positions
  for (unsigned int j = 0; j < NIn[1] + 1; ++j) {
    for (unsigned int i = 0; i < NIn[0] + 1; ++i) {
      unsigned int ind = i + j * (NIn[0] + 1);
      NodePos[ind][0] = lbIn[0] + dx * i;
      NodePos[ind][1] = lbIn[1] + dy * j;
    }
  }

  // fill in the even elements
  unsigned int k = 0;
  for (unsigned int j = 0; j < NIn[1]; ++j) {
    for (unsigned int i = 0; i < NIn[0]; ++i) {
      // first, do lower triangular portion
      Eigen::Matrix<double, 3, 1> CurrEle(3);
      CurrEle[0] = i + j * (NIn[0] + 1);
      CurrEle[1] = CurrEle[0] + 1;
      CurrEle[2] = CurrEle[0] + NIn[0] + 1;
      Eles[k]    = CurrEle;

      // now do lower upper triangular portion
      CurrEle[0]  = i + j * (NIn[0] + 1) + 1;
      CurrEle[1]  = CurrEle[0] + NIn[0];
      CurrEle[2]  = CurrEle[0] + NIn[0] + 1;
      Eles[k + 1] = CurrEle;

      k += 2;
    }
  }
}

template<> UnstructuredSimplicialMesh<3>::UnstructuredSimplicialMesh(const Eigen::Matrix<double, 3, 1>& lbIn,
                                                                     const Eigen::Matrix<double, 3, 1>& ubIn,
                                                                     const Eigen::Matrix<unsigned int, 3, 1>& NIn)
{
  // allocate space for the nodes and elements
  unsigned int NumNodes = (NIn + Eigen::Matrix<unsigned int, 3, 1>::Ones(3, 1)).prod();
  unsigned int NumEles  = 6 * NIn.prod();

  Eles.resize(NumEles); //,-1.0*Eigen::Matrix<double,4,1>::Ones(2,1));
  NodePos.resize(NumNodes, -1.0 * Eigen::Matrix<double, 3, 1>::Ones(3, 1));

  // get the quadrilateral mesh sizes
  double dx = (ubIn[0] - lbIn[0]) / NIn[0];
  double dy = (ubIn[1] - lbIn[1]) / NIn[1];
  double dz = (ubIn[2] - lbIn[2]) / NIn[2];

  // fill in the node positions
  for (unsigned int k = 0; k < NIn[2] + 1; ++k) {
    for (unsigned int j = 0; j < NIn[1] + 1; ++j) {
      for (unsigned int i = 0; i < NIn[0] + 1; ++i) {
        unsigned int ind = i + j * (NIn[0] + 1) + k * (NIn[0] + 1) * (NIn[1] + 1);
        NodePos[ind][0] = lbIn[0] + dx * i;
        NodePos[ind][1] = lbIn[1] + dy * j;
        NodePos[ind][2] = lbIn[2] + dz * k;
      }
    }
  }

  for (unsigned int i = 0; i < NumEles; i += 6) {
    Eigen::Matrix<double, 4, 1> CurrEle(4);

    int elex = (i / 6) % NIn[0];
    int eley = int(floor(double(i / 6 - elex) / NIn[0])) % NIn[1];
    int elez = int(floor(double(i / 6 - elex - eley * NIn[0])) / (NIn[0] * NIn[1]));


    unsigned int p[8];
    p[0] = elex + eley * (NIn[0] + 1) + elez * (NIn[0] + 1) * (NIn[1] + 1);
    unsigned int n5 = p[0] + (NIn[0] + 1) * (NIn[1] + 1);
    p[1] = p[0] + 1;
    p[2] = p[0] + NIn[0] + 1;
    p[3] = p[0] + NIn[0] + 2;
    p[4] = n5;
    p[5] = n5 + 1;
    p[6] = n5 + NIn[0] + 1;
    p[7] = n5 + NIn[0] + 2;

    // first simplex
    CurrEle[0] = p[0];
    CurrEle[1] = p[4];
    CurrEle[2] = p[6];
    CurrEle[3] = p[5];

    Eles[i] = CurrEle;

    // second simplex
    CurrEle[0] = p[0];
    CurrEle[1] = p[2];
    CurrEle[2] = p[6];
    CurrEle[3] = p[5];

    Eles[i + 1] = CurrEle;

    // third simplex
    CurrEle[0] = p[0];
    CurrEle[1] = p[2];
    CurrEle[2] = p[5];
    CurrEle[3] = p[1];

    Eles[i + 2] = CurrEle;

    // fourth simplex
    CurrEle[0] = p[2];
    CurrEle[1] = p[5];
    CurrEle[2] = p[7];
    CurrEle[3] = p[6];

    Eles[i + 3] = CurrEle;

    // fifth simplex
    CurrEle[0] = p[1];
    CurrEle[1] = p[2];
    CurrEle[2] = p[3];
    CurrEle[3] = p[5];

    Eles[i + 4] = CurrEle;

    // sixth simplex
    CurrEle[0] = p[3];
    CurrEle[1] = p[2];
    CurrEle[2] = p[5];
    CurrEle[3] = p[7];

    Eles[i + 5] = CurrEle;
  }
}
}
}

/** Get the position of the a node in the mesh */
template<unsigned int dim>
Eigen::Matrix<double, dim, 1> UnstructuredSimplicialMesh<dim>::GetNodePos(unsigned int node) const
{
      assert(node < NodePos.size());
  return NodePos[node];
}

/** Get the geometric center of an element in the mesh. */
template<unsigned int dim>
Eigen::Matrix<double, dim, 1> UnstructuredSimplicialMesh<dim>::GetElePos(unsigned int ele) const
{
  // get the locations of all nodes on this element and take the mean
  Eigen::Matrix<double, dim, dim + 1> AllPts(dim, dim + 1);

  for (unsigned int d = 0; d < dim + 1; ++d) {
    AllPts.col(d) = GetNodePos(Eles[ele][d]);
  }

  // return the mean
  return AllPts.rowwise().sum() / (dim + 1);
}

/** Return a vector of unsigned integers for the center nodes around an element. */
template<unsigned int dim>
Eigen::Matrix<unsigned int, Eigen::Dynamic,
              Eigen::Dynamic> UnstructuredSimplicialMesh<dim>::GetNodes(unsigned int ele) const
{
  Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic> output(dim + 1, 1);

  for (unsigned int d = 0; d < dim + 1; ++d) {
    output(d) = Eles[ele][d];
  }
  return output;
}

/** Get the area or volume of an element. */
template<unsigned int dim>
double UnstructuredSimplicialMesh<dim>::EleSize(unsigned int ele) const
{
  Eigen::Matrix<double, dim, dim> VolMat(dim, dim);

  Eigen::Matrix<double, dim, 1> p0 = GetNodePos(Eles[ele][0]);

  for (unsigned int d = 1; d < dim + 1; ++d) {
    VolMat.col(d - 1) = GetNodePos(Eles[ele][d]) - p0;
  }

  return VolMat.determinant() / boost::math::factorial<double>(dim);
}

namespace muq {
namespace Utilities {
template class muq::Utilities::UnstructuredSimplicialMesh<1>;
template class muq::Utilities::UnstructuredSimplicialMesh<2>;
template class muq::Utilities::UnstructuredSimplicialMesh<3>;
}
}

