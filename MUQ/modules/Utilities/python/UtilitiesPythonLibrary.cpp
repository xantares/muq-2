/*
 *  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 *  You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 *  MIT UQ Library Copyright (C) 2013 MIT
 */

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/LogConfig.h"

#include "MUQ/Utilities/python/HDF5WrapperPython.h"
#include "MUQ/Utilities/python/HDF5LoggingPython.h"
#include "MUQ/Utilities/python/RandomGeneratorPython.h"
#include "MUQ/Utilities/python/PolynomialsPython.h"
#include "MUQ/Utilities/python/QuadraturePython.h"
#include "MUQ/Utilities/python/VariableCollectionPython.h"
#include "MUQ/Utilities/python/MultiIndexPython.h"


namespace py = boost::python;

#if MUQ_GLOG
namespace muq {
namespace Utilities {
PyInitializeLogging(py::list const& pyArgv)
{
  static bool initialized = false;

  if (!initialized) {
    initialized = true;
    int argc = py::len(pyArgv);

    if (argc == 0) {
      argc = 1;
      //GLOG holds the actual pointer we pass to it, so make is static so it stays around.
      static std::string exeName = "muqPython";
      google::InitGoogleLogging(exeName.c_str());
    } else {
      //GLOG holds the actual pointer we pass to it, so make is static so it stays around.
      static std::string val = py::extract<std::string>(pyArgv[0]);

      google::InitGoogleLogging(val.c_str());
    }
  } else {
    LOG(WARNING) << "Tried to re-initialize Glog!";
  }
}
} // namespace GeneralUtilities
} // namespace muq
#endif // if MUQ_GLOG

BOOST_PYTHON_MODULE(libmuqUtilities)
{
#if MUQ_GLOG
  py::def("InitializeLogging", muq::Utilities::PyInitializeLogging);
#endif // if MUQ_GLOG

  
  py::class_<muq::Utilities::RandomGeneratorTemporarySetSeed, boost::noncopyable> randGenTempSet("RandomGeneratorTemporarySetSeed", py::init<int>());
  
  muq::Utilities::ExportHDF5Wrapper();
  muq::Utilities::ExportHDF5Logging();

  muq::Utilities::ExportRandomGenerator();
  
  muq::Utilities::ExportRecursive();
  muq::Utilities::ExportHermite();
  muq::Utilities::ExportProbHermite();
  muq::Utilities::ExportLegendre();
  muq::Utilities::ExportMonomial();
  
  muq::Utilities::ExportQuadratureFamily();
  muq::Utilities::ExportGaussianQuadratureFamily();
  muq::Utilities::ExportClenshawCurtisQuadrature();
  muq::Utilities::ExportExpGrowthQuadratureFamily();
  muq::Utilities::ExportGaussChebyshevQuadrature();
  muq::Utilities::ExportGaussHermiteQuadrature();
  muq::Utilities::ExportGaussProbHermiteQuadrature();
  muq::Utilities::ExportGaussLegendreQuadrature();
  muq::Utilities::ExportGaussPattersonQuadrature();
  muq::Utilities::ExportLinearGrowthQuadratureFamily();
  
  muq::Utilities::ExportVariableCollection();
  muq::Utilities::ExportMultiIndex();
  muq::Utilities::ExportMultiIndexSet();
  muq::Utilities::ExportMultiIndexFactory();
}
