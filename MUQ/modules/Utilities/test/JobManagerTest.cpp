
#include "gtest/gtest.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#if MUQ_MPI
# include <mpi.h>
# include <boost/mpi.hpp>
namespace mpi = boost::mpi;
#endif // if MUQ_MPI

#include "MUQ/Utilities/JobManager.h"
#include "MUQ/Utilities/HDF5Wrapper.h"
#include "MUQ/Utilities/LogConfig.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace boost::property_tree;

string JobTestFn_Name(ptree const& tree)
{
  int num = tree.get<int>("CopyNumber");
  string uselessData = tree.get<string>("useless");
  
  string outputPath = "/test/" + uselessData + boost::lexical_cast<string>(num);

  return outputPath;
}

void JobTestFn(ptree const& tree)
{
  string   outputPath = JobTestFn_Name(tree);
  MatrixXd mat        = MatrixXd::Zero(3, 3);

  usleep(100000); //wait a little bit
  HDF5Wrapper::WriteMatrix(outputPath, mat);
}

REGISTER_JOB_FUNCTION(JobTestFn, JobTestFn_Name)
///Tests that jobs can write to a shared hdf5 and that it will wait for non-instantaneous jobs to complete
TEST(Utilities, JobManager)
{
  string filename = "results/tests/JobTest.h5";
 
  #ifdef MUQ_MPI
  unique_ptr<mpi::communicator> worldComm(new mpi::communicator);
  if(worldComm->size() <= 1){
	  std::cout << "Only 1 worker, cannot test JobManager" << std::endl;
	  return;
  }
  #endif

  HDF5Wrapper::OpenFile(filename);

  ptree jobConfig;
  int   numJobs = 10;

  jobConfig.put("Job.NumberOfCopies", numJobs);
  jobConfig.put("Job.Function", "JobTestFn");
  jobConfig.put("Job.Data.useless", "foo");
  JobManager::RunJobs(jobConfig); //job makes a bunch of datasets
  LOG(INFO) << "closing file";
  HDF5Wrapper::CloseFile();       //close so that it gathers them all

  HDF5Wrapper::OpenFile(filename);
  #ifdef MUQ_MPI

  //if mpi, only try this test on the top rank, as it'll read the real file
  if (worldComm->rank() == 0) {
    #endif // ifdef MUQ_MPI


  //so check that they're all there
  for (int i = 0; i < numJobs; ++i) {
    string datasetName = "/test/foo" + boost::lexical_cast<string>(i);
    EXPECT_TRUE(HDF5Wrapper::DoesDataSetExist(datasetName));
  }

  LOG(INFO) << "closing file";


    #ifdef MUQ_MPI
}

 #endif // ifdef MUQ_MPI
  HDF5Wrapper::CloseFile();
  LOG(INFO) << "job test over";
}

TEST(Utilities, JobManager_MultiJobTest)
{
  string filename = "results/tests/JobTest2.h5";
  
  #ifdef MUQ_MPI
  unique_ptr<mpi::communicator> worldComm(new mpi::communicator);
  if(worldComm->size() <= 1){
	  std::cout << "Only 1 worker, cannot test JobManager" << std::endl;
	  return;
  }
  #endif
  
  HDF5Wrapper::OpenFile(filename);
  
  ptree jobConfig;
  int   numJobs = 10;
  
  boost::property_tree::ptree job1;
  job1.put("NumberOfCopies", numJobs);
  job1.put("Function", "JobTestFn");
  job1.put("Data.useless", "multifoo");
  
  boost::property_tree::ptree job2;
  job2.put("NumberOfCopies", numJobs);
  job2.put("Function", "JobTestFn");
  job2.put("Data.useless", "multifoo2");
  
  boost::property_tree::ptree job3;
  job3.put("NumberOfCopies", numJobs);
  job3.put("Function", "JobTestFn");
  job3.put("Data.useless", "multifoo3");
  
  jobConfig.add_child("Job",job1);
  jobConfig.add_child("Job",job2);
  jobConfig.add_child("Job",job3);
  
  
  JobManager::RunJobs(jobConfig); //job makes a bunch of datasets
  LOG(INFO) << "closing file";
  HDF5Wrapper::CloseFile();       //close so that it gathers them all
  
  HDF5Wrapper::OpenFile(filename);
  
  #ifdef MUQ_MPI
  //if mpi, only try this test on the top rank, as it'll read the real file
  if (worldComm->rank() == 0) {
  #endif // ifdef MUQ_MPI
    
    
    //so check that they're all there
    for (int i = 0; i < numJobs; ++i) {
      string datasetName = "/test/multifoo" + boost::lexical_cast<string>(i);
      EXPECT_TRUE(HDF5Wrapper::DoesDataSetExist(datasetName));
      
      datasetName = "/test/multifoo2" + boost::lexical_cast<string>(i);
      EXPECT_TRUE(HDF5Wrapper::DoesDataSetExist(datasetName));
      
      datasetName = "/test/multifoo3" + boost::lexical_cast<string>(i);
      EXPECT_TRUE(HDF5Wrapper::DoesDataSetExist(datasetName));
    }
    
    LOG(INFO) << "closing file";
    
    
  #ifdef MUQ_MPI
  }
  #endif // ifdef MUQ_MPI
  
  HDF5Wrapper::CloseFile();
  LOG(INFO) << "job test over";
}
