
#include "MUQ/Geostatistics/RationalFifthKernel.h"

#include <assert.h>

#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities/LogConfig.h"

using namespace muq::Geostatistics;

REGISTER_KERNEL_DEF_TYPE(RationalFifthKernel)
/** Construct the kernel from the characteristic length scale, power, and variance.
 */
RationalFifthKernel::RationalFifthKernel(boost::property_tree::ptree& ptree) : IsotropicCovKernel(2), logC(0), logVar(0)
{
  LOG(INFO) << "Creating RationalFifthKernel";
  logC = log(ptree.get("Kernel.Length", exp(logC)));
  LOG(INFO) << "with Kernel.Length" <<  exp(logC);

  logVar = log(ptree.get("Kernel.Variance", exp(logVar)));
  LOG(INFO) << "with Kernel.Variance" << exp(logVar);
}

RationalFifthKernel::RationalFifthKernel(double cIn,
                                         double VarIn) : IsotropicCovKernel(2), logC(log(cIn)),
                                                         logVar(log(VarIn))
{}

/** This function evaluates the power kernel.
 */
double RationalFifthKernel::DistKernel(double d) const
{
  double result;
  double c = exp(logC);
  double x = d / c;

  if (d < c) {
    result = -0.25 * pow(x, 5) + 0.5 * pow(x, 4) + 0.625 * pow(x, 3) - (5.0 / 3.0) * pow(x, 2) + 1;
  } else if (d < 2.0 * c) {
    result =
      (1.0 /
       12.0) *
      pow(x, 5) - 0.5 * pow(x, 4) + (5.0 / 8.0) * pow(x, 3) + (5.0 / 3.0) * pow(x, 2) - 5.0 * x + 4 - (2.0 / 3.0) * pow(
        x,
        -1.0);
  } else {
    result = 0.0;
  }

  assert(!std::isnan(result)); //can underflow during optimization

  // return the result
  return exp(logVar) * result;
}

/** Set the covariance kernel parameters (Length, power, variance) from a vector of parameters.  */
void RationalFifthKernel::SetParms(const Eigen::VectorXd& theta)
{
  // check the vector length
  assert(theta.size() == 2);

  // set the parameters
  logC   = theta[0];
  logVar = theta[1];
}

/** Compute the gradient of the covariance kernel wrt the gradient. */
void RationalFifthKernel::GetGrad(double d, Eigen::VectorXd& Grad) const
{
  Grad.resize(2);
  double c       = exp(logC);
  double x       = d / c;
  double dxdlogc = -1.0 * d / exp(-1.0 * logC);
  double dfdx;
  if (d < c) {
    dfdx = -0.25 * 5 * pow(x, 4) + 2.0 * pow(x, 3) + 0.625 * 3.0 * pow(x, 2) - (10.0 / 3.0) * x;
  } else if (d < 2.0 * c) {
    dfdx =
      (5.0 /
       12.0) *
      pow(x, 4) - 2.0 * pow(x, 3) + (15.0 / 8.0) * pow(x, 2) + (1.0 / 3.0) * pow(x, 1) - 5.0 + (2.0 / 3.0) * pow(
        x,
        -2.0);
  } else {
    dfdx = 0.0;
  }

  Grad(0) = dfdx * dxdlogc; //wrt logC
  Grad(1) = DistKernel(d);  //wrt log var
}

/** This function will return the derivative of the kernal value wrt d */
double RationalFifthKernel::KernelRadialDerivative(double const r)
{
  double c    = exp(logC);
  double x    = r / c;
  double dxdr = -1.0 * r / exp(-1.0 * logC);
  double dfdx;

  if (r < c) {
    dfdx = -0.25 * 5 * pow(x, 4) + 2.0 * pow(x, 3) + 0.625 * 3.0 * pow(x, 2) - (10.0 / 3.0) * x;
  } else if (r < 2.0 * c) {
    dfdx =
      (5.0 /
       12.0) *
      pow(x, 4) - 2.0 * pow(x, 3) + (15.0 / 8.0) * pow(x, 2) + (1.0 / 3.0) * pow(x, 1) - 5.0 + (2.0 / 3.0) * pow(
        x,
        -2.0);
  } else {
    dfdx = 0.0;
  }

  return dfdx * dxdr;
}
