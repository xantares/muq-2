
#include "MUQ/Geostatistics/PowerKernel.h"

#include <assert.h>

#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities/LogConfig.h"

using namespace muq::Geostatistics;

REGISTER_KERNEL_DEF_TYPE(PowerKernel)
/** Construct the kernel from the characteristic length scale, power, and variance.
 */
PowerKernel::PowerKernel(boost::property_tree::ptree& ptree) : IsotropicCovKernel(2), logLength(0), Power(2), logVar(0)
{
  LOG(INFO) << "Creating PowerKernel";
  Power = ptree.get("Kernel.Exponent", Power);
  LOG(INFO) << "with Kernel.Exponent" << Power;
  logLength = log(ptree.get("Kernel.Length", exp(logLength)));
  LOG(INFO) << "with Kernel.Length" <<  exp(logLength);

  logVar = log(ptree.get("Kernel.Variance", exp(logVar)));
  LOG(INFO) << "with Kernel.Variance" << exp(logVar);
}

PowerKernel::PowerKernel(double LengthIn, double PowerIn,
                         double VarIn) : IsotropicCovKernel(2), logLength(log(LengthIn)), Power(PowerIn),
                                         logVar(log(VarIn))
{}

/** This function evaluates the power kernel.
 */
double PowerKernel::DistKernel(double d) const
{
  double result = exp(logVar) * exp(-1.0 * pow(d / exp(logLength), Power) / Power);

  assert(!std::isnan(result)); //can underflow during optimization
  // evaluate the kernel
  return result;
}

/** Set the covariance kernel parameters (Length, power, variance) from a vector of parameters.  */
void PowerKernel::SetParms(const Eigen::VectorXd& theta)
{
  // check the vector length
  assert(theta.size() == 2);

  // set the parameters
  logLength = theta[0];

  //     Power = theta[1];
  logVar = theta[1];
}

/** Compute the gradient of the covariance kernel wrt the gradient. */
void PowerKernel::GetGrad(double d, Eigen::VectorXd& Grad) const
{
  Grad.resize(2);

  double k = DistKernel(d);

  Grad(0) = k * pow(d, Power) * exp(-logLength) / Power; //wrt log length
  Grad(1) = k;                                           //wrt log var
}

double PowerKernel::KernelRadialDerivative(double const r)
{
  double Length = exp(logLength);

  return -DistKernel(r) * pow(r / Length, Power - 1) / Length;
}
