
#include "MUQ/Inference/MCMC/MHProposal.h"

// standard library includes
#include <string>

// boost includes
#include <boost/property_tree/ptree.hpp>

// other MUQ includes
#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Modelling/RandVarBase.h"

#include "MUQ/Utilities/HDF5Wrapper.h"


// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;


REGISTER_MCMCPROPOSAL(MHProposal);


MHProposal::MHProposal(std::shared_ptr<AbstractSamplingProblem> inferenceProblem,
                       boost::property_tree::ptree            & properties) : MCMCProposal(inferenceProblem, properties)
{
  LOG(INFO) << "Constructing Metropolis-Hastings Proposal";

  propSize = ReadAndLogParameter(properties, "MCMC.MHProposal.PropSize", 0.5);

  setProposal(propSize);

  samplingProblem->SetStateComputations(false, false, false);
}

/** simply sets the proposal to newProp.  Note that this is just pointing the proposal at an underlying distribution, so
 * changing newProp anywhere will result in a different proposal distribution.
 */
void MHProposal::setProposal(std::shared_ptr<muq::Modelling::GaussianPair> newProp)
{
  proposal = newProp;
}

/** set the proposal to a zero mean isotropic gaussian. */
void MHProposal::setProposal(double var)
{
  // build a new isotropic gaussian proposal with var width and zero mean
  Eigen::VectorXd newMu = Eigen::VectorXd::Zero(samplingProblem->samplingDim);

  // create the Gaussian proposal
  proposal = make_shared<GaussianPair>(newMu, var);
}

/** Function performs one MCMC step using the fixed proposal and simple accept/reject stage through the
 *  Metropolis-Hastings Rule */
std::shared_ptr<muq::Inference::MCMCState> MHProposal::DrawProposal(
  std::shared_ptr<muq::Inference::MCMCState>    currentState,
  std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry,
  int const currIteration)
{
  // draw the delta
  Eigen::VectorXd delta = proposal->rv->Sample();

  //compute the new state
  Eigen::VectorXd prop = currentState->state + delta;

  //compute the entire state of the chain, which runs the forward model
  return samplingProblem->ConstructState(prop, currIteration, logEntry);
}

double MHProposal::ProposalDensity(std::shared_ptr<muq::Inference::MCMCState> currentState,
                                   std::shared_ptr<muq::Inference::MCMCState> proposedState)
{
  assert(proposedState);
  assert(currentState);
  
  //since the proposal is centered, the density is based on the difference
  return proposal->density->LogDensity(currentState->state - proposedState->state);
}

void MHProposal::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteStringAttribute(groupName, prefix + "Proposal", "MH");
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "MH - Variance", propSize);
}

