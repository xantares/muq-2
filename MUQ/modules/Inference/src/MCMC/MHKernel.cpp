
#include "MUQ/Inference/MCMC/MHKernel.h"

// standard library includes
#include <string>

// boost includes
#include <boost/property_tree/ptree.hpp>

// other MUQ includes
#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Modelling/RandVarBase.h"

#include "MUQ/Utilities/HDF5Wrapper.h"


// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;

REGISTER_MCMCKERNEL(MHKernel);


MHKernel::MHKernel(std::shared_ptr<AbstractSamplingProblem> inferenceProblem,
                   boost::property_tree::ptree            & properties) : MCMCKernel(inferenceProblem, properties)
{
  //create the proposal
  proposal = MCMCProposal::Create(inferenceProblem, properties);
}

void MHKernel::PrintStatus() const
{
  std::cout << "  MH: Acceptance rate = " << std::setw(5) << std::fixed << std::setprecision(1) << 100.0 *
    GetAccept() << "%\n";
}

/** Function performs one MCMC step using the fixed proposal and simple accept/reject stage through the
 *  Metropolis-Hastings Rule */
std::shared_ptr<muq::Inference::MCMCState> MHKernel::ConstructNextState(
  std::shared_ptr<muq::Inference::MCMCState>    currentState,
  int const                                     iteration,
  std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry)
{
  totalSteps++;

  //use the proposal to create the state
  auto proposedState = proposal->DrawProposal(currentState, logEntry, iteration);

  //if the proposed state does not have numeric likelihood or prior, reject it immediately
  if (!proposedState) {
    logEntry->loggedData["isAccepted"] = Eigen::VectorXd::Constant(1, 0);
    proposal->PostProposalProcessing(currentState, MCMCProposal::ProposalStatus::rejected, iteration, logEntry);
    return currentState;
  }

  //use a helper function
  double gamma = ComputeAcceptanceProbability(currentState, proposedState);

  logEntry->loggedData["acceptanceProbability"] = Eigen::VectorXd::Constant(1, gamma);

  //actually accept or reject based on a that probability
  if (RandomGenerator::GetUniform() < gamma) {
    logEntry->loggedData["isAccepted"] = Eigen::VectorXd::Constant(1, 1);

    //give the proposal a chance to adapt
    proposal->PostProposalProcessing(proposedState, MCMCProposal::ProposalStatus::accepted, iteration, logEntry);

    numAccepts++;
    return proposedState;
  } else {
    logEntry->loggedData["isAccepted"] = Eigen::VectorXd::Constant(1, 0);

    //give the proposal a chance to adapt
    proposal->PostProposalProcessing(currentState, MCMCProposal::ProposalStatus::rejected, iteration, logEntry);

    return currentState;
  }
}

double MHKernel::ComputeAcceptanceProbability(std::shared_ptr<muq::Inference::MCMCState> currentState,
                                              std::shared_ptr<muq::Inference::MCMCState> proposedState)
{
  //this part is reused in IncrementalApproximationMCMC::ComputeAcceptanceProbability
  assert(currentState); //current state must exist
  if (!proposedState) {
    return 0;           //if proposed state is null, accept with probability zero (reject it)
  } else {
    //the standard MH correction probability
    return exp(proposedState->GetDensity() - currentState->GetDensity() -
               proposal->ProposalDensity(currentState,
                                         proposedState) + proposal->ProposalDensity(proposedState, currentState));
  }
}

double MHKernel::GetAccept() const
{
  return static_cast<double>(numAccepts) / static_cast<double>(totalSteps);
}

void MHKernel::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteStringAttribute(groupName, prefix + "Kernel", "Metropolis Hastings");

  proposal->WriteAttributes(groupName, prefix);
}
