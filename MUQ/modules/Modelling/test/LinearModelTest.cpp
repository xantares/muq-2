
#include <iostream>
#include <math.h>
#include <vector>

// google testing library
#include "gtest/gtest.h"

// muq related includes
#include "MUQ/Modelling/ModParameter.h"
#include "MUQ/Modelling/LinearModel.h"
#include "MUQ/Modelling/ModGraphOperators.h"
#include "MUQ/Modelling/ModGraphPiece.h"

using namespace muq::Modelling;
using namespace std;

TEST(ModellingLinearModelTest, AffineEval)
{
  unsigned int dim = 20;

  // create a random matrix
  Eigen::MatrixXd A = Eigen::MatrixXd::Random(10, dim);

  // create a model parameter filled with 0.1
  Eigen::VectorXd input = 0.1 * Eigen::VectorXd::Ones(dim);
  auto x                = make_shared<ModParameter>(input);

  // get the output
  auto b = A * x;

  // get the linear model output
  Eigen::VectorXd Actual = ModGraphPiece::Create(b)->Evaluate();

  // compare to what we should be getting
  Eigen::VectorXd Truth = A * input;

  for (unsigned int i = 0; i < 10; ++i) {
    EXPECT_DOUBLE_EQ(Truth[i], Actual[i]);
  }
}

TEST(ModellingLinearModelTest, LinearEval)
{
  unsigned int dim = 20;

  // create a random matrix
  Eigen::MatrixXd A = Eigen::MatrixXd::Random(10, dim);
  Eigen::MatrixXd b = Eigen::VectorXd::Random(10);

  // create a model parameter filled with 0.1
  Eigen::VectorXd input = 0.1 * Eigen::VectorXd::Ones(dim);

  auto mod = make_shared<LinearModel>(b, A);

  // get the linear model output
  Eigen::VectorXd Actual = mod->Evaluate(input);

  // compare to what we should be getting
  Eigen::VectorXd Truth = A * input + b;

  for (unsigned int i = 0; i < 10; ++i) {
    EXPECT_DOUBLE_EQ(Truth[i], Actual[i]);
  }
}

TEST(ModellingLinearModelTest, AdditiveEval)
{
  unsigned int dim = 20;

  // create a random matrix
  Eigen::MatrixXd b = Eigen::VectorXd::Random(dim);

  // create a model parameter filled with 0.1
  Eigen::VectorXd input = 0.1 * Eigen::VectorXd::Ones(dim);

  auto mod = make_shared<LinearModel>(b, true);

  // get the linear model output
  Eigen::VectorXd Actual = mod->Evaluate(input);

  // compare to what we should be getting
  Eigen::VectorXd Truth = input + b;

  for (unsigned int i = 0; i < dim; ++i) {
    EXPECT_DOUBLE_EQ(Truth[i], Actual[i]);
  }
}

TEST(ModellingLinearModelTest, MultipleMatsForwardEval)
{
  unsigned int dim1 = 20;
  unsigned int dim2 = 10;

  // create two random matrices
  Eigen::MatrixXd A1 = Eigen::MatrixXd::Random(dim2, dim1);
  Eigen::MatrixXd A2 = Eigen::MatrixXd::Random(10, dim2);

  // create a model parameter filled with 0.1
  Eigen::VectorXd input = 0.1 * Eigen::VectorXd::Ones(dim1);
  auto x                = make_shared<ModParameter>(input);

  // get the output
  auto b1 = A1 * x;
  auto b  = A2 * b1;

  // get the linear model output
  Eigen::VectorXd Actual = ModGraphPiece::Create(b)->Evaluate();

  // compare to what we should be getting
  Eigen::VectorXd Truth = A2 * A1 * input;

  for (unsigned int i = 0; i < 10; ++i) {
    EXPECT_NEAR(Truth[i], Actual[i], 1e-10);
  }
}

TEST(ModellingLinearModelTest, LinearEvalExpression)
{
  unsigned int dim = 20;

  // create a random matrix
  Eigen::MatrixXd A = Eigen::MatrixXd::Random(10, dim);
  Eigen::MatrixXd b = Eigen::VectorXd::Random(10);

  // create a model parameter filled with 0.1
  Eigen::VectorXd input = 0.1 * Eigen::VectorXd::Ones(dim);

  auto x   = make_shared<ModParameter>(input);
  auto mod = A * x + b;

  // get the linear model output
  Eigen::VectorXd Actual = ModGraphPiece::Create(mod)->Evaluate();

  // compare to what we should be getting
  Eigen::VectorXd Truth = A * input + b;

  for (unsigned int i = 0; i < 10; ++i) {
    EXPECT_DOUBLE_EQ(Truth[i], Actual[i]);
  }
}
