import unittest
import numpy as np

import libmuqModelling

class UniformDensTest(unittest.TestCase):
    def testDens(self):
        lb = [0, 0]
        ub = [1, 1]

        box  = libmuqModelling.UniformBox(lb, ub)
        dens = libmuqModelling.UniformDensity(box)

        testOut = [2.0]*2
        testIn  = [0.5]*2

        self.assertEqual(np.exp(dens.LogDensity([testOut])), 0.0)
        self.assertEqual(dens.LogDensity([testIn]), 1.0)

