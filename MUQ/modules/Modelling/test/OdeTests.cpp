// include the google testing header
#include "gtest/gtest.h"
#include <boost/property_tree/ptree.hpp>

// include the ODE ModPiece header
#include "MUQ/Modelling/OdeModPiece.h"

// include other MUQ models
#include "MUQ/Modelling/LinearModel.h"

using namespace std;
using namespace muq::Modelling;


class OdeRhs : public ModPiece {
public:

  // inputSizes must be 1, dim for time and state size
  OdeRhs(Eigen::VectorXi const& inputSizes) : ModPiece(inputSizes,    // input sizes = [1; state dim; anything...]
                                                       inputSizes(1), // output dimensions is the dimension of the state
                                                       true,          // has direct gradient
                                                       true,          // has direct jacobian
                                                       true,          // has direct jacobian action
                                                       false,         // has direct hessian
                                                       false)
  {
    assert(inputSizes.rows() == 3);
    assert(inputSizes(0) == 1);
    assert(inputSizes(1) == 2); // my state dimension is always 2
  }                             // is Random

protected:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override
  {
    double time           = inputs.at(0) (0);
    Eigen::VectorXd state = inputs.at(1);
    double springConst    = inputs.at(2) (0);
    Eigen::VectorXd rhs(2);

    rhs(0) = state(1);
    rhs(1) = -springConst *state(0);
    return rhs;
  }

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override
  {
    double springConst = input.at(2) (0);

    if (inputDimWrt == 1) { // derivative wrt state
      Eigen::VectorXd output(2);
      output <<  target(1), -springConst *target(0);
      return output;
    } else if (inputDimWrt == 2) { // derivative wrt spring constant
      Eigen::VectorXd output(2);
      Eigen::VectorXd state = input.at(1);
      output << 0, -state(0) * target(0);
      return output;
    } else {
      return Eigen::VectorXd::Zero(outputSize);
    }
  }

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& inputs,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override
  {
    double springConst = inputs.at(2) (0);

    if (inputDimWrt == 1) { // derivative wrt state
      Eigen::VectorXd grad(2);
      grad <<  -springConst *sensitivity(1), sensitivity(0);
      return grad;
    } else if (inputDimWrt == 2) { // derivative wrt spring constant
      Eigen::VectorXd grad(1);
      Eigen::VectorXd state = inputs.at(1);
      grad << -state(0) * sensitivity(1);
      return grad;
    } else {
      return Eigen::VectorXd::Zero(inputSizes(inputDimWrt));
    }
  }

  Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt)
  {
    double springConst = inputs.at(2) (0);

    if (inputDimWrt == 1) { // derivative wrt state
      Eigen::MatrixXd jac(2, 2);
      jac <<  0,           1, -springConst, 0;
      return jac;
    } else if (inputDimWrt == 2) {    // derivative wrt spring constant
      Eigen::MatrixXd jac(2, 1);
      Eigen::VectorXd state = inputs.at(1);
      jac << 0, -state(0);
      return jac;
    } else {
      return Eigen::MatrixXd::Zero(outputSize, inputSizes(inputDimWrt));
    }
  }
};

class Observer : public ModPiece {
public:

  Observer(Eigen::VectorXi const& inputSizes) : ModPiece(inputSizes, // input sizes = [1; state dim; anything...]
                                                         1,          // dimension of the state
                                                         false,      // has direct gradient
                                                         true,       // has direct jacobian
                                                         false,      // has direct jacobian action
                                                         false,      // has direct hessian
                                                         false)
  {
    assert(inputSizes(0) == 1);
    assert(inputSizes(1) == 2); // my state dimension is always 2
  }

protected:

  Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override
  {
    return inputs.at(1) (0) * Eigen::VectorXd::Ones(1);
  }

  // Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& inputs,
  //                           Eigen::VectorXd const             & sensitivity,
  //                           int const                           inputDimWrt) override{
  //   if(inputDimWrt==1){
  //     return sensitivity(0)*Eigen::VectorXd::Ones(1);
  //   }else{
  //     return Eigen::VectorXd::Zero(inputSizes(inputDimWrt));
  //   }
  // }

  Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt)
  {
    if (inputDimWrt == 1) {
      Eigen::MatrixXd jac(1, 2);
      jac <<  1, 0;
      return jac;
    } else {
      return Eigen::MatrixXd::Zero(outputSize, inputSizes(inputDimWrt));
    }
  }
};


class ModellingOdeTest : public::testing::Test {
protected:

  // set up the mesh that we will use, as well as an empty covariance matrix
  virtual void SetUp()
  {
    obsTimes.setLinSpaced(10, 0.0, 2.0); // the times we want to observe the output

    inputSizes.resize(3);
    inputSizes << 1, 2, 1;               // time=scalar, state=lenght 2, spring constant=scalar

    // define the ODE right hand side: du/dt = f(t,u)
    f = make_shared<OdeRhs>(inputSizes);

    // define an "observation" function.  In this case, just extract the second component of the state
    g = make_shared<Observer>(inputSizes);

    // create a parameter list to hold the ODE solver parameters
    params.put("CVODES.ATOL", 1e-13);           // absolute tolerance
    params.get("CVODES.RTOL", 1e-13);           // relative tolerance
    params.put("CVODES.Method", "BDF");         // either BDF or Adams
    params.put("CVODES.SolveMethod", "Newton"); // either Newton or Iter
    params.put("CVODES.MaxStepSize", 8e-4);     // maximum step size, should be small for accurate discrete
    params.put("CVODES.CheckPtGap", 40);        // number of steps to take between checkpoints

    // set the initial condition
    u0.resize(2);
    u0 << 1, 0; // the initial velocity (i.e. u0(1)) must be zero in this test

    // set the spring constant
    springConstant = 0.12 * Eigen::VectorXd::Ones(1);
  }

  Eigen::VectorXi inputSizes;
  Eigen::VectorXd springConstant, u0, obsTimes;
  std::shared_ptr<ModPiece>   f, g;
  boost::property_tree::ptree params;
};


TEST_F(ModellingOdeTest, LinearOscillatorSolution_dense)
{
  params.put("CVODES.LinearSolver", "Dense");
  auto u = make_shared<OdeModPiece>(f, g, obsTimes, params);

  // Integrate the ode
  Eigen::VectorXd output = u->Evaluate(u0, springConstant);

  // compare the ode modpiece output with the analytic solution
  double omega = sqrt(springConstant(0));
  double c1    = u0(0);

  for (int i = 0; i < obsTimes.size(); ++i) {
    double t = obsTimes(i);
    EXPECT_NEAR(u0(0) * cos(omega * t), output(i), 1e-3);
  }
}

TEST_F(ModellingOdeTest, LinearOscillatorSolution_IdentityObserver)
{
  params.put("CVODES.LinearSolver", "Dense");
  auto u = make_shared<OdeModPiece>(f, obsTimes, params);

  // Integrate the ode
  Eigen::VectorXd output = u->Evaluate(u0, springConstant);

  // compare the ode modpiece output with the analytic solution
  double omega = sqrt(springConstant(0));
  double c1    = u0(0);

  for (int i = 0; i < obsTimes.size(); ++i) {
    double t = obsTimes(i);
    EXPECT_NEAR(u0(0) * cos(omega * t), output(2*i), 1e-3);
  }
}

TEST_F(ModellingOdeTest, LinearOscillatorSolution_spgmr)
{
  params.put("CVODES.LinearSolver", "SPGMR");
  auto u = make_shared<OdeModPiece>(f, g, obsTimes, params);
  // Integrate the ode
  Eigen::VectorXd output = u->Evaluate(u0, springConstant);

  // compare the ode modpiece output with the analytic solution
  double omega = sqrt(springConstant(0));
  double c1    = u0(0);

  for (int i = 0; i < obsTimes.size(); ++i) {
    double t = obsTimes(i);
    EXPECT_NEAR(u0(0) * cos(omega * t), output(i), 1e-3);
  }
}

TEST_F(ModellingOdeTest, LinearOscillatorSolution_spbcg)
{
  params.put("CVODES.LinearSolver", "SPBCG");
  auto u = make_shared<OdeModPiece>(f, g, obsTimes, params);

  // Integrate the ode
  Eigen::VectorXd output = u->Evaluate(u0, springConstant);

  // compare the ode modpiece output with the analytic solution
  double omega = sqrt(springConstant(0));
  double c1    = u0(0);

  for (int i = 0; i < obsTimes.size(); ++i) {
    double t = obsTimes(i);
    EXPECT_NEAR(u0(0) * cos(omega * t), output(i), 1e-3);
  }
}

TEST_F(ModellingOdeTest, LinearOscillatorSolution_sptfqmr)
{
  params.put("CVODES.LinearSolver", "SPTFQMR");
  auto u = make_shared<OdeModPiece>(f, g, obsTimes, params);

  // Integrate the ode
  Eigen::VectorXd output = u->Evaluate(u0, springConstant);

  // compare the ode modpiece output with the analytic solution
  double omega = sqrt(springConstant(0));
  double c1    = u0(0);

  for (int i = 0; i < obsTimes.size(); ++i) {
    double t = obsTimes(i);
    EXPECT_NEAR(u0(0) * cos(omega * t), output(i), 1e-3);
  }
}

TEST_F(ModellingOdeTest, LinearOscillatorSolution_functional)
{
  params.put("CVODES.Method", "Adams");     // either BDF or Adams
  params.put("CVODES.SolveMethod", "Iter"); // either Newton or Iter
  auto u = make_shared<OdeModPiece>(f, g, obsTimes, params);

  // Integrate the ode
  Eigen::VectorXd output = u->Evaluate(u0, springConstant);

  // compare the ode modpiece output with the analytic solution
  double omega = sqrt(springConstant(0));
  double c1    = u0(0);

  for (int i = 0; i < obsTimes.size(); ++i) {
    double t = obsTimes(i);
    EXPECT_NEAR(u0(0) * cos(omega * t), output(i), 1e-3);
  }
}


TEST_F(ModellingOdeTest, LinearOscillatorForwardSens)
{
  auto u = make_shared<OdeModPiece>(f, g, obsTimes, params);

  // Integrate the ode and forward sensitivity equations
  Eigen::MatrixXd jac0 = u->Jacobian(u0, springConstant, 0);
  Eigen::MatrixXd jac1 = u->Jacobian(u0, springConstant, 1);

  double omega = sqrt(springConstant(0));

  // verify jac0
  for (int i = 0; i < obsTimes.size(); ++i) {
    double t = obsTimes(i);
    EXPECT_NEAR(cos(omega * t), jac0(i, 0), 1e-3);
  }

  // verify jac1
  for (int i = 0; i < obsTimes.size(); ++i) {
    double t = obsTimes(i);
    EXPECT_NEAR(-0.5 / omega * u0(0) * t * sin(omega * t), jac1(i, 0), 1e-3);
  }
}


TEST_F(ModellingOdeTest, LinearOscillatorAdjoint_dense)
{
  params.put("CVODES.LinearSolver", "Dense");
  auto u = make_shared<OdeModPiece>(f, g, obsTimes, params);

  int ind = obsTimes.size() - 1;

  // Integrate the ode and forward sensitivity equations
  Eigen::VectorXd sens = Eigen::VectorXd::Zero(u->outputSize);
  sens(ind) = 1;
  Eigen::VectorXd grad0 = u->Gradient(u0, springConstant, sens, 0);
  Eigen::VectorXd grad1 = u->Gradient(u0, springConstant, sens, 1);

  double tend = obsTimes(ind);
  EXPECT_NEAR(cos(sqrt(springConstant(0)) * tend),                                                 grad0(0), 1e-2);

  EXPECT_NEAR(-0.5 * u0(0) * sin(sqrt(springConstant(0)) * tend) * tend / sqrt(springConstant(0)), grad1(0), 1e-2);
}

TEST_F(ModellingOdeTest, LinearOscillatorAdjoint_spgmr)
{
  params.put("CVODES.LinearSolver", "SPGMR");
  auto u = make_shared<OdeModPiece>(f, g, obsTimes, params);

  int ind = obsTimes.size() - 1;

  // Integrate the ode and forward sensitivity equations
  Eigen::VectorXd sens = Eigen::VectorXd::Zero(u->outputSize);
  sens(ind) = 1;
  Eigen::VectorXd grad0 = u->Gradient(u0, springConstant, sens, 0);
  Eigen::VectorXd grad1 = u->Gradient(u0, springConstant, sens, 1);

  double tend = obsTimes(ind);
  EXPECT_NEAR(cos(sqrt(springConstant(0)) * tend),                                                 grad0(0), 1e-2);

  EXPECT_NEAR(-0.5 * u0(0) * sin(sqrt(springConstant(0)) * tend) * tend / sqrt(springConstant(0)), grad1(0), 1e-2);
}

TEST_F(ModellingOdeTest, LinearOscillatorAdjoint_spbcg)
{
  params.put("CVODES.LinearSolver", "SPBCG");
  auto u = make_shared<OdeModPiece>(f, g, obsTimes, params);

  int ind = obsTimes.size() - 1;

  // Integrate the ode and forward sensitivity equations
  Eigen::VectorXd sens = Eigen::VectorXd::Zero(u->outputSize);
  sens(ind) = 1;
  Eigen::VectorXd grad0 = u->Gradient(u0, springConstant, sens, 0);
  Eigen::VectorXd grad1 = u->Gradient(u0, springConstant, sens, 1);

  double tend = obsTimes(ind);
  EXPECT_NEAR(cos(sqrt(springConstant(0)) * tend),                                                 grad0(0), 1e-2);

  EXPECT_NEAR(-0.5 * u0(0) * sin(sqrt(springConstant(0)) * tend) * tend / sqrt(springConstant(0)), grad1(0), 1e-2);
}

TEST_F(ModellingOdeTest, LinearOscillatorAdjoint_sptfqmr)
{
  params.put("CVODES.LinearSolver", "SPTFQMR");
  auto u = make_shared<OdeModPiece>(f, g, obsTimes, params);

  int ind = obsTimes.size() - 1;

  // Integrate the ode and forward sensitivity equations
  Eigen::VectorXd sens = Eigen::VectorXd::Zero(u->outputSize);
  sens(ind) = 1;
  Eigen::VectorXd grad0 = u->Gradient(u0, springConstant, sens, 0);
  Eigen::VectorXd grad1 = u->Gradient(u0, springConstant, sens, 1);

  double tend = obsTimes(ind);
  EXPECT_NEAR(cos(sqrt(springConstant(0)) * tend),                                                 grad0(0), 1e-2);

  EXPECT_NEAR(-0.5 * u0(0) * sin(sqrt(springConstant(0)) * tend) * tend / sqrt(springConstant(0)), grad1(0), 1e-2);
}


TEST_F(ModellingOdeTest, LinearOscillatorAdjoint_functional)
{
  params.put("CVODES.Method", "Adams");     // either BDF or Adams
  params.put("CVODES.SolveMethod", "Iter"); // either Newton or Iter
  auto u = make_shared<OdeModPiece>(f, g, obsTimes, params);

  int ind = obsTimes.size() - 1;

  // Integrate the ode and forward sensitivity equations
  Eigen::VectorXd sens = Eigen::VectorXd::Zero(u->outputSize);
  sens(ind) = 1;
  Eigen::VectorXd grad0 = u->Gradient(u0, springConstant, sens, 0);
  Eigen::VectorXd grad1 = u->Gradient(u0, springConstant, sens, 1);

  double tend = obsTimes(ind);
  EXPECT_NEAR(cos(sqrt(springConstant(0)) * tend),                                                 grad0(0), 1e-2);

  EXPECT_NEAR(-0.5 * u0(0) * sin(sqrt(springConstant(0)) * tend) * tend / sqrt(springConstant(0)), grad1(0), 1e-2);
}
