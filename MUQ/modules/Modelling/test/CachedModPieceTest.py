import unittest
from numpy import * # math stuff in python

import libmuqModelling

class InfModel(libmuqModelling.OneInputNoDerivModPiece):
    def __init__(self):
        libmuqModelling.OneInputNoDerivModPiece.__init__(self, 3, 1)

    def EvaluateImpl(self, x):
        if x[0]<15.0:
            return [x[0]]
        else: 
            return [float("inf")]

class CachedModPieceTest(unittest.TestCase):
    def testCachedNeighborFunctions(self):
        rawModel = InfModel()
        fn       = libmuqModelling.CachedModPiece(rawModel)

        points = [[1.0, 2.0, 3.0],
                  [2.0, 6.0, 10.0],
                  [3.0, 7.0, 11.0],
                  [4.0, 8.0, 12.0]]

        for i in range(4):
            fn.Evaluate([points[i]]) # add to cache 

        testPoint  = [2.0]*3
        twoNearest = [[1.0, 2.0, 3.0],
                      [2.0, 6.0, 10.0]]

        twoNearestComputed = fn.FindKNearestNeighbors([testPoint], 2)

        for i in range(2):
            self.assertListEqual(twoNearest[i], twoNearestComputed[i])

        newPoints = [[2.0, 2.0, 3.0],
                     [3.0, 3.0, 9.0]]

        for i in range(2):
            fn.Evaluate([newPoints[i]]) # add to cache

        radiusNearest = [[2.0, 2.0, 3.0],
                         [1.0, 2.0, 3.0]]

        radiusNearestComputed = fn.FindNeighborsWithinRadius([testPoint], 3.0)

        for i in range(2):
            self.assertListEqual(radiusNearest[i], radiusNearestComputed[i])

        failing = [[18.0, 3.0, 5.0],
                   [9.0, 2.0, 7.0]]

        for i in range(2):
            self.assertTrue(fn.Evaluate([failing[i]])[0]==float("inf") or i>0)

        allPoints = [[1.0, 2.0, 3.0],
                     [2.0, 2.0, 3.0],
                     [2.0, 6.0, 10.0],                     
                     [3.0, 3.0, 9.0],
                     [3.0, 7.0, 11.0],
                     [4.0, 8.0, 12.0],
                     [9.0, 2.0, 7.0]]

        allPointsComputed = fn.GetCachePoints()

        for i in range(7):
            self.assertListEqual(allPoints[i], allPointsComputed[i])
