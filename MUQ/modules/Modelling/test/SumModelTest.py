import unittest

import libmuqModelling

class SumModelTest(unittest.TestCase):
    def testBasicTest(self):
        mod = libmuqModelling.SumModel(3, 5)

        inputs = [None]*3
        inputs[0] = [0.0]*5
        inputs[1] = [1.0]*5
        inputs[2] = [2.0]*5

        result = mod.Evaluate(inputs)

        for i in range(5):
            self.assertEqual(result[i], 3.0)

        for i in range(3):
            J = mod.Jacobian(inputs, i)

            for m in range(5):
                for n in range(5):
                    if( m==n ):
                        self.assertEqual(J[m][n], 1.0)
                    else:
                        self.assertEqual(J[m][n], 0.0)

            target = [i+1.0]*5

            Jv = mod.JacobianAction(inputs, target, i)

            for m in range(5):
                self.assertEqual(Jv[m], target[m])

            sens = [i-1.0]*5
            
            grad = mod.Gradient(inputs, sens, i)

            for m in range(5):
                self.assertEqual(grad[m], sens[m])

            H = mod.Hessian(inputs, sens, i)

            for m in range(5):
                for n in range(5):
                    self. assertEqual(H[m][n], 0.0)

