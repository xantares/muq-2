#include "MUQ/Modelling/python/RosenbrockRVPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;

py::list RosenbrockRV::PySample()
{
  return GetPythonVector<Eigen::VectorXd>(Sample());
}

py::list RosenbrockRV::PySampleInputs(py::list const& inputs)
{
  vector<Eigen::VectorXd> ins = PythonListToVector(inputs);

  return GetPythonVector<Eigen::VectorXd>(Sample(ins));
}

py::list RosenbrockRV::PySamples(int numSamps)
{
  return GetPythonMatrix<double>(Sample(numSamps).transpose());
}

py::list RosenbrockRV::PySamplesInputs(py::list const& inputs, int numSamps)
{
  vector<Eigen::VectorXd> ins = PythonListToVector(inputs);

  return GetPythonMatrix<double>(Sample(ins, numSamps).transpose());
}

void muq::Modelling::ExportRosenbrockRV()
{
  py::class_<RosenbrockRV, shared_ptr<RosenbrockRV>, py::bases<RandVar>, boost::noncopyable> exportRosenRV("RosenbrockRV", py::init<double const, double const>());

  exportRosenRV.def("Sample", &RosenbrockRV::PySample);
  exportRosenRV.def("Sample", &RosenbrockRV::PySampleInputs);
  exportRosenRV.def("Sample", &RosenbrockRV::PySamples);
  exportRosenRV.def("Sample", &RosenbrockRV::PySamplesInputs);

  // convert to ModPiece and RandVar
  py::implicitly_convertible<shared_ptr<RosenbrockRV>, shared_ptr<ModPiece> >();
  py::implicitly_convertible<shared_ptr<RosenbrockRV>, shared_ptr<RandVar> >();

}
