
#include "MUQ/Modelling/python/ScaledMvNormDensPython.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;

ScaledMvNormDensPython::ScaledMvNormDensPython(std::shared_ptr<ConstantMVNormSpecification> const& specification) : ScaledMvNormDens(
                                                                                                          specification)
{}

ScaledMvNormDensPython::ScaledMvNormDensPython(int dimIn) : ScaledMvNormDens(dimIn) {}

ScaledMvNormDensPython::ScaledMvNormDensPython(boost::python::list const& meanIn,
                                   double                     scalarCovariance) : ScaledMvNormDens(GetEigenVector<Eigen::
                                                                                                            VectorXd>(
                                                                                               meanIn),
                                                                                             scalarCovariance)
{}


ScaledMvNormDensPython::ScaledMvNormDensPython(Eigen::VectorXd const                        & meanIn,
                                   Eigen::VectorXd const                        & covDiag,
                                   ConstantMVNormSpecification::SpecificationMode mode) : ScaledMvNormDens(meanIn, covDiag,
                                                                                                     mode)
{}

ScaledMvNormDensPython::ScaledMvNormDensPython(Eigen::VectorXd const                        & meanIn,
                                   Eigen::MatrixXd const                        & cov,
                                   ConstantMVNormSpecification::SpecificationMode mode) : ScaledMvNormDens(meanIn, cov,
                                                                                                     mode)
{}

shared_ptr<ScaledMvNormDensPython> ScaledMvNormDensPython::Create(boost::python::list const& meanIn,
                                                      boost::python::list const& covIn,
                                                      bool                       isDiag)
{
  if (isDiag) {
    return make_shared<ScaledMvNormDensPython>(GetEigenVector<Eigen::VectorXd>(meanIn),
                                         GetEigenVector<Eigen::VectorXd>(covIn));
  } else {
    return make_shared<ScaledMvNormDensPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenMatrix(covIn));
  }
}

shared_ptr<ScaledMvNormDensPython> ScaledMvNormDensPython::CreateMode(boost::python::list const                    & meanIn,
                                                          boost::python::list const                    & covIn,
                                                          bool                                           isDiag,
                                                          ConstantMVNormSpecification::SpecificationMode mode)
{
  if (isDiag) {
    return make_shared<ScaledMvNormDensPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenVector<Eigen::VectorXd>(
                                           covIn), mode);
  } else {
    return make_shared<ScaledMvNormDensPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenMatrix(covIn), mode);
  }
}

void muq::Modelling::ExportScaledMvNormDens()
{
  // expose constructors
  boost::python::class_<ScaledMvNormDensPython, std::shared_ptr<ScaledMvNormDensPython>,
                        boost::python::bases<Density, ModPiece>, boost::noncopyable> exportMvNormDens("ScaledMvNormDens",
                                                                                                      boost::python::init<std::shared_ptr<ConstantMVNormSpecification> const&>());

  exportMvNormDens.def(boost::python::init<int>());
  exportMvNormDens.def(boost::python::init<boost::python::list const&, double>());
  exportMvNormDens.def("__init__", boost::python::make_constructor(&ScaledMvNormDensPython::Create));
  exportMvNormDens.def("__init__", boost::python::make_constructor(&ScaledMvNormDensPython::CreateMode));

  // convert to parent, ModPiece, and Density
  boost::python::implicitly_convertible<std::shared_ptr<ScaledMvNormDensPython>, std::shared_ptr<ScaledMvNormDens> >();
  boost::python::implicitly_convertible<std::shared_ptr<ScaledMvNormDensPython>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<ScaledMvNormDensPython>, std::shared_ptr<Density> >();
}
