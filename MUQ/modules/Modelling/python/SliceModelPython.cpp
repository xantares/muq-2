#include "MUQ/Modelling/python/SliceModelPython.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;

SliceModelPython::SliceModelPython(int inputDim, int startIndIn, int endIndIn, int skipIn) :
  SliceModel(inputDim, startIndIn, endIndIn, skipIn) {}

SliceModelPython::SliceModelPython(int inputDim, boost::python::list const& indicesIn) :
  SliceModel(inputDim, GetIndicesIn(indicesIn)) {}

vector<unsigned int> SliceModelPython::GetIndicesIn(boost::python::list const& indicesIn)
{
  const Eigen::VectorXi indIn = GetEigenVector<Eigen::VectorXi>(indicesIn);

  Translater<Eigen::VectorXi, vector<unsigned int> > trans(indIn);

  return *trans.GetPtr();
}

void muq::Modelling::ExportSliceModel()
{
  boost::python::class_<SliceModelPython, shared_ptr<SliceModelPython>,
                        boost::python::bases<OneInputFullModPiece, ModPiece>, boost::noncopyable> exportSlice(
    "SliceModel",
    boost::python::init<int, int, int, int>());

  exportSlice.def(boost::python::init<int, int, int>());
  exportSlice.def(boost::python::init<int, boost::python::list const&>());

  boost::python::implicitly_convertible<shared_ptr<SliceModelPython>, shared_ptr<SliceModel> >();
  boost::python::implicitly_convertible<shared_ptr<SliceModelPython>,
                                        shared_ptr<OneInputFullModPiece> >();
  boost::python::implicitly_convertible<shared_ptr<SliceModelPython>, shared_ptr<ModPiece> >();
}

