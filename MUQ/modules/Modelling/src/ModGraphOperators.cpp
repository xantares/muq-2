#include "MUQ/Modelling/ModGraphOperators.h"

#include "MUQ/Modelling/SumModel.h"
#include <MUQ/Modelling/DifferenceModel.h>
#include <MUQ/Modelling/ProductModel.h>
#include <MUQ/Modelling/LinearModel.h>

using namespace std;
using namespace muq::Modelling;
using namespace Eigen;

std::shared_ptr<ModGraph> muq::Modelling::GraphCompose_piece_impl(std::shared_ptr<ModPiece> const& modToBind,
                                                                  std::vector < std::shared_ptr < ModGraph >>
                                                                  collectingVector)
{
  assert(
    modToBind->inputSizes.rows() == collectingVector.size() &&
    "Must bind all the arguments of a ModPiece or graph at once");

  std::shared_ptr<ModGraph> merged = std::make_shared<ModGraph>(modToBind);
  int i                            = 0;
  for (auto input : collectingVector) {
    merged = ModGraph::FormUnion(merged, input);

    merged->AddEdge(input->GetOutputNodeName(), modToBind->GetName(), i);
    ++i;
  }

  return merged;
}

std::shared_ptr<ModGraph> muq::Modelling::GraphCompose_graph_impl(std::shared_ptr<ModGraph> const           & modToBind,
                                                                  std::vector < std::shared_ptr < ModGraph >> collectingVector,
                                                                  std::vector<std::string>                    inputOrder,
                                                                  std::vector<int>                            inputDims)
{
  assert(
    modToBind->inputSizes().rows() == collectingVector.size() &&
    "Must bind all the arguments of a ModPiece or graph at once");

  if (inputOrder.size() == 0) {
    //default behavior is the arbitrary order from the ModGraph
    inputOrder = modToBind->GetOutputNames();
  }

  if (inputDims.size() == 0) {
    //default behavior is to assume we're attaching to the first inputs
    inputDims = std::vector<int>(collectingVector.size(), 0);
  }
  assert(
    inputOrder.size() == modToBind->inputSizes().rows() &&
    "Mismatch between the node names provided and the number of inputs required");
  assert(
    inputDims.size() == modToBind->inputSizes().rows() &&
    "Mismatch between the node dims provided and the number of inputs required");

  std::shared_ptr<ModGraph> merged = modToBind->DeepCopy();
  int i                            = 0;
  for (auto input : collectingVector) {
    merged = ModGraph::FormUnion(merged, input);
    merged->AddEdge(input->GetOutputNodeName(), inputOrder.at(i), inputDims.at(i));
    ++i;
  }

  return merged;
}

std::shared_ptr<ModGraph> muq::Modelling::operator+(const std::shared_ptr<ModGraph>& a,
                                                    const std::shared_ptr<ModGraph>& b)
{
  //merge the graphs
  auto mergedGraph = ModGraph::FormUnion(a, b);

  auto outputA = a->GetNodeModel(a->GetOutputNodeName());
  auto outputB = b->GetNodeModel(b->GetOutputNodeName());

  assert(outputA->outputSize == outputB->outputSize);

  //create the sum node
  auto sum = make_shared<SumModel>(2, outputA->outputSize);

  //add it to the graph
  mergedGraph->AddNode(sum, sum->GetName());

  //connect the nodes
  mergedGraph->AddEdge(a->GetOutputNodeName(), sum->GetName(), 0);
  mergedGraph->AddEdge(b->GetOutputNodeName(), sum->GetName(), 1);

  return mergedGraph;
}

std::shared_ptr<ModGraph> muq::Modelling::operator+(const std::shared_ptr<ModPiece>& a,
                                                    const std::shared_ptr<ModGraph>& b)
{
  return make_shared<ModGraph>(a) + b;
}

std::shared_ptr<ModGraph> muq::Modelling::operator+(const std::shared_ptr<ModGraph>& a,
                                                    const std::shared_ptr<ModPiece>& b)
{
  return b + a; //call the other one
}

std::shared_ptr<ModGraph> muq::Modelling::operator+(const std::shared_ptr<ModPiece>& a,
                                                    const std::shared_ptr<ModPiece>& b)
{
  return make_shared<ModGraph>(a) + make_shared<ModGraph>(b);
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(const std::shared_ptr<ModGraph>& a,
                                                    const std::shared_ptr<ModGraph>& b)
{
  //merge the graphs
  auto mergedGraph = ModGraph::FormUnion(a, b);

  auto outputA = a->GetNodeModel(a->GetOutputNodeName());
  auto outputB = b->GetNodeModel(b->GetOutputNodeName());

  assert(outputA->outputSize == outputB->outputSize);

  //create the sum node
  auto difference = make_shared<DifferenceModel>(outputA->outputSize);

  //add it to the graph
  mergedGraph->AddNode(difference, difference->GetName());

  //connect the nodes
  mergedGraph->AddEdge(a->GetOutputNodeName(), difference->GetName(), 0);
  mergedGraph->AddEdge(b->GetOutputNodeName(), difference->GetName(), 1);

  return mergedGraph;
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(const std::shared_ptr<ModPiece>& a,
                                                    const std::shared_ptr<ModGraph>& b)
{
  return make_shared<ModGraph>(a) - b;
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(const std::shared_ptr<ModGraph>& a,
                                                    const std::shared_ptr<ModPiece>& b)
{
  return a - make_shared<ModGraph>(b);
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(const std::shared_ptr<ModPiece>& a,
                                                    const std::shared_ptr<ModPiece>& b)
{
  return make_shared<ModGraph>(a) - make_shared<ModGraph>(b);
}

std::shared_ptr<ModGraph> muq::Modelling::operator*(const std::shared_ptr<ModGraph>& a,
                                                    const std::shared_ptr<ModGraph>& b)
{
  //merge the graphs
  auto mergedGraph = ModGraph::FormUnion(a, b);

  auto outputA = a->GetNodeModel(a->GetOutputNodeName());
  auto outputB = b->GetNodeModel(b->GetOutputNodeName());

  assert(outputA->outputSize == outputB->outputSize);

  //create the sum node
  auto product = make_shared<ProductModel>(2, outputA->outputSize);

  //add it to the graph
  mergedGraph->AddNode(product, product->GetName());

  //connect the nodes
  mergedGraph->AddEdge(a->GetOutputNodeName(), product->GetName(), 0);
  mergedGraph->AddEdge(b->GetOutputNodeName(), product->GetName(), 1);

  return mergedGraph;
}

std::shared_ptr<ModGraph> muq::Modelling::operator*(const std::shared_ptr<ModPiece>& a,
                                                    const std::shared_ptr<ModGraph>& b)
{
  return make_shared<ModGraph>(a) * b;
}

std::shared_ptr<ModGraph> muq::Modelling::operator*(const std::shared_ptr<ModGraph>& a,
                                                    const std::shared_ptr<ModPiece>& b)
{
  return a * make_shared<ModGraph>(b);
}

std::shared_ptr<ModGraph> muq::Modelling::operator*(const std::shared_ptr<ModPiece>& a,
                                                    const std::shared_ptr<ModPiece>& b)
{
  return make_shared<ModGraph>(a) * make_shared<ModGraph>(b);
}

std::shared_ptr<ModGraph> muq::Modelling::operator*(const Eigen::MatrixXd& A, std::shared_ptr<ModGraph> x)
{
  //copy the graph
  auto newGraph = x->DeepCopy();
  //Add the linear model
  auto newLinearNode = make_shared<LinearModel>(A);

  newGraph->AddNode(newLinearNode, newLinearNode->GetName());
  //connect the nodes
  newGraph->AddEdge(x->GetOutputNodeName(), newLinearNode->GetName(), 0);

  return newGraph;
}

std::shared_ptr<ModGraph> muq::Modelling::operator+(const Eigen::VectorXd& b, std::shared_ptr<ModGraph> x)
{
  //copy the graph
  auto newGraph = x->DeepCopy();

  auto xOutCast = std::dynamic_pointer_cast<LinearModel>(newGraph->GetNodeModel(newGraph->GetOutputNodeName()));

  //if the output node is already a LinearModel, we can just alter the existing node
  if (xOutCast) {
    // the output node is linear, swap it out with a new linear piece that also has this additive vector
    if (xOutCast->isId) {
      newGraph->SwapNode(newGraph->GetOutputNodeName(), std::make_shared<LinearModel>(b + xOutCast->b, true));
    } else {
      if (xOutCast->isAffine) {
        newGraph->SwapNode(newGraph->GetOutputNodeName(), std::make_shared<LinearModel>(b, xOutCast->A));
      } else {
        newGraph->SwapNode(newGraph->GetOutputNodeName(), std::make_shared<LinearModel>(b + xOutCast->b, xOutCast->A));
      }
    }
  } else {
    //Add a new linear model
    auto newLinearNode = make_shared<LinearModel>(b, true);
    newGraph->AddNode(newLinearNode, newLinearNode->GetName());
    //connect the nodes
    newGraph->AddEdge(x->GetOutputNodeName(), newLinearNode->GetName(), 0);
  }


  return newGraph;
}

std::shared_ptr<ModGraph> muq::Modelling::operator+(std::shared_ptr<ModGraph> x, const Eigen::VectorXd& b)
{
  return b + x;
}

std::shared_ptr<ModGraph> muq::Modelling::operator*(const Eigen::MatrixXd& A, std::shared_ptr<ModPiece> x)
{
  return A * make_shared<ModGraph>(x);
}

std::shared_ptr<ModGraph> muq::Modelling::operator+(const Eigen::VectorXd& b, std::shared_ptr<ModPiece> x)
{
  return b + make_shared<ModGraph>(x);
}

std::shared_ptr<ModGraph> muq::Modelling::operator+(std::shared_ptr<ModPiece> x, const Eigen::VectorXd& b)
{
  return make_shared<ModGraph>(x) + b;
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(std::shared_ptr<ModGraph> x, const Eigen::VectorXd& b)
{
  return x + (-b);
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(std::shared_ptr<ModPiece> x, const Eigen::VectorXd& b)
{
  return make_shared<ModGraph>(x) - b;
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(const Eigen::VectorXd& b, std::shared_ptr<ModGraph> x)
{
  return b + (-x);
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(const Eigen::VectorXd& b, std::shared_ptr<ModPiece> x)
{
  return b - make_shared<ModGraph>(x);
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(const std::shared_ptr<ModGraph>& a)
{
  auto outSize = a->outputSize(); //get output size and assert that there's only one output

  return (-MatrixXd::Identity(outSize, outSize)) * a;
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(const std::shared_ptr<ModPiece>& a)
{
  return -make_shared<ModGraph>(a);
}

std::shared_ptr<ModGraph> muq::Modelling::operator+(const std::shared_ptr<ModGraph>& a, const double& b)
{
  auto outSize = a->outputSize(); //get output size and assert that there's only one output

  return VectorXd::Constant(outSize, b) + a;
}

std::shared_ptr<ModGraph> muq::Modelling::operator+(const std::shared_ptr<ModPiece>& a, const double& b)
{
  return make_shared<ModGraph>(a) + b;
}

std::shared_ptr<ModGraph> muq::Modelling::operator+(const double& a, const std::shared_ptr<ModGraph>& b)
{
  return b + a;
}

std::shared_ptr<ModGraph> muq::Modelling::operator+(const double& a, const std::shared_ptr<ModPiece>& b)
{
  return make_shared<ModGraph>(b) + a;
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(const std::shared_ptr<ModGraph>& a, const double& b)
{
  return a + (-b);
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(const std::shared_ptr<ModPiece>& a, const double& b)
{
  return make_shared<ModGraph>(a) - b;
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(const double& a, const std::shared_ptr<ModGraph>& b)
{
  auto outSize = b->outputSize(); //get output size and assert that there's only one output

  return VectorXd::Constant(outSize, a) - b;
}

std::shared_ptr<ModGraph> muq::Modelling::operator-(const double& a, const std::shared_ptr<ModPiece>& b)
{
  return a - make_shared<ModGraph>(b);
}

std::shared_ptr<ModGraph> muq::Modelling::operator*(const std::shared_ptr<ModGraph>& a, const double& b)
{
  auto outSize = a->outputSize(); //get output size and assert that there's only one output

  return (b * MatrixXd::Identity(outSize, outSize)) * a;
}

std::shared_ptr<ModGraph> muq::Modelling::operator*(const std::shared_ptr<ModPiece>& a, const double& b)
{
  return make_shared<ModGraph>(a) * b;
}

std::shared_ptr<ModGraph> muq::Modelling::operator*(const double& a, const std::shared_ptr<ModGraph>& b)
{
  return b * a;
}

std::shared_ptr<ModGraph> muq::Modelling::operator*(const double& a, const std::shared_ptr<ModPiece>& b)
{
  return make_shared<ModGraph>(b) * a;
}

std::shared_ptr<ModGraph> muq::Modelling::operator/(const std::shared_ptr<ModGraph>& a, const double& b)
{
  return a * (1.0 / b);
}

std::shared_ptr<ModGraph> muq::Modelling::operator/(const std::shared_ptr<ModPiece>& a, const double& b)
{
  return make_shared<ModGraph>(a) / b;
}