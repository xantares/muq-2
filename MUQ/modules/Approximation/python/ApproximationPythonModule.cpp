// #include "MUQ/Approximation/python/NearestNeighborApproximationPython.h"
// #include "MUQ/Approximation/python/LocalGPApproximationPython.h"
// #include "MUQ/Approximation/python/RegressorBasePython.h"
#include "MUQ/Approximation/python/RegressorPython.h"
#include "MUQ/Approximation/python/PolynomialRegressorPython.h"
#include "MUQ/Approximation/python/PolynomialApproximatorPython.h"
#include "MUQ/Approximation/python/PolynomialChaosExpansionPython.h"
#include "MUQ/Approximation/python/SmolyakPython.h"

using namespace muq::Approximation;

BOOST_PYTHON_MODULE(libmuqApproximation)
{

  ExportRegressor();
  ExportPolynomialRegressor();
  ExportPolynomialChaosExpansion();
  ExportSmolyakPCEFactory();
  
#ifdef MUQ_USE_NLOPT
  ExportPolynomialApproximator();
	ExportGPApproximator();

#endif // ifdef MUQ_USE_NLOPT
}
