#include "MUQ/Approximation/python/RegressorPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

void Regressor::PyFit(py::list const& xs, py::list const& ys) 
{
  return Fit(GetEigenMatrix(xs).transpose(), GetEigenMatrix(ys).transpose());
}

void Regressor::PyCenteredFit(py::list const& xs, py::list const& ys, py::list const& center) 
{
  return Fit(GetEigenMatrix(xs).transpose(), GetEigenMatrix(ys).transpose(), GetEigenVector<Eigen::VectorXd>(center));
}

void muq::Approximation::ExportRegressor()
{
  py::class_<Regressor, shared_ptr<Regressor>, 
	     py::bases<OneInputJacobianModPiece>, boost::noncopyable> exportReg("Regressor", py::no_init);

  // fit functions
  exportReg.def("Fit", &Regressor::PyFit);
  exportReg.def("Fit", &Regressor::PyCenteredFit);

  // convert to parents 
  py::implicitly_convertible<shared_ptr<Regressor>, shared_ptr<ModPiece> >();
}
