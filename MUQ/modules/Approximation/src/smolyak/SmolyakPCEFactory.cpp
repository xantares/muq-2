#include "MUQ/Approximation/smolyak/SmolyakPCEFactory.h"

#include <string.h>
#include <iostream>
#include <fstream>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/function.hpp>

#include "MUQ/Utilities/LogConfig.h"

#include "MUQ/Modelling/CachedModPiece.h"
#include "MUQ/Utilities/VariableCollection.h"
#include "MUQ/Utilities/Quadrature/FullTensorQuadrature.h"
#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"

#include "MUQ/Approximation/pce/PolynomialChaosExpansion.h"
#include "MUQ/Approximation/utilities/PolychaosXMLHelpers.h"
#include "MUQ/Approximation/pce/SingleQuadraturePCEFactory.h"
#include "MUQ/Approximation/smolyak/SmolyakEstimate.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

SmolyakPCEFactory::SmolyakPCEFactory() {}

SmolyakPCEFactory::SmolyakPCEFactory(boost::property_tree::ptree const& pt,
                                     std::shared_ptr<muq::Modelling::ModPiece> inFn) : SmolyakPCEFactory(ConstructVariableCollection(pt),inFn){};

SmolyakPCEFactory::SmolyakPCEFactory(VariableCollection::Ptr                   inVariables,
                                     std::shared_ptr<muq::Modelling::ModPiece> inFn,
                                     unsigned int                              simplexLimit) : SmolyakEstimate<PolynomialChaosExpansion::Ptr>(
                                                                                                 inVariables->length(),
                                                                                                 GetLimiter(simplexLimit,inVariables)),
                                                                                               variables(inVariables),
                                                                                               fn(inFn)
{
  assert(static_cast<int>(variables->length()) == fn->inputSizes(0)); //check that the vars and fn match
  
  //initialize the unstructured set indicating which terms are present
  polynomialBasisTermsEstimated = make_shared<MultiIndexSet>(inVariables->length());
}

SmolyakPCEFactory::SmolyakPCEFactory(VariableCollection::Ptr                        inVariables,
                                     std::shared_ptr<muq::Modelling::ModPiece>      inFn,
                                     std::shared_ptr<muq::Utilities::MultiIndexSet> limitingSet) : SmolyakEstimate<PolynomialChaosExpansion::Ptr>(
                                                                                                     inVariables->length(),
                                                                                                     GetLimiter(limitingSet,inVariables)),
                                                                                              variables(inVariables),
                                                                                              fn(inFn)
{
  assert(static_cast<int>(variables->length()) == fn->inputSizes(0)); //check that the vars and fn match
  //initialize the unstructured set indicating which terms are present
  polynomialBasisTermsEstimated = make_shared<MultiIndexSet>(inVariables->length());
}

SmolyakPCEFactory::~SmolyakPCEFactory() {}


std::shared_ptr<MultiIndexLimiter> SmolyakPCEFactory::GetLimiter(unsigned int                                        simplexLimit,
                                                                 std::shared_ptr<muq::Utilities::VariableCollection> inVariables)
{
  Eigen::VectorXi maxQuads(inVariables->length());
  for(int i=0; i<inVariables->length(); ++i)
    maxQuads(i) = inVariables->GetVariable(i)->quadFamily->GetMaximumPolyOrder();
  
  shared_ptr<MultiIndexLimiter> quadLimiter = make_shared<MaxOrderLimiter>(maxQuads);
  
  if(simplexLimit>0){
    shared_ptr<MultiIndexLimiter> orderLimiter = make_shared<TotalOrderLimiter>(simplexLimit);
    
    return make_shared<AndLimiter>(orderLimiter,quadLimiter);
    
  }else{
    return quadLimiter;
  }
  
}

std::shared_ptr<MultiIndexLimiter> SmolyakPCEFactory::GetLimiter(std::shared_ptr<muq::Utilities::MultiIndexSet>      limitingSet,
                                                                 std::shared_ptr<muq::Utilities::VariableCollection> inVariables)
{
  Eigen::VectorXi maxQuads(inVariables->length());
  for(int i=0; i<inVariables->length(); ++i)
    maxQuads(i) = inVariables->GetVariable(i)->quadFamily->GetMaximumPolyOrder();
  
  shared_ptr<MultiIndexLimiter> quadLimiter = make_shared<MaxOrderLimiter>(maxQuads);
  shared_ptr<MultiIndexLimiter> termLimiter = make_shared<GeneralLimiter>(limitingSet);
  
  return make_shared<AndLimiter>(termLimiter,quadLimiter);
  
}



PolynomialChaosExpansion::Ptr SmolyakPCEFactory::ComputeOneEstimate(RowVectorXu const& multiIndex)
{
  SingleQuadraturePCEFactory pceFactory(variables);

  //bump up the order by one because 0th order quadrature is meaningless
  PolynomialChaosExpansion::Ptr result = pceFactory.ConstructPCE(fn, multiIndex, RowVectorXu::Ones(multiIndex.cols()));

  //stash away the terms that this one estimated
  for (unsigned int i = 0; i < result->multis->GetNumberOfIndices(); ++i) {
    auto polyTermIncluded = result->multis->IndexToMultiPtr(i);
    unsigned int polyTermIncludedIndex = polynomialBasisTermsEstimated->AddActive(polyTermIncluded);

    //indicate that this multiIndex of Smolyak contributes to the polyTermIncluded
    basisMultiIndexCache[polyTermIncludedIndex].insert(termsIncluded->MultiToIndex(multiIndex));
  }

  //return the estimate
  return result;
}

double SmolyakPCEFactory::CostOfOneTerm(Eigen::RowVectorXu const& multiIndex)
{
  FullTensorQuadrature quad(variables, multiIndex + RowVectorXu::Ones(multiIndex.cols()));
  MatrixXd nodes;
  VectorXd weights;

  quad.GetNodesAndWeights(nodes, weights);

  auto cachedPtr = dynamic_pointer_cast<CachedModPiece>(fn);
  if (!cachedPtr) {
    return nodes.cols();
  } else {
    return cachedPtr->CostOfEvaluations(nodes);
  }
}

PolynomialChaosExpansion::Ptr SmolyakPCEFactory::WeightedSumOfEstimates(Eigen::VectorXd const& weights) const
{
  //just call the class method, passing on the caches you have available, being careful to copy the
  //family of the indices in the polynomial
  return PolynomialChaosExpansion::ComputeWeightedSum(termEstimates,
                                                      weights,
                                                      fn->outputSize,
                                                      MultiIndexSet::CloneExisting(polynomialBasisTermsEstimated),
                                                      basisMultiIndexCache);
}

double SmolyakPCEFactory::ComputeMagnitude(PolynomialChaosExpansion::Ptr estimate)
{
  return estimate->ComputeMagnitude().norm();
}

MatrixXu SmolyakPCEFactory::GetEffectiveIncludedTerms()
{
  MatrixXu result = MatrixXu::Zero(termsIncluded->GetNumberOfIndices(), termsIncluded->GetMultiIndexLength());

  for (unsigned int i = 0; i < termsIncluded->GetNumberOfIndices(); ++i) {
    RowVectorXu multiIndex = termsIncluded->IndexToMulti(i);
    for (unsigned int j = 0; j < termsIncluded->GetMultiIndexLength(); ++j) {
      result(i, j) = variables->GetVariable(j)->quadFamily->GetPrecisePolyOrder(multiIndex(j) + 1);
    }
  }

  return result;
}

// std::shared_ptr<muq::Utilities::FunctionWrapper> SmolyakPCEFactory::GetFunction()
// {
//   return fn;
// }


PolynomialChaosExpansion::Ptr SmolyakPCEFactory::ComputeFixedPolynomials(MatrixXu polys)
{
  shared_ptr<MultiIndexSet> quadSet = make_shared<MultiIndexSet>(polys.cols());

  for (unsigned int i = 0; i < polys.rows(); ++i) {
    RowVectorXu newQuad = RowVectorXu::Zero(polys.cols());
    for (unsigned int j = 0; j < polys.cols(); ++j) {
      unsigned int necessaryOrder = 0;
      while (polys(i, j) * 2 > variables->GetVariable(j)->quadFamily->GetPrecisePolyOrder(necessaryOrder + 1)) {
        ++necessaryOrder;
      }
      newQuad(j) = necessaryOrder;
    }

    quadSet->ForciblyActivate(newQuad);
  }

  //compute the estimate with these terms
  return StartFixedTerms(quadSet);
}

// SmolyakPCEFactory::Ptr SmolyakPCEFactory::LoadProgress(string baseName, boost::function<Eigen::VectorXd
// (Eigen::VectorXd const&)> fn)
// {
//
//   SmolyakPCEFactory::Ptr progress(new SmolyakPCEFactory());
//   // create and open an archive for input
//   std::string fullName = baseName;
//   std::ifstream ifs(fullName.c_str());
//   assert(ifs.good());
//   boost::archive::text_iarchive ia(ifs);
//
//   // read class state from archive
//   ia >> progress;
//
//   progress->fn->SetVectorFunction(fn);
//
//   return progress;
// }

unsigned int SmolyakPCEFactory::ComputeWorkDone() const
{
  auto cachedFn = dynamic_pointer_cast<CachedModPiece>(fn);

  if (!cachedFn) {
    LOG(INFO) << "Work done metric not valid!";
    return -1;
  } else {
    return cachedFn->GetNumOfEvals();
  }
}

// void SmolyakPCEFactory::SaveProgress(SmolyakPCEFactory::Ptr pceFactory, string baseName)
// {
//
//   std::ofstream ofs(baseName.c_str());
//
//
//   if(!ofs.good())
//   {
//     LOG(ERROR) << "SmolyakPCEFactory::SaveProgress file name not valid. Falling back to \"pceProgress.dat\"";
//     std::ofstream ofs("pceProgress.dat");
//   }
//   assert(ofs.good());
//
//
//   // save data to archive
//   {
//     boost::archive::text_oarchive oa(ofs);
//     // write class instance to archive
//     oa << pceFactory;
//     // archive and stream closed when destructors are called
//   }
// }

// template<class Archive>
// void SmolyakPCEFactory::serialize(Archive & ar, const unsigned int version)
// {
//   ar & boost::serialization::base_object<SmolyakEstimate<PolynomialChaosExpansion::Ptr> >(*this);
//   ar & variables;
//   ar & polynomialBasisTermsEstimated;
//   ar & basisMultiIndexCache;
//   ar & fn;
// }
//
// BOOST_CLASS_EXPORT(SmolyakPCEFactory)
