#include <boost/property_tree/ptree.hpp> // need to avoid weird toupper bug on osx

#include "MUQ/Pde/python/MeshParameterPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Pde;

MeshParameterPython::MeshParameterPython(string const& systemName, 
					 shared_ptr<GenericEquationSystems> const& system, 
					 boost::property_tree::ptree const& pt) :
  MeshParameter(systemName, system, pt) {}

shared_ptr<MeshParameterPython> MeshParameterPython::PyCreate(shared_ptr<GenericEquationSystems> const& system, py::dict const& para) 
{
  boost::property_tree::ptree pt = PythonDictToPtree(para);

  // get the system name and add the system to the GenericEquationSystems
  const string systemName = MakeLibMeshSystem(system, pt);
  
  return make_shared<MeshParameterPython>(systemName, system, pt);
}

 libMesh::Number MeshParameterPython::ParameterEvaluate(double const x, double const y, double const z, string const& var) const 
 {
   // if there are inputs
   if (inputSizes.size() > 0) {
     // list of inputs
     py::list inputs;

     // for each input
     for (unsigned int i = 0; i < inputNames.size(); ++i) {
       // doubles are stored as doubles
       if (inputSizes[i] == 1) {
	 inputs.append(system->GetEquationSystemsPtr()->parameters.get<double>(inputNames[i]));
       } else {
	 /// vectors are stored as boost::python::lists
	 inputs.append(GetPythonVector<Eigen::VectorXd>(system->GetEquationSystemsPtr()->parameters.get<Eigen::VectorXd>(inputNames[i])));
       }
     }

     // return with inputs
     return get_override("ParameterEvaluate")(x, y, z, var, inputs);
   }

   // return without inputs
   return get_override("ParameterEvaluate")(x, y, z, var);
 }

py::list MeshParameterPython::PyEvaluate() 
{
  return GetPythonVector<Eigen::VectorXd>(Evaluate());
}

py::list MeshParameterPython::PyEvaluateInputs(py::list const& inputs) 
{
  return GetPythonVector<Eigen::VectorXd>(Evaluate(PythonListToVector(inputs)));
}



static shared_ptr<MeshParameter> CreateMeshParameterCpp(shared_ptr<GenericEquationSystems> const& system, py::dict const& para) 
{
  boost::property_tree::ptree pt = PythonDictToPtree(para);

  return MeshParameter::Create(system, pt);
}



void muq::Pde::ExportMeshParameter() 
{
  // register
  py::class_<MeshParameterPython, shared_ptr<MeshParameterPython>, 
	     py::bases<ModPiece>, boost::noncopyable> exportMP("MeshParameter", py::no_init);

  // constructor
  exportMP.def("__init__", MakeStaticConstructor(&MeshParameterPython::PyCreate));
  
  // user implemented functions
  exportMP.def("ParameterEvaluate", py::pure_virtual(&MeshParameterPython::ParameterEvaluate));

  // user-facing functions
  exportMP.def("Evaluate", &MeshParameterPython::PyEvaluate);
  exportMP.def("Evaluate", &MeshParameterPython::PyEvaluateInputs);

  // convert to parents
  py::implicitly_convertible<shared_ptr<MeshParameterPython>, shared_ptr<MeshParameter> >();
  py::implicitly_convertible<shared_ptr<MeshParameterPython>, shared_ptr<ModPiece> >();
  
  
  //now cpp version
  py::class_<MeshParameter, shared_ptr<MeshParameter>, 
	     py::bases<ModPiece>, boost::noncopyable> exportMPcpp("MeshParameterCpp", py::no_init);

  // constructor
  exportMPcpp.def("__init__",boost::python::make_constructor(CreateMeshParameterCpp) );
  
  
  py::implicitly_convertible<shared_ptr<MeshParameter>, shared_ptr<ModPiece> >();
}
