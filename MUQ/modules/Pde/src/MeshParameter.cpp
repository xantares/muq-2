#include "MUQ/Pde/MeshParameter.h"

#include "libmesh/dof_map.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Pde;

MeshParameter::MeshParameter(string const                            & systemName,
			     shared_ptr<GenericEquationSystems> const& system,
			     boost::property_tree::ptree const& para) :
  ModPiece(InputSizes(para), system->GetEquationSystemsPtr()->get_system(systemName).n_dofs(),
	   false, false, false, false, false, systemName), 
  system(system), inputNames(InputNames(para)), computeInputGradients(para.get<bool>("MeshParameter.ComputeInputGradients", false))
{
  assert(inputSizes.size() == inputNames.size());
}

vector<string> MeshParameter::InputNames(boost::property_tree::ptree const& para) 
{
  // get the input names, a string separated by commas 
  string names = para.get("MeshParameter.InputNames", "");

  // if there are no inputs, return an empty vector
  if( names.empty() ) {
    return vector<string>();
  }

  // turn the comma separated list into a vector of strings
  return ListToVector(names);   
}

Eigen::VectorXi MeshParameter::InputSizes(boost::property_tree::ptree const& para) 
{
  // get the input sizes, a string separated by commas 
  string sizes = para.get("MeshParameter.InputSizes", "");

  // if there are no inputs, return an empty vector
  if( sizes.empty() ) {
    return Eigen::VectorXi();
  }

  // remove the spaces and turn to lower case 
  boost::algorithm::erase_all(sizes, " ");
  transform(sizes.begin(), sizes.end(), sizes.begin(), ::tolower);

  // turn the comma separated list into a vector of strings
  vector<string> vecSizes = ListToVector(sizes);   

  // loop through them and turn them to integers 
  Eigen::VectorXi inputSizes(vecSizes.size());
  for( unsigned int i=0; i<vecSizes.size(); ++i ) {
    inputSizes(i) = atoi(vecSizes[i].c_str());
  }

  return inputSizes;
}

shared_ptr<MeshParameter> MeshParameter::Create(shared_ptr<GenericEquationSystems> const& system,
						boost::property_tree::ptree const       & para)
{
  // get the system name and add the system to the GenericEquationSystems
  const string systemName = MakeLibMeshSystem(system, para);
  
  return (GetMeshParaMap()->at(systemName))(systemName, system, para);
}

string MeshParameter::MakeLibMeshSystem(shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& para)
{
  // get the system name, variable names, and variable orders
  const string systemName = para.get("MeshParameter.SystemName", "");
  const string parameters = para.get("MeshParameter.VariableNames", "");
  string       orders     = para.get("MeshParameter.VariableOrders", "");

  // turn the comma separated list into a vector of strings
  const vector<string> paraVec = ListToVector(parameters);   
                                      
  // remove spaces and turn to lower case
  boost::algorithm::erase_all(orders, " ");                                                     
  transform(orders.begin(), orders.end(), orders.begin(), ::tolower);                 

  // turn the comma separated list into a vector of strings
  vector<string> orderStringVec = ListToVector(orders);                                  
  assert(paraVec.size() == orderStringVec.size());  

  // if the system does not exist, create it
  if( !system->GetEquationSystemsPtr()->has_system(systemName) ) {
    // add the system (reference so anything changed here changes in the big system too)
    libMesh::System& sys = system->GetEquationSystemsPtr()->add_system<libMesh::System>(systemName);

    // add the variables 
    for( unsigned int i=0; i<paraVec.size(); ++i ) {
      // the order of the variable 
      const unsigned int order = atoi(orderStringVec[i].c_str());

      if( order == 0 ) { // if it is constant 
	sys.add_variable(paraVec[i], libMesh::CONSTANT, libMesh::MONOMIAL);
      } else { // non constant 
	sys.add_variable(paraVec[i], libMesh::Order(order));
      }
    }
    
    // initialize the system 
    sys.init();
  } else {
    cerr << endl << "WARNING: muq::Pde::GenericEquationSystems already has system named " << systemName << "." << endl;
    cerr << "\tMeshParameter.cpp Create method" << endl << endl;
  }

  return systemName;
}

Eigen::VectorXd MeshParameter::EvaluateImpl(vector<Eigen::VectorXd> const& inputs)
{
  assert(inputs.size() == inputNames.size());
  assert(system);

  // get a reference to the system associated with this parameter
  libMesh::System& mySystem = system->GetEquationSystemsPtr()->get_system(GetName());

  // store a pointer to itself in the libMesh parameter; libMesh requires static function wrappers so it needs to store the class
  system->GetEquationSystemsPtr()->parameters.set<string>("myName") = GetName();
  system->GetEquationSystemsPtr()->parameters.set<shared_ptr<const MeshParameter> >(GetName()) = shared_from_this();

  // store the inputs (either in as a libMesh parameter or projected onto the mesh)
  int numMeshDependentInputs = StoreInputs(inputs);

  // store the number of mesh-dependent parameters
  system->GetEquationSystemsPtr()->parameters.set<int>("numMeshDependentInputs") = numMeshDependentInputs;

  // project the parameter onto the mesh using a static function wrapper
  mySystem.project_solution(MeshParameter::ParameterEvaluateWrapper, nullptr, system->GetEquationSystemsPtr()->parameters);

  // clear the equation system parameter tree
  system->GetEquationSystemsPtr()->parameters.clear();

  // translate the libMesh::NumericVector into the Eigen::VectorXd for returning
  Translater<libMesh::NumericVector<libMesh::Number>, Eigen::VectorXd> trans(*(mySystem.solution));
  
  // return the translated vector
  return *trans.GetPtr();
}

int MeshParameter::StoreInputs(vector<Eigen::VectorXd> const& inputs) 
{
  // start counting the number of mesh dpendent parameters
  int numMeshDependentInputs = 0;

  // start counting number of variables in this input system
  int numVars = 0;

  // loop through each input 
  for (unsigned int i = 0; i < inputNames.size(); ++i) {
    // if it is a scalar 
    if (inputs[i].size() == 1) {
      // store the double in the libmesh parameters
      system->GetEquationSystemsPtr()->parameters.set<double>(inputNames[i]) = inputs[i](0);

      // if there is a system with the same name
    } else if (system->GetEquationSystemsPtr()->has_system(inputNames[i])) { 
      // project the input onto the mesh
      numMeshDependentInputs += ProjectInput(inputNames[i], inputs[i], numVars);

      // just store the input as an Eigen::VectorXd
    } else { 
      system->GetEquationSystemsPtr()->parameters.set<Eigen::VectorXd>(inputNames[i]) = inputs[i];
    }
  }
  
  return numMeshDependentInputs;
}

int MeshParameter::ProjectInput(string const& name, Eigen::VectorXd const& in, int& numVars)
{
  // number of variables already counted
  const int oldNumVars = numVars;

  // get a constant reference to the mesh
  const libMesh::MeshBase& mesh = system->GetEquationSystemsPtr()->get_mesh();
  
  // get a reference to the input system
  libMesh::System& sys = system->GetEquationSystemsPtr()->get_system(name);
  assert(sys.solution->size() == in.size());
  
  // translate the input into a std::vector
  muq::Utilities::Translater<Eigen::VectorXd, vector<double> > inTrans(in);
  
  // project the input onto the mesh
  sys.solution->operator=(*inTrans.GetPtr());

  // update the system with the new solution
  sys.update();
  
  // get the parameter numbers of each input variable
  vector<unsigned int> paraVarNums(sys.n_vars());
  sys.get_all_variable_numbers(paraVarNums);
  
  // loop through each input parameter
  for (unsigned int p = 0; p < paraVarNums.size(); ++p) {
    // a map from DOF points to parameter value or parameter gradient
    auto vals  = make_shared<map<libMesh::Point, double> >();
    auto grads = make_shared<map<libMesh::Point, libMesh::Gradient> >();
    
    for (unsigned int n = 0; n < mesh.n_nodes(); ++n) {
      // DOF node
      libMesh::Node nd = mesh.node(n);
      
      // the value at the DOF
      const double val = sys.point_value(paraVarNums[p], (libMesh::Point)nd, mesh.elem(nd.id()));
      
      // store the value at each point
      vals->insert(pair<libMesh::Point, double>((libMesh::Point)nd, val));
      
      // compute the gradient if asked for
      if( computeInputGradients ) {
	// the gradient at the DOF
	const libMesh::Gradient grad = sys.point_gradient(paraVarNums[p], (libMesh::Point)nd, mesh.elem(nd.id()));
	
	// store the gradient at each point
	grads->insert(pair<libMesh::Point, libMesh::Gradient>((libMesh::Point)nd, grad));
      }
    }
    
    // store the values in the libmesh parameter
    system->GetEquationSystemsPtr()->parameters.set<shared_ptr<map<libMesh::Point, double> > >("pointVals" + sys.variable_name(paraVarNums[p])) = vals;
    
    // store the gradient in the libmesh parameter
    if( computeInputGradients ) {
      system->GetEquationSystemsPtr()->parameters.set<shared_ptr<map<libMesh::Point, libMesh::Gradient> > >("pointGrads" + sys.variable_name(paraVarNums[p])) = grads;
    }
    
    // store the name of the variable
    system->GetEquationSystemsPtr()->parameters.set<string>("in" + to_string(numVars++)) = sys.variable_name(paraVarNums[p]);
  }
  
  // store if the gradients were computed (will be used by static member function)
  system->GetEquationSystemsPtr()->parameters.set<bool>("ComputeInputGradients") = computeInputGradients;
  
  return numVars - oldNumVars;
}

libMesh::Number MeshParameter::ParameterEvaluateWrapper(libMesh::Point const     & point,
							libMesh::Parameters const& para,
							string const             & sysName,
							string const             & var)
{
  // get the pointer to the parameter being evalauted (this function is static)
  string myName = para.get<string>("myName");
  auto   myself = para.get<shared_ptr<const MeshParameter> >(myName);

  // the number of mesh dependent inputs
  const int numMeshDependentInputs = para.get<int>("numMeshDependentInputs");

  // if there is a mesh-depndent input
  if( numMeshDependentInputs > 0 ) {
    // did we compute the spatial gradients of the inputs?
    bool haveInputGrads = para.get<bool>("ComputeInputGradients");

    // loop through each input parameter
    for (unsigned int i = 0; i < numMeshDependentInputs; ++i) {
      // the name of the parameter
      const string inPara = para.get<string>("in" + to_string(i));
      
      // map from point to value
      auto vals = para.get<shared_ptr<map<libMesh::Point, double> > >("pointVals" + inPara);
      
      // store the value for this parameter
      myself->system->GetEquationSystemsPtr()->parameters.set<double>(inPara) = vals->at(point);
      
      // if we computed the gradient ...
      if( haveInputGrads ) {
	// map from point to gradient
	auto grads = para.get<shared_ptr<map<libMesh::Point, libMesh::Gradient> > >("pointGrads" + inPara);

	// get the gradient
	libMesh::Gradient grad = grads->at(point);

	// store the gradient for this parameter
	myself->system->GetEquationSystemsPtr()->parameters.set<double>("d" + inPara + "dx") = grad(0);
	myself->system->GetEquationSystemsPtr()->parameters.set<double>("d" + inPara + "dy") = grad(1);
	myself->system->GetEquationSystemsPtr()->parameters.set<double>("d" + inPara + "dz") = grad(2);
      }
    }
  }
  
  return myself->ParameterEvaluate(point(0), point(1), point(2), var);
}

shared_ptr<MeshParameter::MeshParaMap> MeshParameter::GetMeshParaMap()
{
  static shared_ptr<MeshParameter::MeshParaMap> meshParaMap;

  if (!meshParaMap) {
    meshParaMap = make_shared<MeshParameter::MeshParaMap>();
  }

  return meshParaMap;
}

double MeshParameter::EvaluateAtPoint(libMesh::Point const& pt, string const& var) const
{
  // get the system 
  const libMesh::System& sys = system->GetEquationSystemsPtr()->get_system(GetName());
  assert(sys.has_variable(var));

  // compute the value
  return sys.point_value(sys.variable_number(var), pt);
}

libMesh::RealGradient MeshParameter::GradientAtPoint(libMesh::Point const& pt, string const& var) const
{
  // get the system 
  const libMesh::System& sys = system->GetEquationSystemsPtr()->get_system(GetName());
  assert(sys.has_variable(var));

  // compute the value
  return sys.point_gradient(sys.variable_number(var), pt);
}

