
#include "MUQ/Pde/PointSensor.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Pde;

PointSensor::PointSensor(int const inputSize, vector<Eigen::VectorXd> const& pnts,
                         std::string const& sysName,
                         std::shared_ptr<GenericEquationSystems> const eqnsystem) :
  ModPiece(inputSize * Eigen::VectorXi::Ones(1),  eqnsystem->GetEquationSystemsPtr()->get_system(sysName).n_vars() *
           pnts.size(), false, true, false, false, false, sysName), system(eqnsystem->GetEquationSystemsPtr())
{
    assert(system->has_system(GetName()));
    assert(system->get_system(GetName()).solution->size() == inputSize);

  const libMesh::MeshBase& mesh = system->get_mesh();
  libMesh::AutoPtr<libMesh::PointLocatorBase> ptLoc = mesh.sub_point_locator();

  points.resize(pnts.size());
  elements.resize(pnts.size());
  for (unsigned int i = 0; i < pnts.size(); ++i) {
    assert(pnts[i].size() == system->get_mesh().mesh_dimension());

    libMesh::Point pt;
    for (unsigned int j = 0; j < pnts[i].size(); ++j) {
      pt(j) = pnts[i](j);
    }
    points[i] = pt;

    elements[i] = ptLoc->operator()(points[i]);
  }
}

Eigen::VectorXd PointSensor::EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs)
{
  libMesh::System& sys = system->get_system(GetName());

  assert(inputs.size() == 1);
  Translater<Eigen::VectorXd, vector<double> > trans(inputs[0]);
  sys.solution->operator=(*trans.GetPtr());
  sys.update();

  vector<unsigned int> varNums(sys.n_vars());
  sys.get_all_variable_numbers(varNums);

  Eigen::VectorXd sensed(outputSize);
  for (unsigned int i = 0; i < points.size(); ++i) {
    for (unsigned int v = 0; v < sys.n_vars(); ++v) {
      sensed(i * sys.n_vars() + v) = sys.point_value(varNums[v], points[i], *elements[i]);
    }
  }

  return sensed;
}

Eigen::MatrixXd PointSensor::JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt)
{
  libMesh::System& sys = system->get_system(GetName());

  // get a constant reference to the mesh
  const libMesh::MeshBase& mesh = system->get_mesh();

  Eigen::MatrixXd Jac(outputSize, inputSizes(inputDimWrt));
  for (unsigned int j = 0; j < inputSizes(inputDimWrt); ++j) {
    Eigen::VectorXd sens = Eigen::VectorXd::Zero(inputSizes(0));
    sens(j)              = 1.0;

    assert(inputs.size() == 1);
    Translater<Eigen::VectorXd, vector<double> > trans(sens);
    sys.solution->operator=(*trans.GetPtr());
    sys.update();

    vector<unsigned int> varNums(sys.n_vars());
    sys.get_all_variable_numbers(varNums);

    Eigen::VectorXd sensed(outputSize);
    for (unsigned int i = 0; i < points.size(); ++i) {
      for (unsigned int v = 0; v < sys.n_vars(); ++v) {
        Jac(i * sys.n_vars() + v, j) = sys.point_value(varNums[v], points[i], elements[i]);
      }
    }
  }

  return Jac;
}

