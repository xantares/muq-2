#include "MUQ/Pde/Nontransient.h"

using namespace std;
using namespace muq::Pde;

Nontransient::Nontransient(Eigen::VectorXi const& inSizes, shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& para) :
  PDE(InputSizes(para.get("PDE.OutputSize", 0), inSizes), system, para) 
{
  isTransient = false;
}

Eigen::VectorXi Nontransient::InputSizes(unsigned int const outSize, Eigen::VectorXi const& paraInSizes) const
{
  Eigen::VectorXi inSizes(paraInSizes.size() + 1);
  inSizes(0) = outSize; // initial guess
  inSizes.tail(paraInSizes.size()) = paraInSizes; // other parameters

  return inSizes;
}

double Nontransient::DirichletBCs(string const& var, libMesh::Point const& pt, double const time) const
{
  assert(!isTransient);

  return DirichletBCs(var, pt);
}

double Nontransient::DirichletBCs(string const&, libMesh::Point const& pt) const
{
  return 0.0;
}
