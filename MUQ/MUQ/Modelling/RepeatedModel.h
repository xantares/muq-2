
#ifndef _RepeatedModel_h
#define _RepeatedModel_h

#include "MUQ/Modelling/ModPieceTemplates.h"

namespace muq {
namespace Modelling {
/** @class RepeatedModel
 *   @ingroup Modelling
 *   @brief Apply the same model to several segments of the input vector.
 *   @details This model applies another Modpiece to many segments of the input vector.  For example, let the input to
 *      the repeated model be x = [x1; x2; x3; x4].  Then the model to repeate takes vectors the same size of x1,x2,x3,
 *      or x4 and produces some new vector, perhaps y1=g(x1).  Then, this repeated model is defined by f(x) = [g(x1);
 *      g(x2); g(x3); g(x4)].
 */
class RepeatedModel : public OneInputFullModPiece {
public:

  // construct the repeated model
  RepeatedModel(std::shared_ptr<ModPiece> modelIn, int NumRepeatsIn);


  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input);

  virtual Eigen::VectorXd GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity);

  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input);

  virtual Eigen::VectorXd JacobianActionImpl(Eigen::VectorXd const& input,  Eigen::VectorXd const& target);

  virtual Eigen::MatrixXd HessianImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity);

private:

  std::shared_ptr<ModPiece> model; // the model to repeat
  int NumRepeats;                  // the number of times to repeate the model
};

// class RepeatedModel
} // namespace Modelling
} // namespace muq

#endif // ifndef _RepeatedModel_h
