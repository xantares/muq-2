
#ifndef _ModGraphPiece_h
#define _ModGraphPiece_h

#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/RandVar.h"
#include "MUQ/Modelling/Density.h"


namespace muq {
namespace Inference {
class InferenceProblem;
class MarginalInferenceProblem;
}

namespace Modelling {
class ModParameter;
class ModGraphDensity;
class ModGraphRandVar;

/** @class ModGraphPiece
 *  @ingroup Modelling
 *  @brief Provides a mechanism to create a valid ModPiece from a ModGraph
 *  @details The muq::Modelling::ModGraph class provides an easy mechanism for organizing many model pieces into more
 *sophisticated models.  However, in many situations, i.e. computing gradients, jacobians, etc... a ModPiece provides an
 *easier and more general interface to work with.  This class allows users to create a ModPiece from a graph.
 * Furthermore, wrapping a graph in a ModPiece allows users the flexibility to have a hierarchy of graphs, with graphs
 *within graphs.
 *
 * Note that the graph stored internally is a bit odd. It will not have any inputs left, as they are replaced with
 * ModParameters, which this ModGraphPiece knows how to use (i.e., bind with input values) during evaluation. But, the 
 * graph is likely to be meaningless to any other object, so should not be extracted. Hence, making a ModGraphPiece
 * from a graph is probably the last step, and likely implies that you're done manipulating the graph.
 */
class ModGraphPiece : public ModPiece {
public:

  // friend classes
  friend muq::Inference::InferenceProblem;
  friend muq::Inference::MarginalInferenceProblem;
  friend ModGraphDensity;
  friend ModGraphRandVar;

  // friend functions
  template<typename ModelType>
  friend std::shared_ptr<ModPiece> PiecewiseOperator(std::shared_ptr<ModPiece> x);
  
  virtual ~ModGraphPiece() = default;

  /** Create a shared_ptr to a ModPiece using a particular node of the graph for the ModPiece output. The inputs to the
   *  resulting ModPiece are in alphabetical order.
   *  @param[in] inGraph The original graph defining inter-ModPiece connects that we want to wrap into a single ModPiece
   *  @param[in] outNode The node in inGraph to use as the output of this ModPiece. If the default, "", is used, 
   * assume (and check) that there's only one output that we should use.
   *  @return a ModPiece shared_ptr created from an instance of this class.
   */
  static std::shared_ptr<ModGraphPiece> Create(std::shared_ptr<ModGraph> inGraph, const std::string& outNode = "");

  /** Create a shared_ptr to a ModPiece using a particular node of the graph for the ModPiece output.
   *  @param[in] inGraph The original graph defining inter-ModPiece connects that we want to wrap into a single ModPiece
   *  @param[in] outNode The node in inGraph to use as the output of this ModPiece. If the input is "" is used, 
   * assume (and check) that there's only one output that we should use.
   *  @param[in] inputOrder A vector of node names holding the order the inputs should be placed in.
   *  @return a ModPiece shared_ptr created from an instance of this class.
   */
  static std::shared_ptr<ModGraphPiece> Create(std::shared_ptr<ModGraph> inGraph,
                                               const std::string             & outNode,
                                               const std::vector<std::string>& inputOrder);

  /** Write the internal graph
   */
  template<typename ... Args>
  void writeGraphViz(Args ... args) const
  {
    internalGraph->writeGraphViz(args ...);
  }

  #if MUQ_PYTHON == 1
  void PyWriteGraphViz0(const std::string& filename) const;
  void PyWriteGraphViz1(const std::string& filename, ModGraph::colorOptionsType colorOption) const;
  void PyWriteGraphViz2(const std::string& filename, ModGraph::colorOptionsType colorOption,
                        bool addDerivLabel) const;
#endif // if MUQ_PYTHON == 1

  

private:

  // private constructor
  ModGraphPiece(std::shared_ptr<ModGraph> inGraph,
                const boost::graph_traits<Graph>::vertex_descriptor& outNode,
                const std::vector<std::shared_ptr<ModParameter> >  & inPieces,
                const Eigen::VectorXi                              & inSizes);


  /** Evaluate the ModPiece by computing dependencies in the graph and propagating information*/
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;

  /** Evaluate teh gradient. */
  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override;

  /** Construct the Jacobian. */
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;

  /** Compute the action of the Jacobian on a vector. */
  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt = 0) override;

  /** Compute the Hessian */
  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt = 0) override;

  // map from the vertex descriptor to the output value of the model at that node -- built during Evaluate call but used
  // in subsequent gradient and jacobian calls
  std::map<boost::graph_traits<Graph>::vertex_descriptor, Eigen::VectorXd> valMap;

  // collect the outputs of each ModPiece into vectors for the downstream ModPieces
  std::map<boost::graph_traits<Graph>::vertex_descriptor, std::vector<Eigen::VectorXd> > inputMap;

  void BuildValMap(); // construct the value map

  // like run order, but specific to which input node is used (also in output->input order)
  std::vector<std::deque<boost::graph_traits<Graph>::vertex_descriptor> > sensRunOrders;

  // run order computed during construction of the modpiece (input->output order)
  std::deque<boost::graph_traits<Graph>::vertex_descriptor> runOrder;

  // internal graph
  std::shared_ptr<ModGraph> internalGraph;


  std::vector<std::shared_ptr<ModParameter> >   inputPieces;
  boost::graph_traits<Graph>::vertex_descriptor outputNode;
};

// class ModGraphPiece


/**
 * A lightweight wrapper class for a graph piece that is a density. ModGraphPiece does all the hard work.
 *
 * Note that this class is a friend so it can call the impl methods directly, thereby avoiding double-caching and error
 *checking.
 **/
class ModGraphDensity : public Density {
public:

  friend muq::Inference::InferenceProblem;
  friend muq::Inference::MarginalInferenceProblem;

  virtual ~ModGraphDensity() = default;

  static std::shared_ptr<ModGraphDensity> Create(std::shared_ptr<ModGraph> inGraph, const std::string& outNode = "");
  static std::shared_ptr<ModGraphDensity> Create(std::shared_ptr<ModGraph> inGraph,
                                                 const std::string             & outNode,
                                                 std::vector<std::string> const& order);

  /** Write the internal graph
   */
  template<typename ... Args>
  void writeGraphViz(Args ... args) const
  {
    genericPiece->writeGraphViz(args ...);
  }
  

private:

  std::shared_ptr<ModGraphPiece> genericPiece;

  ModGraphDensity(std::shared_ptr<ModGraphPiece> genericPiece);

  virtual double LogDensityImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    return genericPiece->EvaluateImpl(input) (0);
  }

  /** Evaluate the gradient. */
  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override
  {
    return genericPiece->GradientImpl(input, sensitivity, inputDimWrt);
  }

  /** Construct the Jacobian. */
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override
  {
    return genericPiece->JacobianImpl(input, inputDimWrt);
  }

  /** Compute the action of the Jacobian on a vector. */
  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt = 0) override
  {
    return genericPiece->JacobianActionImpl(input, target, inputDimWrt);
  }
};

/**
 * A lightweight wrapper class for a graph piece that is a RandVar. ModGraphPiece does all the hard work.
 *
 * Note that this class is a friend so it can call the impl methods directly, thereby avoiding double-caching and error
 *checking.
 **/
class ModGraphRandVar : public RandVar {
	
public:
 
  virtual ~ModGraphRandVar() = default;

  static std::shared_ptr<ModGraphRandVar> Create(std::shared_ptr<ModGraph> inGraph, const std::string& outNode = "");

  /** Write the internal graph
   */
  template<typename ... Args>
  void writeGraphViz(Args ... args) const
  {
    genericPiece->writeGraphViz(args ...);
  }

private:

  ModGraphRandVar(std::shared_ptr<ModGraphPiece> genericPiece);

  std::shared_ptr<ModGraphPiece> genericPiece;

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    return genericPiece->EvaluateImpl(input);
  }

  /** Evaluate the gradient. */
  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override
  {
    return genericPiece->GradientImpl(input, sensitivity, inputDimWrt);
  }

  /** Construct the Jacobian. */
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override
  {
    return genericPiece->JacobianImpl(input, inputDimWrt);
  }

  /** Compute the action of the Jacobian on a vector. */
  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt = 0) override
  {
    return genericPiece->JacobianActionImpl(input, target, inputDimWrt);
  }
};
} // namespace Modelling
} // namespace muq

#endif // ifndef _ModGraphPiece_h
