#ifndef POINTCACHE_H_
#define POINTCACHE_H_

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // MUQ_PYTHON == 1

#include "MUQ/Modelling/CachedModPiece.h"

namespace muq {
  namespace Modelling {
    /// A class to store input and output points 
    /**
       Suppose input points and their outputs are given but we may not know how they are generated.  This class stores input/output pairs.  If the class is evalauted where we don't already have a point, it fails.
     */
    class PointCache : public CachedModPiece {
    public: 
      
      /// Create with some initial input/output pairs 
      /**
	 Rows are the dimension of input/output.  Cols are the number of points.
	 @param[in] inputs The inputs to the (unknown) function 
	 @param[in] outputs The corresponding outputs
       */
      PointCache(Eigen::MatrixXd const& inputs, Eigen::MatrixXd const& outputs);

#if MUQ_PYTHON == 1
      PointCache(boost::python::list const& inputs, boost::python::list const& outputs);
#endif // MUQ_PYTHON == 1

    private:
      
      virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;
      
      virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
					   Eigen::VectorXd const             & sensitivity,
					   int const                           inputDimWrt) override;
      
      virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;
      
      virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
						 Eigen::VectorXd const             & target,
						 int const                           inputDimWrt) override; 
      
      virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
					  Eigen::VectorXd const             & sensitivity,
					  int const                           inputDimWrt) override;
      
    };
  } // namespace Modelling
} // namespace muq

#endif
