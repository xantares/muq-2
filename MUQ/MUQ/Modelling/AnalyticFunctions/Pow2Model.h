
#ifndef _Pow2Expr_h
#define _Pow2Expr_h


#include "MUQ/Modelling/AnalyticFunctions/ComponentwiseModel.h"
#include "MUQ/Modelling/ModGraph.h"

namespace muq {
namespace Modelling {
/**
 *  @class Pow2Model
 *  @ingroup Modelling
 *  @brief Implemenation of componentwise Pow2
 *
 */
class Pow2Model : public ComponentwiseModel {
public:

  Pow2Model(int dim) : ComponentwiseModel(dim) {}

  virtual ~Pow2Model() = default;
  
  /** Apply the base symbolic function
   *  @param[in] input  the value to be altered
   *  @return f(x) where f is a simple analytic functions such as exp(x), sin(x), etc...
   */
  virtual double BaseFunc(const double& x) const
  {
    return pow2(x);
  }

  /** return the derivative of the base symbolic function evaluated at point
   *  @param[in] input  where we want to evaluate the derivative
   *  @return df/dx the derivative of the analytic function at the input point, examples are exp(x), cos(x), etc...
   */
  virtual double BaseDeriv(const double& x) const
  {
    return log(2)*pow2(x);
  }
  
   /** return the second derivative of the base symbolic function evaluated at point
   *   @param[in] input  where we want to evaluate the second derivative
   *   @return df/dx the derivative of the analytic function at the input point, examples are exp(x), cos(x), etc...
   */
  virtual double BaseSecondDeriv(const double& x) const
  {
    return log(2)*log(2)*pow2(x);

   }
};

///Create direct operator-like calls
inline std::shared_ptr<ModGraph> Pow2(const std::shared_ptr<ModGraph>& x)
{
  //copy the graph
  auto newGraph = x->DeepCopy();
  
  //Find out the size, asserting that the graph only has one output
  auto outputSize = newGraph->outputSize();
  
  //Create the new model
  auto newComponentwiseNode = std::make_shared<Pow2Model>(outputSize);

  //add it to the graph
  newGraph->AddNode(newComponentwiseNode, newComponentwiseNode->GetName());
  //connect the nodes
  newGraph->AddEdge(x->GetOutputNodeName(), newComponentwiseNode->GetName(), 0);

  return newGraph;
}

inline std::shared_ptr<ModGraph> Pow2(const std::shared_ptr<ModPiece>& x)
{
  return Pow2(std::make_shared<ModGraph>(x));
}
}
} // namespace muq


#endif // ifndef _Pow2Expr_h
