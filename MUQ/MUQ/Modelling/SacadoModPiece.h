
#ifndef _SacadoModPiece_h
#define _SacadoModPiece_h

#include "MUQ/Modelling/ModPiece.h"

#define HAS_C99_TR1_CMATH
#include <Sacado.hpp>


typedef Sacado::Fad::DFad<double>     FadType;     /* Forward Sacado type.  DFad is the slowest but easiest to work
                                                    *  with, SFad is faster */
typedef Sacado::RadVec::ADvar<double> RadType;     /* Vector reverse Sacado type. */

typedef Sacado::Fad::DFad<FadType>     FadFadType; /* Forward-Forward Sacado type. */
typedef Sacado::RadVec::ADvar<FadType> FadRadType; /* Forward-Reverse Sacado type. */

// place this macro in the public section of SacadoModPiece children to define all the necessary virtual functions from
// a single template function
#define  SACADO_EVALUATORS(FUNCNAME)                                                                          \
  virtual Eigen::Matrix<FadRadType, Eigen::Dynamic, 1> EvaluateImpl(std::vector < Eigen::Matrix < FadRadType, \
                                                                    Eigen::Dynamic,                           \
                                                                    1 >> const & input)                       \
  {                                                                                                           \
    return FUNCNAME(input);                                                                                   \
  }                                                                                                           \
  virtual Eigen::Matrix<FadFadType, Eigen::Dynamic, 1> EvaluateImpl(std::vector < Eigen::Matrix < FadFadType, \
                                                                    Eigen::Dynamic,                           \
                                                                    1 >> const & input)                       \
  {                                                                                                           \
    return FUNCNAME(input);                                                                                   \
  }                                                                                                           \
  virtual Eigen::Matrix<FadType, Eigen::Dynamic, 1> EvaluateImpl(std::vector < Eigen::Matrix < FadType,       \
                                                                 Eigen::Dynamic,                              \
                                                                 1 >> const & input)                          \
  {                                                                                                           \
    return FUNCNAME(input);                                                                                   \
  }                                                                                                           \
  virtual Eigen::Matrix<RadType, Eigen::Dynamic, 1> EvaluateImpl(std::vector < Eigen::Matrix < RadType,       \
                                                                 Eigen::Dynamic,                              \
                                                                 1 >> const & input)                          \
  {                                                                                                           \
    return FUNCNAME(input);                                                                                   \
  }                                                                                                           \
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const & input)                            \
  {                                                                                                           \
    return FUNCNAME(input);                                                                                   \
  }


namespace muq {
namespace Modelling {
typedef Eigen::Matrix<Sacado::Rad::ADvar<double>, Eigen::Dynamic, 1> SacadoVec;

class SacadoModPiece : public ModPiece {
public:

  SacadoModPiece(const Eigen::VectorXi& inSizes, const int outSize) : ModPiece(inSizes,
                                                                               outSize,
                                                                               true,
                                                                               true,
                                                                               true,
                                                                               true,
                                                                               false)
  {}

  virtual ~SacadoModPiece() = default;
  
  /** Evaluate the forward model using an eigen vector of sacado types. */
  virtual Eigen::Matrix<FadType, Eigen::Dynamic, 1> EvaluateImpl(std::vector < Eigen::Matrix < FadType,
                                                                 Eigen::Dynamic,
                                                                 1 >> const & input) = 0;
  virtual Eigen::Matrix<RadType, Eigen::Dynamic, 1> EvaluateImpl(std::vector < Eigen::Matrix < RadType,
                                                                 Eigen::Dynamic,
                                                                 1 >> const & input) = 0;
  virtual Eigen::Matrix<FadFadType, Eigen::Dynamic, 1> EvaluateImpl(std::vector < Eigen::Matrix < FadFadType,
                                                                    Eigen::Dynamic,
                                                                    1 >> const & input) = 0;
  virtual Eigen::Matrix<FadRadType, Eigen::Dynamic, 1> EvaluateImpl(std::vector < Eigen::Matrix < FadRadType,
                                                                    Eigen::Dynamic,
                                                                    1 >> const & input) = 0;

  /** Evaluate the forward model by converting the doubles to sacado types and running the sacado type model. */
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) = 0;

  /** Evaluate the gradient of this model using Sacado automatic differentiation. */
  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt = 0) override;

  /** Compute the jacobian of this model using Sacado automatic differentiation. */
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;

  /** Evaluate the action of the Jacobian matrix on a vector using Sacado AD to compute the Jacobian. */
  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt = 0) override;

  /** Evalaute the Hessian */
  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt = 0) override;

private:

  Eigen::MatrixXd Jacobian_reverse(const std::vector<Eigen::VectorXd>& x, int const inputDimWrt);
  Eigen::MatrixXd Jacobian_forward(const std::vector<Eigen::VectorXd>& x, int const inputDimWrt);

  Eigen::MatrixXd Hessian_forward_reverse(const std::vector<Eigen::VectorXd>& x,
                                          Eigen::VectorXd const             & sensitivity,
                                          int const                           inputDimWrt);

  Eigen::MatrixXd Hessian_forward_forward(const std::vector<Eigen::VectorXd>& x,
                                          Eigen::VectorXd const             & sensitivity,
                                          int const                           inputDimWrt);
};
} // namespace Modelling
} // namespace muq


#endif // ifndef _SacadoModPiece_h
