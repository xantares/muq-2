
#ifndef PARALLELCACHEDMODPIECE_H
#define PARALLELCACHEDMODPIECE_H

#include "MUQ/Modelling/CachedModPiece.h"
#include "MUQ/Utilities/LogConfig.h"

namespace muq {
namespace Modelling {
class ParallelCachedModPiece : public CachedModPiece {
public:

  ParallelCachedModPiece(std::shared_ptr<ModPiece> sourceModPiece);

  virtual ~ParallelCachedModPiece();

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input);

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt);

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt);

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt);

  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt);

  void         ProcessIncomingMessages();

  virtual void RefreshCache()
  {
    ProcessIncomingMessages();
  }
};
}
}

#endif // PARALLELCACHEDMODPIECE_H
