
#ifndef _LinOpMvNormSpecification_h
#define _LinOpMvNormSpecification_h

#include <memory>

#include "MUQ/Utilities/LinearOperators/Operators/GeneralSymmetricLinOp.h"
#include "MUQ/Utilities/LinearOperators/Solvers/CG.h"

#include "MUQ/Modelling/ConstantMVNormSpecification.h"

namespace muq {
namespace Modelling {
class LinOpMvNormSpecification : public ConstantMVNormSpecification {
public:

  LinOpMvNormSpecification(const Eigen::VectorXd                                 & MeanIn,
                           std::shared_ptr<muq::Utilities::SymmetricOpBase> Ain,
                           ConstantMVNormSpecification::SpecificationMode          modeIn);

  /** Can this MvNorm directly evaluate the inverse covariance without performing a linear solve? Always NO. */
  virtual bool HasDirectInverseCovariance() const
  {
    return false;
  }

  /**Apply the covariance matrix to an input matrix
   *  @param[in] input The rhs matrix
   *  @retune Sigma*input where Sigma is the covariance matrix
   */
  virtual Eigen::MatrixXd ApplyCovariance(Eigen::MatrixXd const& input) const override;


  /** Apply the inverse covariance to an input matrix
   *  @param[in] input The rhs matrix
   *  @return The inverse covariance applied to the input matrix.
   */
  virtual Eigen::MatrixXd ApplyInverseCovariance(Eigen::MatrixXd const& input) const override;

  /** Apply a square root of the covariance to the input matrix.  A Cholesky decomposition of the covariance will be
   *  used for a dense covariance.
   *  @param[in] input The rhs matrix
   *  @return \f$L*input\f$ where \f$L*L^T = \Sigma\f$
   */
  virtual Eigen::MatrixXd ApplyCovarianceSqrt(Eigen::MatrixXd const& input) const override;

protected:

  virtual Eigen::VectorXd ApplyCovSqrt(Eigen::VectorXd const& input) const;

  // Modified conjugate gradient method for sampling a gaussian, based on "SAMPLING GAUSSIAN DISTRIBUTIONS IN KRYLOV
  // SPACES WITH CONJUGATE GRADIENTS" by Parker and Fox
  virtual Eigen::VectorXd ApplyCovInverse(Eigen::VectorXd const& input) const;

  virtual Eigen::VectorXd ApplyCov(Eigen::VectorXd const& input) const;


  // The linear operator
  std::shared_ptr<muq::Utilities::SymmetricOpBase> A;

  // the conjugate gradient solver
  std::shared_ptr<muq::Utilities::CG> solver;
};

// class LinOpMvNormSpecification
} // namespace Modelling
} // namespace muq

#endif // ifndef _LinOpMvNormSpecification_h
