
#ifndef SUMMODELPYTHON_H_
#define SUMMODELPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/SumModel.h"

namespace muq {
namespace Modelling {
class SumModelPython : public SumModel, public boost::python::wrapper<SumModel> {
public:

  SumModelPython(int const numInputs, int const outputSize);

  /// Python implementation of Evaluate
  virtual boost::python::list PyEvaluate(boost::python::list const& inputs);

  /// Allow the user to call the gradient function from python
  virtual boost::python::list PyGradient(boost::python::list const& pyInputs,
                                         boost::python::list const& pySensitivity,
                                         int const                  inputDimWrt = 0);

  /// Allow the user to call the Jacobian function from python
  virtual boost::python::list PyJacobian(boost::python::list const& pyInputs, int const inputDimWrt = 0);

  /// Allow the user to call the Jacobian action function from python
  virtual boost::python::list PyJacobianAction(boost::python::list const& pyInputs,
                                               boost::python::list const& pyTarget,
                                               int const                  inputDimWrt = 0);

  /// Allow the user to call the Hessian function from python
  virtual boost::python::list PyHessian(boost::python::list const& pyInputs,
                                        boost::python::list const& pySensitivity,
                                        int const                  inputDimWrt = 0);
};

void ExportSumModel();
} // namespace modelling
} // namespace muq

#endif // ifndef SUMMODELPYTHON_H_
