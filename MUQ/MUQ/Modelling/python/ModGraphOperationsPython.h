#ifndef MODGRAPHOPERATIONSPYTHON_H
#define MODGRAPHOPERATIONSPYTHON_H

#include "MUQ/Modelling/python/ModGraphPython.h"
#include "MUQ/Modelling/python/ModPiecePython.h"


namespace muq {
namespace Modelling {
	
/**
 * Thin Python wrappers of the contents of Modelling/ModGraphOperations.h.
 * 
 * These are mostly simple, but a few implementation notes:
 * # The python interfaces use ModGraphPython, so we have to overload all the operations
 * to return those instead.  
 * # Python wants operators to be member functions, so uses __radd__ to provide right handed
 * addition, and so on. We have to define a few variations for that.
 * # To be member functions, these operators are all exposed to python in the ModPiecePython and 
 * ModGraphPython files.
 * # The linear algebra operators need to unwrap python lists into VectorXds and MatrixXds.
 *
 * Most of these operate like you would expect, +,-,*,/, and mirror the C++ versions.
 * 
 * GraphCompose is slightly different, since we don't have vararg templates, we gather the inputs into a list.
 * For ModPieces:
 * GraphCompose(pieceToBind, [input1, input2, ...])
 * For ModGraphs:
 * GraphCompose(graphToBind, [input1, input2, ...], inputNames, inputDims)
 * where the later arguments follow the same default rules as the C++ code. As in C++, the inputs may
 * be ModPieces or single output ModGraphs
 * 
 */

///Add graph
std::shared_ptr<ModGraphPython> PyAdd(const std::shared_ptr<ModGraph>& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraphPython> PyAdd(const std::shared_ptr<ModPiece>& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraphPython> PyAdd(const std::shared_ptr<ModGraph>& a, const std::shared_ptr<ModPiece>& b);
std::shared_ptr<ModGraphPython> PyAdd(const std::shared_ptr<ModPiece>& a, const std::shared_ptr<ModPiece>& b);

///Subtract graph
std::shared_ptr<ModGraphPython> PySub(const std::shared_ptr<ModGraph>& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraphPython> PySub(const std::shared_ptr<ModPiece>& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraphPython> PySub(const std::shared_ptr<ModGraph>& a, const std::shared_ptr<ModPiece>& b);
std::shared_ptr<ModGraphPython> PySub(const std::shared_ptr<ModPiece>& a, const std::shared_ptr<ModPiece>& b);

///Component-wise multiply graphs
std::shared_ptr<ModGraphPython> PyMul(const std::shared_ptr<ModGraph>& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraphPython> PyMul(const std::shared_ptr<ModPiece>& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraphPython> PyMul(const std::shared_ptr<ModGraph>& a, const std::shared_ptr<ModPiece>& b);
std::shared_ptr<ModGraphPython> PyMul(const std::shared_ptr<ModPiece>& a, const std::shared_ptr<ModPiece>& b);


///The following batch are all just linear operators, where we promote scalars to matrices or vectors

///Negate graph
std::shared_ptr<ModGraphPython> PySub(const std::shared_ptr<ModGraph>& a);
std::shared_ptr<ModGraphPython> PySub(const std::shared_ptr<ModPiece>& a);

///Add a constant
std::shared_ptr<ModGraphPython> PyAdd(const std::shared_ptr<ModGraph>& a, const double& b);
std::shared_ptr<ModGraphPython> PyAdd(const std::shared_ptr<ModPiece>& a, const double& b);

///Subtract a constant
std::shared_ptr<ModGraphPython> PySub(const std::shared_ptr<ModGraph>& a, const double& b);
std::shared_ptr<ModGraphPython> PySub(const std::shared_ptr<ModPiece>& a, const double& b);

///Multiply by a constant
std::shared_ptr<ModGraphPython> PyMul(const std::shared_ptr<ModGraph>& a, const double& b);
std::shared_ptr<ModGraphPython> PyMul(const std::shared_ptr<ModPiece>& a, const double& b);

///Divide by a constant
std::shared_ptr<ModGraphPython> PyDiv(const std::shared_ptr<ModGraph>& a, const double& b);
std::shared_ptr<ModGraphPython> PyDiv(const std::shared_ptr<ModPiece>& a, const double& b);


std::shared_ptr<ModGraphPython> PyAdd(const std::shared_ptr<ModGraph>& x,
                                                      boost::python::list const      & b);

std::shared_ptr<ModGraphPython> PySub(const std::shared_ptr<ModGraph>& x,
                                                      boost::python::list const      & b);

std::shared_ptr<ModGraphPython> PyRSub(const std::shared_ptr<ModGraph>& b, const double& a);

std::shared_ptr<ModGraphPython> PyRSub(const std::shared_ptr<ModGraph>& b,
                                                       boost::python::list const      & a);

std::shared_ptr<ModGraphPython> PyRMul(const std::shared_ptr<ModGraph>& x,
                                                       boost::python::list const      & A);


std::shared_ptr<ModGraphPython> GraphCompose_graph_impl_py(std::shared_ptr<ModGraph> const& modToBind,
                                                                           boost::python::list              collectingVector_py,
                                                                           boost::python::list              inputOrder_py,
                                                                           boost::python::list              inputDims_py);

std::shared_ptr<ModGraphPython> PyAdd(const std::shared_ptr<ModPiece>& x,
                                                      boost::python::list const      & b);

std::shared_ptr<ModGraphPython> PySub(const std::shared_ptr<ModPiece>& x,
                                                      boost::python::list const      & b);

std::shared_ptr<ModGraphPython> PyRSub(const std::shared_ptr<ModPiece>& b, const double& a);

std::shared_ptr<ModGraphPython> PyRSub(const std::shared_ptr<ModPiece>& b,
                                                       boost::python::list const      & a);

std::shared_ptr<ModGraphPython> PyRMul(const std::shared_ptr<ModPiece>& x,
                                                       boost::python::list const      & A);
std::shared_ptr<ModGraphPython> GraphCompose_piece_impl_py(std::shared_ptr<ModPiece> const& modToBind,
                                                                           boost::python::list              collectingVector_py);
}
}
#endif // MODGRAPHOPERATIONSPYTHON_H
