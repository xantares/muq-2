
#ifndef MODPIECEPYTHON_H_
#define MODPIECEPYTHON_H_

#include "MUQ/config.h"

#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Modelling/ModPieceTemplates.h"

#include "MUQ/Utilities/python/PythonTranslater.h"

namespace muq {
namespace Modelling {
/// A ModPiece wrapper that is used to interface ModPiece with Python.
class ModPiecePython : public ModPiece, public boost::python::wrapper<ModPiece> {
public:

  /// constructor using python lists
  ModPiecePython(boost::python::list const& inputSizes,
                 int const                  outputSize,
                 bool const                 hasDirectGradient,
                 bool const                 hasDirectJacobian,
                 bool const                 hasDirectJacobianAction,
                 bool const                 hasDirectHessian,
                 bool const                 isRandom,
                 std::string const        & name = "");

  /// User must implement this in python

  /**
   * @param[in] input A list of lists that python uses as inputs
   * \return The evaluation as a python list
   */
  boost::python::list         PyEvaluateImpl(boost::python::list const& input);

  /// Allow the user to call the Hessian by finite difference function from python
  virtual boost::python::list PyHessianImplDefault(boost::python::list const& inputs,
                                                   boost::python::list const& sensIn,
                                                   int const                  inputDimWrt);

  /// Call the gradient implementation

  /**
   * The user can implement this in python, but if not it defaults to finite difference
   */
  boost::python::list PyGradientImpl(boost::python::list const& input,
                                     boost::python::list const& sens,
                                     int const                  inputDimWrt);

  /// Define a default function for Jacobian to call if there is no python implementation

  /**
   * This will be called if the user as indicated there is a python implemenatation of the Jacobian but has not
   *implmented.  It is finite difference but requires extra python vector Eigen::VectorXd translations and therefore has
   *extra overhead.
   */
  boost::python::list PyJacobianImplDefault(boost::python::list const& inputs, int const inputDimWrt);

  /// call Jacobian from python

  /**
   * The user can implement this in python, but if not it defaults to finite difference
   */
  virtual boost::python::list PyJacobianImpl(boost::python::list const& inputs, int const inputDimWrt);

  /// call Jacobian acttion from python

  /**
   * The user can implement this in python, but if not it defaults to finite difference
   */
  virtual boost::python::list PyJacobianActionImpl(boost::python::list const& inputs,
                                                   boost::python::list const& target,
                                                   int const                  inputDimWrt);

  /// Define a default function for Jacobian action to call if there is no python implementation

  /**
   * This will be called if the user as indicated there is a python implemenatation of the Jacobian but has not
   *implmented.  It is finite difference but requires extra python vector Eigen::VectorXd translations and therefore has
   *extra overhead.
   */
  virtual boost::python::list PyJacobianActionImplDefault(boost::python::list const& inputs,
                                                          boost::python::list const& target,
                                                          int const                  inputDimWrt);

  /// Call the Hessian implementation
  boost::python::list PyHessianImpl(boost::python::list const& input,
                                    boost::python::list const& sens,
                                    int const                  inputDimWrt);

  boost::python::list PyGradientImplDefault(boost::python::list const& inputs,
                                            boost::python::list const& sensIn,
                                            int const                  inputDimWrt);

private:

  /// Call the python evaluation

  /**
   * Translate vectors to/from Eigen to python
   */
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;

  /// Call the python gradient evaluation
  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override;

  /// Call the python Jacobian evaluation
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;

  /// Call the python Jacobian action evaluation
  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override;

  /// Call the python Hessian evaluation
  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt) override;
};

// end ModPiecePython

// Add ModPiece to the python MUQmodelling module
void ExportModPiece();
} // namespace modelling
} // namespace muq

#endif // ifndef MODPIECEPYTHON_H_
