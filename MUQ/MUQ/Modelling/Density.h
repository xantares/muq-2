
#ifndef Density_h_
#define Density_h_

#include "MUQ/Modelling/ModPiece.h"

namespace muq {
namespace Modelling {
///Subclass of ModPiece that guarantees children have scalar output. May not be random.
class Density : public ModPiece {
public:

  Density(Eigen::VectorXi const& inputSizes,
          bool const             hasDirectGradient,
          bool const             hasDirectJacobian,
          bool const             hasDirectJacobianAction,
          bool const             hasDirectHessian);


  virtual ~Density() = default;


  ///A convenient pass-through that grabs the one output from an Evaluate call. Otherwise does nothing.
  template<typename ... Args>
  double LogDensity(Args ... args)
  {
    return Evaluate(args ...) (0);
  }

  ///Allow call to evaluate with no inputs, which will fail if the ModPiece requires inputs
  double LogDensity()
  {
    return Evaluate() (0);
  }

#if MUQ_PYTHON==1

  /// A function to call LogDensity from python
  double PyLogDensity(boost::python::list const& inputs)
  {
    std::vector<Eigen::VectorXd> eigenInputs = muq::Utilities::PythonListToVector(inputs);

    return LogDensity(eigenInputs);
  }

#endif 

private:

  virtual double          LogDensityImpl(std::vector<Eigen::VectorXd> const& input) = 0;

  ///Evaluate is just the density as a vector
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    return Eigen::VectorXd::Constant(1, LogDensityImpl(input)); //turn constant into length 1 vector
  }
};
}                                                               // namespace Modelling
} //namespace muq


#endif // ifndef Density_h_
