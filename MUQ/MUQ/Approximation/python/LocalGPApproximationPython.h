#ifndef LOCALGPAPPROXIMATIONPYTHON_H_
#define LOCALGPAPPROXIMATIONPYTHON_H_

#include "MUQ/Approximation/Incremental/LocalGPApproximation.h"

namespace muq {
namespace Approximation {
void ExportLocalGPApproximation();
} // namespace Regression
} // namespace muq

#endif // ifndef LOCALGPAPPROXIMATIONPYTHON_H_
