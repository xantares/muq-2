#ifndef POLYCHAOSXMLHELPERS_H
#define POLYCHAOSXMLHELPERS_H

#include <boost/property_tree/ptree.hpp>


namespace muq {
namespace Utilities {
  class CachedFunctionWrapper;
  class VariableCollection;
}


namespace Approximation {

class SmolyakPCEFactory;
class PolynomialChaosExpansion;

///Construct a variable collection from a ptree.

/**
 * Takes xml like this:
 *  <Variable>
 *  <index>1</index> <!-- Specifies the relative ordering of the variable block -->
 *  <type>Gaussian</type> <!-- Type of variables, Gaussian or Legendre. -->
 *  <count>2</type> <!-- How many in this block, defaults to 1-->
 *  <name>x</name> <!-- A name. Defaults to x.-->
 *  </Variable>
 *
 * Repeated as desired for more variables.
 * The indices must be totally ordered integers, but need not be consecutive. Blocks of variables
 * with the same type may be specified using the count, defaults to 1. Supported
 * variable types are "Gaussian" and "Legendre". Name may be supplied or defaults to "x".
 *
 **/
 std::shared_ptr<muq::Utilities::VariableCollection> ConstructVariableCollection(boost::property_tree::ptree const& pt);

///Construct a CachedFunctionWrapper from a ptree

/**
 * e.g.
 *  <InputDim>3</InputDim>
 *  <OutputDim>2</OutputDim>
 *  <ExternalFunction>data/tests/testShellFn</ExternalFunction>
 *
 * Input and output dimensions are specified.
 * Always creates a cached around an external executable. This exe is called with
 * "exe inputPath outputPath", where the exe must take a batch of column vectors
 * and provide output vectors in the same order. Either use an absolute path
 * or relative to the run directory. File i/o uses /tmp.
 **/

//     std::shared_ptr<muq::utilities::CachedFunctionWrapper>
// ConstructFunctionWrapper(boost::property_tree::ptree const& pt);

///Constructs a SmolyakPCEFactory from a ptree.

/**
 * ptree starts with name <Polychaos>. constructs the nested <Function> and
 *  <VariableCollection>  and returns the resulting PCEFactory.
 **/

//     std::shared_ptr<SmolyakPCEFactory> ConstructSmolyakPCE(boost::property_tree::ptree const& pt);


///Compute a PCE as directed by an xml file.

/**
 * Uses the above methods to construct a PCE problem, that is, variables and function.
 * Then computes the PCE as directed below:
 *
 * Top level xml tag is  <Polychaos>. Attributes:
 *  <InitialSimplex>2</InitialSimplex> <!-- initial simplex, default=1 -->
 *  <AdaptationTime>1e-4</AdaptationTime><!-- Additional adaptation time in seconds, default=0 -->
 *
 *  <!-- Where to print the pce -->
 *  <PceOutputPath>results/testXMLPce</PceOutputPath>
 *
 *  Also uses <Function> and <VariableCollection> as above. See [muqDir]/data/tests/ExamplePolychaosConfig.xml.
 **/

//      std::shared_ptr<PolynomialChaosExpansion> ComputePCE(boost::property_tree::ptree const& pt);
}
}


#endif // POLYCHAOSXMLHELPERS_H
