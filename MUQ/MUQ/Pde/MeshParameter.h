#ifndef MESHPARAMETER_H_
#define MESHPARAMETER_H_

#include <boost/property_tree/ptree.hpp>

#include <boost/algorithm/string/erase.hpp>

#include "libmesh/exodusII_io.h"
#include "libmesh/equation_systems.h"
#include "libmesh/numeric_vector.h"

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // MUQ_PYTHON == 1

#include "MUQ/Utilities/LibMeshTranslater.h"
#include "MUQ/Utilities/VariableNameManipulation.h"

#include "MUQ/Modelling/ModPiece.h"

#include "MUQ/Pde/GenericEquationSystems.h"

namespace muq {
  namespace Pde {
    /// A mesh dependent parameter expressed as a (known) function of space
    /**
     *   Allows the user to implement a spatial parameter on a mesh that is a known function of space. 
     */
    class MeshParameter : public muq::Modelling::ModPiece, public std::enable_shared_from_this<MeshParameter> {
    public:
      
      virtual ~MeshParameter() = default;
      
      /// A function that creates a muq::Pde::MeshParameter
      typedef std::function<std::shared_ptr<MeshParameter>(std::string const&,
							   std::shared_ptr<GenericEquationSystems> const&,
							   boost::property_tree::ptree const&)> MeshParaConstructor;
      
      /// A map that holds all of the muq::Pde::MeshParameter that can be created
      typedef std::map<std::string, MeshParaConstructor> MeshParaMap;
      
      /// Get the map that stores all of the muq::Pde::MeshParameters that can be created
      /**
	 \return A muq::Pde::MeshParameter::MeshParaMap that has all of the children's cosntructors 
      */
      static std::shared_ptr<MeshParaMap> GetMeshParaMap();
      
      /// Create a muq::Pde::MeshParameter 
      /**
	 <ol type="1">
	 <li> The system name (<EM>MeshParameter.SystemName</EM>)
	     <ul>
	     <li> The name of the system of parameters (e.g. "velocity")
	     </ul>
	 <li> The name of each variable (<EM>MeshParameter.VariableNames</EM>)
	     <ul>
             <li> Comma-separated named of each variable in the system (e.g. "u,v")
	     </ul>
	 <li> The order of each variable on the element (<EM>MeshParameter.VariableOrders</EM>
	     <ul>
             <li> Comma-separated numbers for each variable; 0: Constant, 1: Linear, 2: Quadratic (e.g. "1,2")
	     </ul>
         <li> The names of each input (<EM>MeshParameter.InputNames</EM>)
	     <ul>
             <li> Comma-separated input parameter names (e.g. "a,b")
	     </ul>
         <li> The size of each parameter (<EM>MeshParameter.InputSizes</EM>)
	     <ul>
             <li> Each input must have a size
             <li> Comma-separated number (e.g. "1,2")
	     </ul>
	 <li> Compute spatial graident of mesh-dependent inputs? (<EM>MeshParameter.ComputeInputGradients</EM>)
	     <ul>
             <li> True: compute the spatial gradients mesh-dependent parameters at degrees of freedom
	     <li> False: only compute the values (default)
	     </ul>
	 </ol>
         @param[in] system The system holding the mesh the parameter is define over
	 @param[in] para The options for the muq::Pde::MeshParameter
	 \return The muq::Pde::MeshParameter 
      */
      static std::shared_ptr<MeshParameter> Create(std::shared_ptr<GenericEquationSystems> const& system,
						   boost::property_tree::ptree const            & para);
      
      /// Constructor (each child must call this one)
      /**
	 @param[in] systemName The name of the system
	 @param[in] system The system holding the mesh the parameter is define over
	 @param[in] para The options for the muq::Pde::MeshParameter (see muq::Pde::MeshParameter::Create)
      */
      MeshParameter(std::string const                            & systemName,
		    std::shared_ptr<GenericEquationSystems> const& system,
		    boost::property_tree::ptree const& para);
      
      /// Return the value of the mesh parameter at the point pt
      /**
	 WARNING: this function has to find the element containing the point, which is a slow operation.
         @param[in] pt The point where we want the field value
         @param[in] var The name of the varaible we want
       */
      double EvaluateAtPoint(libMesh::Point const& pt, std::string const& var) const;
      
      /// Return the gradient of the mesh parameter at the point pt
      /**
	 WARNING: this function has to find the element containing the point, which is a slow operation.
         @param[in] pt The point where we want the field gradient
         @param[in] var The name of the varaible we want
       */
      libMesh::RealGradient GradientAtPoint(libMesh::Point const& pt, std::string const& var) const;
      
    protected:
      
      /// Add the system to muq::Pde::GenericEquationSystems and return the system name 
      /**
	 muq::Pde::GenericEquationSystems holds a mesh and all the systems defined over that mesh.  Add the muq::Pde::MeshParameter to the system.
	 @param[in] system The system hold the mesh this parameter is defined over
	 @param[in] para A ptree holding all the options (see muq::Pde::MeshParameter::Create)
      */
      static std::string MakeLibMeshSystem(std::shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& para);
      
      /// Evaluate the mesh parameter at a point 
      /**
	 The user must override this; this function evaluates the parameter at a point.
	 @param[in] x x-coordinate 
	 @param[in] y y-coordinate (zero if less than two dimensions) 
	 @param[in] z z-coordinate (zero if less than three dimensions) 
	 @param[in] var The variable name
       */
      virtual libMesh::Number ParameterEvaluate(double const x, double const y, double const z, std::string const& var) const = 0;
      
      /// A muq::Pde::GenericEquationSystems to store the parameter on a mesh
      std::shared_ptr<GenericEquationSystems> system;
      
      /// The name of each input
      const std::vector<std::string> inputNames;
      
    private:
      
      /// Evaluate the parameter at each DOF for the parameter
      /**
	 @param[in] inputs The inputs to the muq::Pde::MeshParameter
       */
      virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;
      
      /// Wrap the evaluation of the parameter at the DOFs in a static function
      /**
	 @param[in] p The point where the parameter is being evalauted 
	 @param[in] para The libmesh parameters required to evaluate the point 
	 @param[in] sysName The name of the system 
	 @param[in] var The name of the variable in the system
       */
      static libMesh::Number ParameterEvaluateWrapper(libMesh::Point const     & p,
						      libMesh::Parameters const& para,
						      std::string const        & sysName,
						      std::string const        & var);

      /// Extract inputs; store them as doubles, Eigen::VectorXds, or put them on the mesh 
      /**
	 muq::Pde::MeshParameter::EvaluateImpl takes Eigen::VectorXd as inputs.  If they are size 1, they can be stored as a double in the equation system parameters, if they have greater size they are stored as an Eigen::VectorXd.  If the equation system has a system matching the parameter's names, they are projected onto the mesh.
	 @param[in] inputs The inputs (same as muq::Pde::MeshParameter::EvaluateImpl)
	 \return The number of mesh-dependent inputs
       */
      int StoreInputs(std::vector<Eigen::VectorXd> const& inputs);

      /// Project mesh-dependent input onto the mesh 
      /**
	 @param[in] name The input name
	 @param[in] input The input at each degree of freedom
	 @param[in,out] numVars The current count of mesh-dependent variables
	 \return The number of variables in the input system
       */
      int ProjectInput(std::string const& name, Eigen::VectorXd const& in, int& numVars);
      
      /// Exact the input sizes from the boost::property_tree::ptree 
      /**
	 Turns the comma-separated string into an Eigen::VectorXi
	 @param[in] para Options for muq::Pde::MeshParameter (see muq::Pde::MeshParameter::Create)
	 \return An Eigen::VectorXi of sizes (empty if there are no inputs)
      */
      static Eigen::VectorXi InputSizes(boost::property_tree::ptree const& para);
      
      /// Exact the input names from the boost::property_tree::ptree 
      /**
	 Turns the comma-separated string into a std::vector of names
	 @param[in] para Options for muq::Pde::MeshParameter (see muq::Pde::MeshParameter::Create)
	 \return A std::vector of names (empty if there are no inputs)
      */
      static std::vector<std::string> InputNames(boost::property_tree::ptree const& para);

      /// Compute spatial gradients of mesh-dependent input parameter at degrees of freedom?
      /**
	 Spatial gradient gradients of input parameters can be expensive so they are not computed by default.  This flag lets the user decide to compute them.
       */
      const bool computeInputGradients;
    };
  } // namespace Pde
} // namespace muq

#define REGISTER_MESH_PARAMETER(NAME) static auto NAME##_ =		\
    muq::Pde::MeshParameter::GetMeshParaMap()->insert(std::make_pair(#NAME, boost::shared_factory<NAME>()));

#endif // ifndef MESHPARAMETER_H_
