#ifndef HDF5LOGGING_H
#define HDF5LOGGING_H

#include <map>
#include <string>
#include <memory>
#include <Eigen/Dense>

#include <boost/property_tree/ptree.hpp>

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1

#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Utilities/HDF5Wrapper.h"


namespace muq {
namespace Utilities {
///A way to collect all the entries to be logged as one iteration
/**
 * This class is basically just a wrapper around the std::map loggedData. Users may write any values they want to log
 *into the map. It is easy to pass one of these to a function by reference, allow that method (and possibly children) to
 *fill it in, and then for the originating method to store the data it gets back, or not, as it chooses.
 *
 * This facility allows the function to return multiple values at once, including intermediate products for logging. C++
 ****generally doesn't allow multiple return, and while it makes sense to allow this for logging, it is important that
 *this isn't used as a general purpose hack to do this, so in general you shouldn't be reading things out of the map.
 *
 * See HDF5Logging for the logger.
 */
class HDF5LogEntry {
public:

  HDF5LogEntry()          = default;
  virtual ~HDF5LogEntry() = default;

  /**
   * @brief A helper method for merging two logs.
   *
   * Say log1 has fields "x" and "y" and "this" log entry has field "z". If we merge log1 into log2 with prefix "point",
   *then this would contain "point/x", "point/y", and "z".
   *
   * @param prefix The prefix is added to the keys
   * @param dataToPrefix The log entry to add to this one, with the given prefix.
   * @return void
   */
  void MergeWithPrefix(std::string const prefix, std::shared_ptr<HDF5LogEntry> dataToPrefix);

  ///The actual logged data. Users should write into it directly.
  /**
   * For example, log["x"] = vector1;
   */
  std::map<std::string, Eigen::VectorXd> loggedData;
};

///Works with the HDF5Wrapper library to provide a vector-based logging facility for things that iterate.
/**
 * The goal of this library is to help an iterative algorithm to collect data for the user. Especially, this can be
 *tricky if you want to collect data from other methods you call that don't know what iteration you're on. The idea,
 *then, is that on iteration X, you create an HDF5LogEntry, and pass it around to populate it with everything you want
 *to log. When the iteration is complete, the object managing the iteration can then save everything.
 *
 * Knowing that we want to log to HDF5, we follow the convention of using directory-like hierarchical dataset names.
 *This class is supposed to be lightweight, so we rely on loosely structured data types and the class makes an effort to
 *just make it work.
 *
 * One thing to log is simply a (string, VectorXd) pair. For any key, the vector must be the same length every time it
 *is provided. The HDF5 file will allocate space to store the key every iteration, although it may be present or omitted
 *in any iteration. The keys are not needed in advance, and may appear for the first time at any iteration. The keys may
 *have arbitrary paths, which and are created on demand.
 *
 * This class is all static methods, so the user need not create instantiations and will typically just call WriteEntry
 * repeatedly. It has some static state, which is used to cache results, reducing the number of calls to the HDF5
 *library and the filesystem, so makes using logging a bit less "spammy". This user doesn't interact with the caching.
 *
 * May be safely used by multiple data providers sources, in a single-threaded environment, as long as the fully
 *qualified keys don't overlap. We do not check for key overlap.
 *
 * The canonical, development case for this class was logging data out of MCMC.
 */
class HDF5Logging {
public:

  HDF5Logging() = default;
  virtual ~HDF5Logging()
  {
    //make sure it buffer is flushed on quit, when static object destructs
    FlushBuffer();
  }

  /**
   * Used to set the delay between HDF5 writes, which is a global property. Reads HDF5.LogWriteFrequency from the ptree.
   *Does not support resizing that buffer, so the buffer must be empty when this method is called. Typically done at
   *program start, and then not touched. Defaults to 100, which is enough to prevent hard drive trashing.
   */
  static void Configure(boost::property_tree::ptree const& properties);

#if MUQ_PYTHON == 1
  static void PyConfigure(boost::python::dict const& properties);
#endif // if MUQ_PYTHON == 1

  ///Write all the buffers immediately.
  /**
   * Should not ordinarily be needed. If called often, would circumvent the caching preventing this code from hitting
   *the hard drive too often. Don't do that.
   */
  static void FlushBuffer();

  /**
   *  Take the entries and put them into the cache, prefixing the names with the input prefix. These entries become
   *column number entryNumber of totalEntries. Creates new fields as needed.
   *
   * As necessary, the cache is manipulated or flushed.
   *
   * totalEntries must be a multiple of the logging frequency.
   */
  /**
   * @brief The main user-facing operation, adds data from the provided log entry to the log.
   *
   * @param pathPrefix The group prefix added to the entry keys to create the HDF5 dataset name.
   * @param entry The actual key/value pairs to log.
   * @param entryNumber The index of the entry.
   * @param totalEntries The total number of entries. Must be a multiple of logWriteFrequency.
   * @return void
   *
   * If the total number of needed entries is not known in advance, round up. It can't be changed for any complete key.
   */
  static void WriteEntry(std::string pathPrefix, std::shared_ptr<HDF5LogEntry> entry, int entryNumber,
                         int totalEntries);

  ///Write the buffers with FlushBuffer and then dump them.
  /**
   * After calling this method, you can call Configure safely.
   */
  static void WipeBuffers();

private:

  ///How often the hdf5 values should be written and likewise how large the buffers should be
  static int logWriteFrequency;

  ///Stores a map from the hdf5 name to a tuple with the buffer and the index for where the buffer starts, and the
  // maximum size
  static std::map<std::string, std::tuple<std::shared_ptr<Eigen::MatrixXd>, std::shared_ptr<int>,
                                          std::shared_ptr<int> > > dataBuffer;
};


///A function that reads a property from a ptree and echoes the value for archival purposes.
/**
 * Works much like the ptree get() function, in that you provide where to read and the default value. The value is
 ****returned, but is first logged to GLOG.
 *
 * Also, the value will be written as an attribute on the group named hdf5Group, if provided. If not, it will try to
 *find a group under the "HDF5.OutputGroup" key in the ptree. This group is created as needed.
 *
 */
template<typename T>
T ReadAndLogParameter(boost::property_tree::ptree const& properties,
                      std::string const                  propertyName,
                      T const                            default_value,
                      std::string const                  hdf5Group = "")
{
  T valueToUse     = properties.get(propertyName, default_value);
  std::string groupToWriteTo = hdf5Group;

  if (hdf5Group.compare("") == 0) {
    groupToWriteTo = properties.get("HDF5.OutputGroup", "");
    LOG(INFO) << "Found parameter logging group from parameters = " << groupToWriteTo;
  }

  if (hdf5Group.compare("") != 0) {
    if (!HDF5Wrapper::DoesGroupExist(groupToWriteTo)) {
      HDF5Wrapper::CreateGroup(groupToWriteTo);
    }

    HDF5Wrapper::WriteAttribute(groupToWriteTo, propertyName, valueToUse);
  }

  LOG(INFO) << "Using " << propertyName << " = " << valueToUse << ", writing to hdf5 group = " << groupToWriteTo;

  return valueToUse;
}

}
}

#endif // HDF5LOGGING_H
