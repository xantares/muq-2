
#ifndef _GeneralSymmetricLinOp_h
#define _GeneralSymmetricLinOp_h

#include <Eigen/Dense>

#include "MUQ/Utilities/LinearOperators/Operators/SymmetricOpBase.h"

namespace muq {
namespace Utilities {
template<typename OpType>
class GeneralSymmetricLinOp : public SymmetricOpBase {
public:

  GeneralSymmetricLinOp(const OpType& Ain, int size) : SymmetricOpBase(size), A(Ain) {}

  virtual Eigen::VectorXd apply(const Eigen::VectorXd& x) override
  {
    return A * x;
  }

protected:

  OpType A;
};

// class GeneralSymmetricLinOp
} // namespace Utilities
} // namespace muq

#endif // ifndef _GeneralSymmetricLinOp_h
