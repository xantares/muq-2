
#ifndef _Stoppers_h
#define _Stoppers_h

//std includes
#include <memory>
#include <map>

// boost includes
#include <boost/property_tree/ptree.hpp>

// eigen includes
#include <Eigen/Core>


// other muq includes
#include "MUQ/Utilities/Trees/IndexType.h"


#define  REGISTER_STOPPER_DEC_TYPE(NAME) \
  static std::shared_ptr < muq::Utilities::DerivedStopperRegister < TrainingTypes ... >> reg;

#define  REGISTER_STOPPER_DEF_TYPE(NAME)                                                                   \
  std::shared_ptr < muq::Utilities::DerivedStopperRegister < NAME, TrainingTypes ... >> NAME::reg = \
    std::make_shared < muq::Utilities::DerivedStopperRegister < NAME >>                             \
    (# NAME, static_cast<NAME::CreateFuncType>(NAME::Create));


namespace muq {
namespace Utilities {
template<typename ... TrainingTypes>
struct DerivedStopperRegister;

/** Decides when to stop splitting the points. */
template<typename ... TrainingTypes>
class Stopper {
public:

  virtual bool StopSplitting() = 0;


  // //////////////////////////////////////////////
  // REGISTRATION CODE
  // //////////////////////////////////////////////

  typedef std::function < std::shared_ptr <
    Stopper<TrainingTypes ... >>
            (const Eigen::MatrixXd&, const muq::Utilities::TreeIndexContainer&, int,
             boost::property_tree::ptree&,
             TrainingTypes ...)> CreateFuncType;

  static std::shared_ptr < Stopper < TrainingTypes ... >> Create(const Eigen::MatrixXd & pts,
                                                                 const TreeIndexContainer &inds,
                                                                 int depth,
                                                                 boost::property_tree::ptree & properties,
                                                                 TrainingTypes ... params)
  {
    // extract the boost any object from the register
    CreateFuncType f =
      Stopper<TrainingTypes ...>::GetConstructorMap()->at(properties.get("Trees.StopMethod", "PtDepthStopper"));

    return f(pts, inds, depth, properties, params ...);
  };

  ///Typedef for the map used to track available stopper types.   Not user code.
  typedef std::map<std::string, CreateFuncType> Name2ConstructorMapType;

  ///Globally available way to get the map that tells you what stopper methods are available.
  static std::shared_ptr<Name2ConstructorMapType> GetConstructorMap()
  {
    static std::shared_ptr<Stopper<TrainingTypes ...>::Name2ConstructorMapType> map;

    // if the map doesn't yet exist, create it
    if (!map) {
      map = std::make_shared<Stopper<TrainingTypes ...>::Name2ConstructorMapType>();
    }
    return map;
  }

  // ///////////////////////////////////////////////
  // ///////////////////////////////////////////////
};

// class Stopper

template<typename ... TrainingTypes>
struct DerivedStopperRegister {
  DerivedStopperRegister(std::string s, typename Stopper<TrainingTypes ...>::CreateFuncType func)
  {
    Stopper<TrainingTypes ...>::GetConstructorMap()->insert(std::make_pair(s, func));
  }
};


template<typename ... TrainingTypes>
class PtDepthStopper : public Stopper<TrainingTypes ...> {
public:

  PtDepthStopper(const Eigen::MatrixXd      & pts,
                 const TreeIndexContainer   & inds,
                 int                          depth,
                 boost::property_tree::ptree& properties)
  {
    int maxDepth = properties.get("Tree.Stopping.MaxDepth", 10);
    int minPts   = properties.get("Tree.Stopping.MinPts", 4);

    shouldStop = ((std::distance(inds.begin(), inds.end()) < minPts) || (depth > maxDepth));
  }

  virtual bool StopSplitting() override
  {
    return shouldStop;
  }

  // //////////////////////////////////////////////
  // REGISTRATION CODE
  // //////////////////////////////////////////////

  static std::shared_ptr < Stopper < TrainingTypes ... >> Create(const Eigen::MatrixXd & pts,
                                                                 const muq::Utilities::TreeIndexContainer & inds,
                                                                 int depth,
                                                                 boost::property_tree::ptree & properties,
                                                                 TrainingTypes ... unused)
  {
    return std::dynamic_pointer_cast < Stopper < TrainingTypes ... >>
           (std::make_shared < PtDepthStopper < TrainingTypes ... >> (pts, inds, depth, properties));
  };

  // ///////////////////////////////////////////////
  // ///////////////////////////////////////////////

private:

  REGISTER_STOPPER_DEC_TYPE(PtDepthStopper) bool shouldStop;
};

// class PtDepthStopper
} // namespace Utilities
} // namespace muq

#endif // ifndef _Stoppers_h
