#ifndef QUADRATURE_H_
#define QUADRATURE_H_

#include <memory>

#include <Eigen/Core>

namespace muq {
  namespace Utilities {
    ///forward declaration avoids inclusion
    class VariableCollection;
    
    /**
     * An abstract class for performing numerical quadrature of functions from
     * R^n -> R^m. Inputs and outputs to functions are represented as arma::colvec.
     *
     * There are two essential methods:
     * ComputeIntegral() - takes a function pointer and computes the numeric integral
     * GetNodesAndWeights() - return (by reference) the nodes and weights for the user
     *
     * The latter option is probably most useful for the computation of many integrals,
     * or where the user needs to control the evaluation of the integrand. The nodes
     * computation is pure virtual and needs to be provided by a subclass.
     *
     * The constructor takes a specification of the input variables to the function,
     * which also define the per-dimension 1D quadrature rule to use, which also determines
     * the domain and a weight function for the integral, if any.
     *
     * ComputeIntegral() will actually produce the integral, taking a FunctionWrapper,
     * which use functions from colvecs to colvecs.
     *
     * This class, and its subclasses, often rely on MultiIndexFamilies to help construct
     * the grid of quadrature points and iterate over them.
     */
    class Quadrature {
    public:
      
      /**
       * Initialize the quadrature rule with the variables. Subclasses must also
       * take in some specification of the order of the desired quadrature rule, if
       * appropriate. Subclasses should also call this one. This class keeps a pointer
       * to the input collection.
       */
      Quadrature(std::shared_ptr<muq::Utilities::VariableCollection> variables) : variables(variables) {};
      virtual ~Quadrature() = default;
      
      ///Compute the nodes and weights of this quadrature rule
      
      /**
       * A pure virtual function that computes and returns nodes and weights for this quadrature rule.
       * @param nodes a matrix that will contain the nodes, where each col in the matrix
       * is a node
       * @param weights a column vector with the weights associated with each row of the nodes
       * matrix
       * @return
       */
      virtual void GetNodesAndWeights(Eigen::MatrixXd& nodes, Eigen::VectorXd& weights) = 0;
      
      /**
       * A function to compute a single integral. Calls GetNodesAndWeights and then evaluates f,
       * a pointer to the function to integrate, at the nodes.
       *
       * @param f A function pointer to f, the integrand. The integrand takes a rowvec and
       * returns a double.
       * @return the integral, as evaluated by the selected quadrature rule.
       */
      template<typename T>
      Eigen::VectorXd ComputeIntegral(std::shared_ptr<T> fn){
        Eigen::MatrixXd nodes;
        Eigen::VectorXd weights;
        
        //compute the nodes and weights
        this->GetNodesAndWeights(nodes, weights);
        
        //initialize someplace to put the evaluations
        
        Eigen::MatrixXd weightedFnEvals(fn->outputSize, nodes.cols());
        for (int i = 0; i < nodes.cols(); ++i) {
          weightedFnEvals.col(i) = fn->Evaluate(nodes.col(i));
        }
        
        Eigen::MatrixXd integral = weightedFnEvals * weights;
        
        //check that the multiplication went well
        assert(integral.rows() == fn->outputSize && integral.cols() == 1);
        return integral.col(0);
      }
      
      
      /**
       * Pointer to the variable collection specification for this quadrature problem.
       * which specifically provides references to the 1D quadrature rules for each dimension.
       * Probably shouldn't be public, but it's easier.
       *
       * Does not clean up the variables, as they are a structure that is passed in!
       */
      std::shared_ptr<muq::Utilities::VariableCollection> variables;
    };
  }
}

#endif /* QUADRATURE_H_ */
