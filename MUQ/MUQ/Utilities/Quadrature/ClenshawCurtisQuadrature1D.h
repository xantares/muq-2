#ifndef CLENSHAWCURTISQUADRATURE1D_H_
#define CLENSHAWCURTISQUADRATURE1D_H_

#include <boost/serialization/access.hpp>

#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"

namespace muq {
namespace Utilities {
/**
 * Implements Clenshaw Curtis quadrature, based on Chebyshev interpolation. Rules are
 * precomputed by using http://www.sam.math.ethz.ch/~waldvoge/Papers/fejer.html
 *
 * Stored in data/Chenshaw_Curtis. Indexed by hierarchical level, corresponding to points
 * n = 1,3,5,9,17,33,65,129, ..., 32769. More can be computed.
 **/
class ClenshawCurtisQuadrature1D : public QuadratureFamily1D {
public:

  ClenshawCurtisQuadrature1D();
  virtual ~ClenshawCurtisQuadrature1D();

  typedef std::shared_ptr<ClenshawCurtisQuadrature1D> Ptr;

  ///Uses a lookup table to return the approximate orders

  /**
   * Taken from http://people.sc.fsu.edu/~jburkardt/cpp_src/patterson_rule/patterson_rule.html
   * Uses the actual accuracy, using the symmetry of the rule, which buys us +1 for all
   * but the first rule.
   **/
  unsigned int GetPrecisePolyOrder(unsigned int const order) const;

private:

  ///Make class serializable
  friend class boost::serialization::access;

  template<class Archive>
  void         serialize(Archive& ar, const unsigned int version);

  ///Return the pre-computed nodes and weights
  virtual void ComputeNodesAndWeights(unsigned int const                   order,
                                      std::shared_ptr<Eigen::RowVectorXd>& nodes,
                                      std::shared_ptr<Eigen::RowVectorXd>& weights) const;
};
}
}

#endif /* CLENSHAWCURTISQUADRATURE1D_H_ */
