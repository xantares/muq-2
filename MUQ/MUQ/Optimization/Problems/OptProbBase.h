
#ifndef OptProblem_h_
#define OptProblem_h_

// standard library includes
#include <memory>
#include <vector>

// external library includes
#include <Eigen/Core>

// other MUQ inclues
#include "MUQ/Optimization/Constraints/ConstraintList.h"

namespace muq {
namespace Optimization {
/** @class OptProblem
 *  @ingroup Optimization
 *
 *  @brief General optimization problem abstract interface
 *  @details Before we can solve an optimization problem, a common way of describing the problem is needed so the
 * solvers
 *     (steepest descent, etc...) know how to evaluate the objective or get the objective gradient.  This class provides
 *     that interface.  Children of this class will implement the eval function, which evaluates the objective and may
 *     implement the grad function, which computes the gradient of the objective at a point.  However, the grad function
 *     is implemented using finite differences in the OptProblem base class.  This class also allows constraints to be
 *     added to the problem.
 */
class OptProbBase {
public:

  /** Construct this class from the problem dimension.
   *  @param[in] dimIn The number of decision variables in the optimization problem
   */
  OptProbBase(int dimIn) : equalityConsts(dimIn), inequalityConsts(dimIn), dim(dimIn) {}

  /** Evaluate the objective.
   *  @param[in] xc The location to evaluate the objective.
   *  @return The value of the objective at xc.
   */
  virtual double          eval(const Eigen::VectorXd& xc) = 0;

  /** Evaluate the gradient, using finite differences if a more sophisticated method was not implemented in child class.
   *  @param[in] xc The location to compute the objective and gradient.
   *  @param[out] gradient A vector that will be filled with the gradient.
   *  @return xc The value of the objective at xc.
   */
  virtual double          grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient);

  /** Apply the Hessian inverse (or approximate Hessian inverse) to the input vector.  This function defaults to finite
   *  differences but more efficient approximations such as Gauss-Newton model Hessians for nonlinear least squares
   *  problems are implemented in child classes.
   *  @param[in] xc The location to evaluate the Hessian
   *  @param[in] vecIn The vector we want to apply the Hessian to.
   *  @return H^{-1}*vecIn The inverse Hessian evaluated at xc applied to the input vector.
   */
  virtual Eigen::VectorXd applyInvHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn);
  virtual Eigen::MatrixXd getInvHess(const Eigen::VectorXd& xc);

  /** Apply the Hessian (or approximate Hessian) to the input vector.  This function defaults to finite differences but
   *  more efficient approximations such as Gauss-Newton model Hessians for nonlinear least squares problems are
   *  implemented in child classes.
   *  @param[in] xc The location to evaluate the Hessian
   *  @param[in] vecIn The vector we want to apply the Hessian to.
   *  @return H*vecIn The Hessian evaluated at xc applied to the input vector.
   */
  virtual Eigen::VectorXd applyHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn);

  virtual Eigen::MatrixXd getHess(const Eigen::VectorXd& xc);

  /** Add a constraint to this optimization problem.
   *  @param[in] cin The constraint we are considering
   *  @return A reference to this optimization problem.
   */

  //virtual OptProblem& operator+=(const OptConstraint &cin);

  /** Return true the problem is constrained or not.
   *  @return True if the problem is constrained, false otherwise
   */
  bool isConstrained() const;


  /** Tell the caller if this problem is stochastic or not.
   *  @return True if the problem is stochastic (i.e. Evaluating the objective at the same point might not always return
   *     the same value) and false if the problem is deterministic.
   */
  bool isStochastic() const;

  // the following functions extract the type of optimization problem, i.e. unconstrained, bound constrained, etc...
  bool isUnconstrained() const;
  bool isBoundConstrained() const;
  bool isPartlyBoundConstrained() const;
  bool isFullyBoundConstrained() const;
  bool isLinearlyConstrained() const;
  bool isNonlinearlyConstrained() const;

  /** Returns the dimension of this optimization problem.
   *  @return The dimension of this optimization problem, i.e. the number of decision variables
   */
  int  GetDim() const
  {
    return dim;
  }

  /** Add a constraint to this optimization problem.
   *  @param[in] c The constraint instance
   *  @param[in] isEqual True is this is an equality constraint, false for inequality
   */
  void AddConstraint(std::shared_ptr<ConstraintBase> c, bool isEqual);

  /** Get the number of equality constraints. */
  int  NumEqualities() const
  {
    return equalityConsts.NumConstraints();
  }

  /** Get the number of inequality constraints. */
  int NumInequalities() const
  {
    return inequalityConsts.NumConstraints();
  }

  // store a list of equality constraints
  ConstraintList equalityConsts;

  // store a list of inequality constraints
  ConstraintList inequalityConsts;

protected:

  // relative finite difference tolerance
  double reltol = 1e-4;

  // absoluted finite difference tolerance -- this is the smallest step allowed
  double abstol = 1e-12;

  // store the number of optimization parameters
  int dim;
};

// class OptProblem
} // namespace Optimization
} // namespace muq


#endif // ifndef OptProblem_h_
