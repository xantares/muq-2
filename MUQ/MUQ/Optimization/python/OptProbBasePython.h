
#ifndef OPTPROBBASEPYTHON_H_
#define OPTPROBBASEPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Optimization/Problems/OptProbBase.h"

namespace muq {
namespace Optimization {
/// Python wrapper around muq::Optimization::OptProbBase
class OptProbBasePython : public OptProbBase, public boost::python::wrapper<OptProbBase> {
public:

  OptProbBasePython(int dimIn);

  /// Evaluate the objective.
  /**
   * A python wrapper; allows users to implement object in python
   *  @param[in] xc The location to evaluate the objective.
   *  @return The value of the objective at xc.
   */
  double PyEval(boost::python::list const& xc);

  /// Evaluate the gradient, using finite differences if a more sophisticated method was not implemented in child class.
  /**
   * Allows user to implement gradient in python.  Python does not allow pass by reference so we have return a list that
   * is [cost function, [gradient]]
   *  @param[in] xc The location to compute the objective and gradient.
   *  @return A list, entry one is the cost funciton evauluation, entry two is the gradient
   */
  boost::python::list PyGrad(boost::python::list const& x);

  /// Apply the Hessian inverse (or approximate Hessian inverse) to the input vector.
  /** This function defaults to finite
   *  differences but more efficient approximations such as Gauss-Newton model Hessians for nonlinear least squares
   *  problems are implemented in child classes.
   *  @param[in] xc The location to evaluate the Hessian
   *  @param[in] vecIn The vector we want to apply the Hessian to.
   *  @return H^{-1}*vecIn The inverse Hessian evaluated at xc applied to the input vector.
   */
  boost::python::list PyApplyInvHess(const boost::python::list& xc, const boost::python::list& vecIn);
  boost::python::list PyGetInvHess(const boost::python::list& xc);

  /// Apply the Hessian (or approximate Hessian) to the input vector.
  /** This function defaults to finite differences but
   *  more efficient approximations such as Gauss-Newton model Hessians for nonlinear least squares problems are
   *  implemented in child classes.
   *  @param[in] xc The location to evaluate the Hessian
   *  @param[in] vecIn The vector we want to apply the Hessian to.
   *  @return H*vecIn The Hessian evaluated at xc applied to the input vector.
   */
  boost::python::list PyApplyHess(const boost::python::list& xc, const boost::python::list& vecIn);

  boost::python::list PyGetHess(const boost::python::list& xc);

private:

  /// Evaluate the gradient, using finite differences if a more sophisticated method was not implemented in child class.
  /**
   *  @param[in] xc The location to compute the objective and gradient.
   *  @param[out] gradient A vector that will be filled with the gradient.
   *  @return xc The value of the objective at xc.
   */
  virtual double grad(Eigen::VectorXd const& xc, Eigen::VectorXd& gradient) override;

  /// Evaluate the objective.
  /**
   *  @param[in] xc The location to evaluate the objective.
   *  @return The value of the objective at xc.
   */
  virtual double eval(Eigen::VectorXd const& xc) override;

  /// Apply the Hessian inverse (or approximate Hessian inverse) to the input vector.
  /** This function defaults to finite
   *  differences but more efficient approximations such as Gauss-Newton model Hessians for nonlinear least squares
   *  problems are implemented in child classes.
   *  @param[in] xc The location to evaluate the Hessian
   *  @param[in] vecIn The vector we want to apply the Hessian to.
   *  @return H^{-1}*vecIn The inverse Hessian evaluated at xc applied to the input vector.
   */
  virtual Eigen::VectorXd applyInvHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn) override;
  virtual Eigen::MatrixXd getInvHess(const Eigen::VectorXd& xc) override;

  /// Apply the Hessian (or approximate Hessian) to the input vector.
  /** This function defaults to finite differences but
   *  more efficient approximations such as Gauss-Newton model Hessians for nonlinear least squares problems are
   *  implemented in child classes.
   *  @param[in] xc The location to evaluate the Hessian
   *  @param[in] vecIn The vector we want to apply the Hessian to.
   *  @return H*vecIn The Hessian evaluated at xc applied to the input vector.
   */
  virtual Eigen::VectorXd applyHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn) override;

  virtual Eigen::MatrixXd getHess(const Eigen::VectorXd& xc) override;
};

void               ExportOptProbBase();
} // namespace optimiztion
} // namespace muq

#endif // ifndef OPTPROBBASEPYTHON_H_
