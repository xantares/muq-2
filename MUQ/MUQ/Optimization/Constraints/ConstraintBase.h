
#ifndef _ConstraintBase_h
#define _ConstraintBase_h

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include "MUQ/Utilities/python/PythonTranslater.h" // This needs to be the first include to avoid a weird "toupper" macro error on osx 
#endif // if MUQ_PYTHON == 1

#include <Eigen/Core>

#if MUQ_PYTHON == 1
# include <boost/python.hpp>
# include <boost/python/def.hpp>
# include <boost/python/class.hpp>

#endif // if MUQ_PYTHON == 1

namespace muq {
namespace Optimization {
/** @class ConstraintBase
 *   @ingroup Optimization
 *
 *  @brief Abstract base for all constraints.
 *  @details This class provides a base for all constraints, whether they be inequality, equality, linear, bound, or
 *nonlinear.  They all must share this common form.  We assume that all equality constraints take the form g(x) = 0 and
 *all inequality constraints take the form g(x) <= 0.  Thus, we can completely define the constraints through the
 *function g.
 */
class ConstraintBase {
public:

  ConstraintBase(int DimInIn, int DimOutIn) : DimIn(DimInIn), DimOut(DimOutIn) {}

  /** Evaluate this constraint at a point xc.  This function returns a vector with the constraint values.
   *  @param[in] xc A vector of decision variables declaring where we wish to evaluate this constraint.
   *  @return A vector containing the constraint values.
   */
  virtual Eigen::VectorXd eval(const Eigen::VectorXd& xc) = 0;

  /** Evaluate the action of the Jacobian of this constraint on a vector.  Another way of thinking of this is that we
   * want to compute the gradient of some objective with respect to the input to this constraint and vecIn is the
   * sensitivity of that objective to the output of this constraint.  In that setting, this function is one step in a
   * chain rule.  For example, if we want to evaluate df/dx =df/dg dg/dx.  This function would take df/dg as input
   * (vecIn) and compute df/dg*dg/dx
   *  @param[in] xc The location where we want to perform the evaluation -- i.e. where do we take the derivative?
   *  @param[in] vecIn The input vector that may represent df/dg
   *  @return The Jacobian of this constraint applied to vecIn
   */
  virtual Eigen::VectorXd ApplyJacTrans(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn) = 0;

  virtual Eigen::MatrixXd getJacobian(const Eigen::VectorXd& xc)
  {
    Eigen::MatrixXd jac(DimOut, DimIn);

    for (int i = 0; i < DimOut; ++i) {
      Eigen::VectorXd sens = Eigen::VectorXd::Zero(DimOut);
      sens(i)    = 1.0;
      jac.row(i) = ApplyJacTrans(xc, sens).transpose();
    }
    return jac;
  }

#if MUQ_PYTHON == 1
  boost::python::list PyGetJacobian(boost::python::list const& xc)
  {
    return muq::Utilities::GetPythonVector<Eigen::VectorXd>(getJacobian(muq::Utilities::GetEigenVector<
                                                                                 Eigen::VectorXd>(xc)));
  }

#endif // if MUQ_PYTHON == 1

  virtual Eigen::MatrixXd getHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& sensitivity)
  {
    const bool HessImplemented = false;

    assert(HessImplemented);
    return Eigen::MatrixXd();
  }

#if MUQ_PYTHON == 1
  boost::python::list PyGetHess(boost::python::list const& xc, boost::python::list const& sensitivity)
  {
    return muq::Utilities::GetPythonVector<Eigen::VectorXd>(getHess(muq::Utilities::GetEigenVector<Eigen
                                                                                                                 ::
                                                                                                                 VectorXd>(
                                                                             xc),
                                                                           muq::Utilities::GetEigenVector<Eigen
                                                                                                                 ::
                                                                                                                 VectorXd>(
                                                                             sensitivity)));
  }

#endif // if MUQ_PYTHON == 1

  int NumConstraints() const
  {
    return DimOut;
  }

  int GetDim() const
  {
    return DimIn;
  }

#if MUQ_PYTHON == 1
  /// Allow user to evaluate in python
  virtual boost::python::list PyEval(boost::python::list const& xc)
  {
    return muq::Utilities::GetPythonVector<Eigen::VectorXd>(eval(muq::Utilities::GetEigenVector<Eigen::
                                                                                                              VectorXd>(
                                                                          xc)));
  }

  /// Allow user to implement apply jacobian transpose in python
  virtual boost::python::list PyApplyJacTrans(boost::python::list const& xc, boost::python::list const& vecIn)
  {
    return muq::Utilities::GetPythonVector<Eigen::VectorXd>(ApplyJacTrans(muq::Utilities::GetEigenVector<
                                                                                   Eigen::VectorXd>(xc),
                                                                                 muq::Utilities::GetEigenVector<
                                                                                   Eigen::VectorXd>(vecIn)));
  }

#endif // if MUQ_PYTHON == 1

protected:

  // the input and output dimensions of this constraint.  DimIn must be the same dimension as x, but DimOut can be
  // anything greater than zero.
  int DimIn, DimOut;
};

// class ConstraintBase
} // namespace Optimization
} // namespace muq
#endif  // ifndef _ConstraintBase_h
