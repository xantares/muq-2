
#ifndef ANISOTROPICPOWERKERNELNUGGET_H
#define ANISOTROPICPOWERKERNELNUGGET_H

#include <Eigen/Dense>

#include "MUQ/Geostatistics/CovKernel.h"

namespace muq {
namespace Geostatistics {
class AnisotropicPowerKernelNugget : public muq::Geostatistics::CovKernel {
public:

  AnisotropicPowerKernelNugget(Eigen::VectorXd lengthScalesIn, double PowerIn = 2, double nugget=1e-4);
  virtual ~AnisotropicPowerKernelNugget() = default;

  virtual double  Kernel(const Eigen::Ref<const Eigen::VectorXd>& x, const Eigen::Ref<const Eigen::VectorXd>& y) const;


  Eigen::MatrixXd BuildCov(Eigen::MatrixXd const& xs) const;
  
  Eigen::VectorXd BuildCov(Eigen::MatrixXd const& xs, Eigen::VectorXd const& y) const;
  
  /** Set the covariance kernel parameters (Length, power, variance) from a vector of parameters.
   *  @param[in] theta MuqVector or parameter values for the covariance kernel.
   */
  virtual void            SetParms(const Eigen::VectorXd& theta) override;

  virtual Eigen::VectorXd GetParms() override;


  virtual void            GetGrad(const Eigen::Ref<const Eigen::VectorXd>& x,
                                  const Eigen::Ref<const Eigen::VectorXd>& y,
                                  Eigen::VectorXd                        & grad) const;


  virtual double EstimateLength() const
  {
    return 1.0;
  }

  virtual void                BuildGradMat(const Eigen::MatrixXd& xs, std::vector<Eigen::MatrixXd>& GradMat) const;

  virtual     Eigen::VectorXd KernelSpatialDerivative(Eigen::VectorXd const& x, Eigen::VectorXd const& xWrt) override;

protected:

  Eigen::VectorXd lengthScales;
  double Power;
  int    Nparams;
  double nugget;
};
}
}

#endif // ANISOTROPICPOWERKERNELNUGGET_H
