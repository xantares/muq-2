#ifndef LINEARBASISPYTHON_H_
#define LINEARBASISPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Inference/TransportMaps/LinearBasis.h"

namespace muq {
namespace Inference {
class ConstantBasisPython : public ConstantBasis, public boost::python::wrapper<ConstantBasis> {
public:

  ConstantBasisPython(int inputSize);

private:
};

class LinearBasisPython : public LinearBasis, public boost::python::wrapper<LinearBasis> {
public:

  LinearBasisPython(int inputSize, int linearDim);

private:
};

void ExportConstantBasis();

void ExportLinearBasis();
} // namespace Inference
} // namespace muq

#endif // ifndef LINEARBASISPYTHON_H_
