#ifndef TRANSPORTMAPBASISPYTHON_H_
#define TRANSPORTMAPBASISPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Inference/TransportMaps/TransportMapBasis.h"

namespace muq {
namespace Inference {
void ExportTransportMapBasis();
}
}

#endif // ifndef TRANSPORTMAPBASISPYTHON_H_
