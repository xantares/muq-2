
#ifndef PolynomialMap_h_
#define PolynomialMap_h_

#include "MUQ/Inference/TransportMaps/TransportMap.h"

#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"
#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"

namespace muq {
namespace Inference {
  
  class MapFactory;
  class MapUpdateManager;
  
  /** @class PolynomialMap
      @ingroup TransportMaps
      @brief Class defining lower triangular transport maps using a multivariate polynomial basis.
   */
class PolynomialMap : public TransportMap {
  friend class MapUpdateManager;
  friend class MapFactory;

public:
  
  PolynomialMap(int inputDim,
                const std::vector<std::shared_ptr<muq::Utilities::MultiIndexSet>>& multisIn,
                const std::vector<Eigen::VectorXd>&                                coeffsIn);

  // default virtual destructor
  virtual ~PolynomialMap() = default;

  const std::vector<muq::Utilities::MultiIndexSet> GetMultis() const;
  virtual const std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> GetPolys() const = 0;
  
  
protected:
  
  /// The multiindices to use in each output dimension
  std::vector<std::shared_ptr<muq::Utilities::MultiIndexSet>> allMultis;
  
  Eigen::VectorXu maxOrders;

}; // class TransportMap

} // namespace Inference
} // namespace muq

#endif // ifndef _TransportMap_h
