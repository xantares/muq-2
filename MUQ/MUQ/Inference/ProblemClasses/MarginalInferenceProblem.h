#ifndef MARGINALINFERENCEPROBLEM_H_
#define MARGINALINFERENCEPROBLEM_H_

#include "MUQ/Modelling/DensityRVPair.h"

#include "MUQ/Inference/ImportanceSampling/ImportanceSampler.h"
#include "MUQ/Inference/ProblemClasses/InferenceProblem.h"

namespace muq {
namespace Inference {
class MCMCState;

/// Sample the marginal distribution
/**
 *   Sample the posterior marginal \f$\pi(\mathbf{x} | \mathbf{d}) = \int_{\mathcal{Y}} \pi(\mathbf{x}, \mathbf{y} |
 * \mathbf{d}) \, d\mathbf{y}\f$ using the pseudo-marginal approach (Andrieu and Roberts 2009)
 */
class MarginalInferenceProblem : public InferenceProblem {
public:

  template<typename rvType, typename densType, typename specType>
  MarginalInferenceProblem(std::shared_ptr<muq::Modelling::ModGraph> completeModel,
                           std::shared_ptr<muq::Modelling::Density> const& conditionalPrior,
                           std::shared_ptr<muq::Modelling::DensityRVPair<rvType, densType, specType> > const& qPair,
                           const unsigned int numIPSamps = 100,
                           const unsigned int marginalizedDim = 0,
                           std::vector<std::string> const& inOrder = std::vector<std::string>(),
                           std::shared_ptr<muq::Inference::AbstractMetric> metric =
                             std::make_shared<muq::Inference::EuclideanMetric>()) :
    InferenceProblem(completeModel, inOrder, metric)
  {
    //  likelihoodIP = std::make_shared<ImportanceSampler>(likelihood, conditionalPrior, qPair, numIPSamps,
    //                                                    marginalizedDim);
  }

  virtual ~MarginalInferenceProblem() = default;

private:

  virtual std::shared_ptr<MCMCState> ConstructState(Eigen::VectorXd const& state, int const currIteration, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override;

  /// Importance sampler evaluation of likelihood
  std::shared_ptr<ImportanceSampler> likelihoodIP;
};
} // namespace Inference
} // namespace muq

#endif // ifndef MARGINALINFERENCEPROBLEM_H_
