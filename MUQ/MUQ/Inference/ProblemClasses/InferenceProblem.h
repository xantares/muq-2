
#ifndef INFERENCEPROBLEM_H
#define INFERENCEPROBLEM_H
#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/RandVar.h"
#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/ModGraphPiece.h"

#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"
#include "MUQ/Inference/MCMC/MCMCState.h"

namespace muq {
namespace Inference {
/**
 * An inference density is simply one where the prior and likelihood are both available, and the actual density
 *evaluates to their product. If desired, you can provide a metric, else the Euclidean metric is used by default.
 **/
class InferenceProblem :  public AbstractSamplingProblem {
public:

  virtual ~InferenceProblem() = default;

  InferenceProblem(std::shared_ptr<muq::Modelling::ModGraph>                       completeModel,
                   std::shared_ptr<muq::Inference::AbstractMetric> metric =
                   std::                                         make_shared<muq::Inference::EuclideanMetric>());

  InferenceProblem(std::shared_ptr<muq::Modelling::ModGraph>                       completeModel,
                   std::vector<std::string> const                & inputOrder,
                   std::shared_ptr<muq::Inference::AbstractMetric> metric =
                   std::                                         make_shared<muq::Inference::EuclideanMetric>());

  virtual std::shared_ptr<MCMCState> ConstructState(Eigen::VectorXd const& state, int const currIteration, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override;

  /// the forward model
  std::shared_ptr<muq::Modelling::ModPiece> forwardMod = nullptr;

  /// the error model, which goes around the Forward model to create the likelihood
  std::shared_ptr<muq::Modelling::Density> errorMod;

  /// the likelihood, including the forward model
  std::shared_ptr<muq::Modelling::ModGraphDensity> likelihood;
  std::shared_ptr<muq::Modelling::ModGraphDensity> posterior;
  
  /// the prior density
  std::shared_ptr<muq::Modelling::Density> prior;


  void InvalidateCaches() override;
  virtual void SetGraph(std::shared_ptr<muq::Modelling::ModGraph> graph) override;
  virtual void SetGraph(std::shared_ptr<muq::Modelling::ModGraph> graph, std::vector<std::string> const& inOrder);
  
protected:
  ///Set the graph. This is necessary so that the virtual function isn't called during construction.
  void SetGraphImpl(std::shared_ptr<muq::Modelling::ModGraph> graph, std::vector<std::string> const& inOrder);
};
}
}
#endif // INFERENCEDENSITY_H
