#!/bin/bash
set +e
cd $WORKSPACE/MUQ
build/modules/RunAllTests --gtest_output=xml:$WORKSPACE/TestResults.xml

exit 0
