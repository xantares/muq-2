#!/bin/bash

getInfo () {
echo " "
echo "User $1 has contributed:"
git log --shortstat --author "$1" \
    | grep "files\? changed" \
    | awk '{files+=$1; inserted+=$4; deleted+=$6} END {printf "  files changed:  %8d\n  lines inserted: %8d\n  lines deleted:  %8d\n", files, inserted, deleted}'
}

cloc --exclude-dir=build,html,external,data,results MUQ
echo " "
getInfo mparno
getInfo prconrad
getInfo davisad
getInfo zheng_w
getInfo ryan.xun.huan
getInfo chifeng
echo " "
